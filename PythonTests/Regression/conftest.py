import pytest
from dplus.CalculationRunner import LocalRunner


@pytest.fixture(scope="module")
def LocalAPI():
    exe_directory=r"D:\UserData\devora\Sources\dplus\x64\Release"
    #session=r"D:\UserData\devora\Sources\dplus\WebApplication\media\testing"
    return LocalRunner(exe_directory)#, session)

def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._previousfailed = item

def pytest_runtest_setup(item):
    if "incremental" in item.keywords:
        previousfailed = getattr(item.parent, "_previousfailed", None)
        if previousfailed is not None:
            pytest.xfail("previous test failed (%s)" %previousfailed.name)


