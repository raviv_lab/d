import pytest
from Regression.utilities import baseforintensity






class TestHelix(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'helix/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'helix/Results.out'

class TestAsymmetricSlabs(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'asymetric slabs/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'asymetric slabs/Results.out'


class TestMScSymmetryPDB(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'MscSymmetry PDB/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'MscSymmetry PDB/Result.out'

class TestMSymmetryPDB(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'MSymmetry PDB/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'MSymmetry PDB/Result.out'

class TestScSymmetryPDB(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'ScSymmetry/paramsPDB.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'ScSymmetry/ResultPDB.out'

class TestScSymmetryAmp(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'ScSymmetry/paramsamp.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'ScSymmetry/Resultamp.out'


class TestSfSymmetry1(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'SfSymmetry PDB/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'SfSymmetry PDB/Result.out'

class TestSfSymmetry2(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'SfSymmetry PDB/params2.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'SfSymmetry PDB/Result2.out'

class TestShortCylinder(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'short cylinder/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'short cylinder/Results.out'

class TestShortGaussCylinder(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'short gaussian cylinder/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'short gaussian cylinder/Results.out'

class TestSphere(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'sphere/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'sphere/Results.out'

class TestSymmetricSlab(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'symmetric slab/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'symmetric slab/Results.out'

class TestSymmetricSlabVegas(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'symmetric slab vegas/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'symmetric slab vegas/Results.out'

