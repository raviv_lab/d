import os
import pytest
from Regression.utilities import baseforamp


mypath=r'D:\UserData\devora\Sources\dplus\PythonTests\Regression\storage\devora'



class TestSphere(baseforamp):
    @pytest.fixture(scope='class')
    def infile(self):
        return os.path.join(mypath, 'sphere.state')

    @pytest.fixture(scope='class')
    def efile(self):
        return os.path.join(mypath, 'sphere.amp')

class TestHelix(baseforamp):
    @pytest.fixture(scope='class')
    def infile(self):
        return os.path.join(mypath, 'helix.state')

    @pytest.fixture(scope='class')
    def efile(self):
        return os.path.join(mypath, 'helix.amp')

