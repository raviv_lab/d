from Regression.utilities import baseforintensity
import pytest
import os

#SfSymmetry2, SymmetricSlabs get different values than expected

os.chdir(r"D:\UserData\devora\Sources\dplus\PythonTests\Regression\storage\avi")

#MScSymmetryPDB, TestMSymmetryPDB-- some kind of issue on the python end that causes problems loading the arguments-- I have a general idea where the problem is located
#UnboundLocalError: local variable 'param_index' referenced before assignment
class xTestMScSymmetryPDB(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'MscSymmetry PDB/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'MscSymmetry PDB/Result.out'

class xTestMSymmetryPDB(baseforintensity):
        @pytest.fixture(scope='class')
        def infile(self):
            return r'MSymmetry PDB/params.state'

        @pytest.fixture(scope='class')
        def efile(self):
            return r'MSymmetry PDB/Result.out'


class xTestSfSymmetry2(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'SfSymmetry PDB/params2.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'SfSymmetry PDB/Result2.out'


class xTestScSymmetryPDB(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'ScSymmetry/paramsPDB.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'ScSymmetry/ResultPDB.out'


class xTestSymmetricSlab(baseforintensity):
    @pytest.fixture(scope='class')
    def infile(self):
        return r'symmetric slab/params.state'

    @pytest.fixture(scope='class')
    def efile(self):
        return r'symmetric slab/Results.out'


if __name__=="__main__":
    pass
    #t=TestHelix()
    #if t.test_args(t.infile(), t.efile()):
    #    t.test_intensity(t.infile(), t.efile())
