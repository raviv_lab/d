-----------------------------Functions defenition---------------------------

function tanhED(r, params)
	local result = 0 
	
	for i = 2,#params  do
		local x1 =  params [i-1] [2] * ( r - params [i-1] [1] )
		local x2 = params [i] [2 ] * (r - params [i] [1] ) 		
		result  =  result +  0.5  * params [i] [3] * (   math.tanh(x1) -    math.tanh(x2))		
	end	
	
	return result +  params [1][3]
	
end

-- The function that generates a table with the parameter values
function genparams(SolED,rmin, rmax, res, lfunc, params)
	local result = {};
	local sz = 1 / res;
	result[1] = {0, SolED}
	for i=1,res do
		local r = rmin +  i* ((rmax - rmin) *  sz)
		local ED = lfunc(r, params)
		if  ED < 0 then ED = 0 end
		result[i + 1] = { (rmax - rmin) *  sz,  ED}
	end
	
	return result;
end

-- The function that generates a table with the parameter values ADAPTIVELY
-- res is the maximal resolution.
function adaptiveparams(SolED, epsilon, rmin, rmax, res, lfunc, params)
	local result = {};
	local sz = 1 / res;
	local j = 1;
	local lastED = SolED;	
	local avgEDsum = SolED;
	local avgCount = 1;
	
	result[1] = {0, SolED}
	for i=1,res do
		local r = rmin +  i* ((rmax - rmin) *  sz)
		local ED = lfunc(r, params)
		if  ED < 0 then ED = 0 end
		
		if math.abs(ED - (avgEDsum / avgCount)) > epsilon then
			-- Add layer
			result[j] = { (rmax - rmin) *  sz * avgCount,  avgEDsum / avgCount}
			j = j + 1
			
			-- Restart
			avgEDsum = ED;
			avgCount = 1;
		else
			-- Accumulate
			avgEDsum = avgEDsum + ED;
			avgCount = avgCount + 1;
		end
	end
	
	-- Add final layer
	result[j] = { (rmax - rmin) *  sz * avgCount,  avgEDsum / avgCount}
	
	return result;
end

----------------------------------------------------------------------


-- Number of discrete layers
resolution = 1000
rmin = 0
SolED  = 333
SolR  = 0 
capsidInnerWall =  20
Sheelwidth = 3
SlopeNearWall = 5
SlopeInner = 0.5
layerDeltaED = 30
-- v[1] = layer offset (nm)
-- v[2] = layer smoothness (a.u.)
-- v[3] = layer ED (e/nm^3)
params = {
	{ SolR, 10000,SolED},   			 -- Solvent
	{ capsidInnerWall - Sheelwidth , SlopeInner,0},			
	{capsidInnerWall,SlopeNearWall , layerDeltaED  }
}

-- Computing r_max
rmax =  0
for k,v in ipairs(params) do
	val = v[1] + (10.36 / v[2]);
	if (val > rmax) then
		rmax = val
	end
end

-----------------------------------------------------------------------


domain = {
	Geometry = "Domain",
	Models = {
		{
			Type = dplus.findamplitude("Sphere"),
			
			                                                                     -- R   ED
			Parameters =	adaptiveparams(SolED, 0.0001, rmin, rmax, resolution, tanhED, params),
			ExtraParameters =	{1, 5},
			Location = {0, 0, 0, 0 ,0, 0}
		},
	}
};

dplus.setparametertree(domain);
