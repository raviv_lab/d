-----------------------------Functions defenition---------------------------
function trim(s) -- trim4
  return s:match"^%s*(.*)":match"(.-)%s*$"
end
----------------------------------------------------------------------------

---Input parameters --------------------------------------------------------
----------------------------------------------------------------------------

--Number of frames
N = 20

-- Max number of amplitudes that can be uploaded simultaneously
maxN = 20

--Folders containing the PDBs for the protein and water
path_protein =  "C:\\Roi\\simulations of VP1\\Pdbs\\vetice_ns_2_3\\"
PDB_protein  = "GC_dplus_format"

path_water = "C:\\Roi\\simulations of VP1\\Pdbs\\tip4pew_ns_2_3\\"
PDB_water  = "GC_dplus_format"

--Folders and base names containing the output amplitudes and intensities from the protein and water frames
path_Output_protein   =  "C:\\Roi\\simulations of VP1\\Pdbs\\vertice_frames\\"
exportFilenameProtein = "Edge_frame_" 
path_Output_water     = "C:\\Roi\\simulations of VP1\\Pdbs\\water_frames\\"
exportFilenameWater   = "Water_frame_" 

-- State files for calculation the amplitude and intensity from a single frame
stateFileProteins = "C:\\Roi\\simulations of VP1\\protein_frames.state"
stateFileWater    = "C:\\Roi\\simulations of VP1\\water_frames.state"

-- Folder and base names containing the output amplitudes and intensities average protein and water amplitudes
path_averageProtein = "C:\\Roi\\simulations of VP1\\Pdbs\\vertice_frames\\"
path_averageWater   = "C:\\Roi\\simulations of VP1\\Pdbs\\water_frames\\" 

-- State files for calculation the amplitude and intensity from a single frame
stateFile_ave_Proteins = "C:\\Roi\\simulations of VP1\\AMP_ave_protein.state"
stateFile_ave_Water    = "C:\\Roi\\simulations of VP1\\AMP_ave_water.state"

-- If N is larger than maxN prepare a state file containing N/maxN amplitude files inside a manual symmetry
final_Protein_ave_state = "C:\\Roi\\simulations\\final_AMP_ave_Protein.state"
final_Water_ave_state = "C:\\Roi\\simulations\\final_AMP_ave_water.state"

-- State file for the subtraction
Sub_stateFile = "C:\\Roi\\simulations of VP1\\AMP_subtraction.state"

-- final output files
final_path = "C:\\Roi\\simulations of VP1\\final subtraction\\"
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------


---Start calculations
print ("Calculation started!!")

-------------------Proteins Frames -----------------------------------------------------
----------------------------------------------------------------------------------------

dof = dofile (stateFileProteins)

-- Uploading the domain from the state file 
dplus.setparametertree(Domain);

-- Over all the frames of of the protein
for h = 1,N do
	  Domain.Populations[1].Models[1].Filename = path_protein .. PDB_protein .. 200 + h ..".pdb"
	  
	  print( path_protein .. PDB_protein .. h ..".pdb")
	  
	  -- Run calculation
	  dplus.setparametertree(Domain);
	  dplus.generate(Domain, path_Output_protein .. exportFilenameProtein .. h, true);
	  -- Force memory release...
	  print("Garbage to be collected: " .. collectgarbage("count")*1024)
	  collectgarbage()
	  print("Garbage left: " .. collectgarbage("count")*1024)
end
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------


-------------------water Frames -----------------------------------------------------
----------------------------------------------------------------------------------------
-- dof = dofile (stateFileWater)

-- -- Uploading the domain from the state file 
-- dplus.setparametertree(Domain);


-- for h = 1,N do
	  -- Domain.Populations[1].Models[1].Filename = path_water .. PDB_water .. 200 + h ..".pdb"
	  
	  -- print( path_water .. PDB_water .. h ..".pdb")
	  
	  -- -- Run calculation
	  -- dplus.setparametertree(Domain);
	  -- dplus.generate(Domain, path_Output_water .. exportFilenameWater .. h, true);
	  -- -- Force memory release...
	  -- print("Garbage to be collected: " .. collectgarbage("count")*1024)
	  -- collectgarbage()
	  -- print("Garbage left: " .. collectgarbage("count")*1024)
-- end
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------



-- Average the amplitudes in fractions of maxN -----------------------------------------
----------------------------------------------------------------------------------------
dof = dofile (stateFile_ave_Proteins)
dplus.setparametertree(Domain);

for l = 0, N / maxN - 1 do
	for h = 1,maxN do
		  Domain.Populations[1].Models[1].Children[h].Filename = path_averageProtein .. exportFilenameProtein .. l*maxM + h .. ".amp"
	end
	
	Domain.Populations[1].Models[1].ExtraParameters[1] = 1 / maxN 
	
	Protein_part_ave = "protein_ave_nFrames_" 
	-- Run calculation
	dplus.setparametertree(Domain);
	dplus.generate(Domain, path_averageProtein .. Protein_part_ave .. l*maxN + 1 .. "_" .. (l + 1) * maxN , true);
		  
	-- Force memory release...
	print("Garbage to be collected: " .. collectgarbage("count")*1024)
	collectgarbage()
	print("Garbage left: " .. collectgarbage("count")*1024)
	
end

-- dof = dofile (stateFile_ave_Water)
-- dplus.setparametertree(Domain);

-- for l = 0, N / maxN - 1 do
	-- for h = 1,maxN do
		  -- Domain.Populations[1].Models[1].Children[h].Filename = path_averageWater .. exportFilenameWater .. h .. ".amp"
	-- end
	
	-- Domain.Populations[1].Models[1].ExtraParameters[1] = 1 / maxN 
	
	-- Water_part_ave = "water_ave_nFrames_" 
	-- -- Run calculation
	-- dplus.setparametertree(Domain);
	-- dplus.generate(Domain, path_averageWater .. Water_part_ave .. l*maxN + 1 .. "_" .. (l + 1) * maxN , true);
		  
	-- -- Force memory release...
	-- print("Garbage to be collected: " .. collectgarbage("count")*1024)
	-- collectgarbage()
	-- print("Garbage left: " .. collectgarbage("count")*1024)
	
-- end
-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

if N > maxN then
    dof = dofile (final_Protein_ave_state)
	for i = 0, N / maxN - 1 do
	
		Domain.Populations[1].Models[1].Children[i+1].Filename = path_averageProtein .. Protein_part_ave .. i*maxN + 1 .. "_" .. (i + 1) * maxN  .. ".amp"
			
		Domain.Populations[1].Models[1].ExtraParameters[1] = maxN / N
		
		Protein_final_ave = "protein_final_ave"
		-- Run calculation
		dplus.setparametertree(Domain);
		dplus.generate(Domain, path_averageProtein .. Protein_final_ave , true);
			  
		-- Force memory release...
		print("Garbage to be collected: " .. collectgarbage("count")*1024)
		collectgarbage()
		print("Garbage left: " .. collectgarbage("count")*1024)
	
	end
	
	dof = dofile (final_Water_ave_state)
	for i = 0, N / maxN - 1 do
	
		Domain.Populations[1].Models[1].Children[i+1].Filename = path_averageWater .. Water_part_ave .. i*maxN + 1 .. "_" .. (i + 1) * maxN  .. ".amp"
			
		Domain.Populations[1].Models[1].ExtraParameters[1] = maxN / N
		
		Water_final_ave = "water_final_ave"
		-- Run calculation
		dplus.setparametertree(Domain);
		dplus.generate(Domain, path_averageWater .. Water_final_ave , true);
			  
		-- Force memory release...
		print("Garbage to be collected: " .. collectgarbage("count")*1024)
		collectgarbage()
		print("Garbage left: " .. collectgarbage("count")*1024)
	
	end
end 


dof = dofile (Sub_stateFile)
dplus.setparametertree(Domain);

if N > maxN then
	Domain.Populations[1].Models[1].Children[1].Children[1].Filename = path_averageProtein .. Protein_final_ave .. ".amp"
	Domain.Populations[1].Models[1].Children[2].Children[1].Filename = path_averageWater .. Water_final_ave .. ".amp"
else 
	Domain.Populations[1].Models[1].Children[1].Children[1].Filename = path_averageProtein .. Protein_part_ave .. 1 .. "_" ..  maxN .. ".amp"
	Domain.Populations[1].Models[1].Children[2].Children[1].Filename = path_averageWater .. Water_part_ave .. 1 .. "_" ..  maxN .. ".amp"
-- Run calculation
dplus.setparametertree(Domain);
dplus.generate(Domain, final_path .. "Ave_".. N .. "_frames_Amp_vertice_sub" , true);
end
print("Finished..")

