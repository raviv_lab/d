Information = {
	Name = "A stupid example",
	Type = "Symmetry",
	NLP = 3,
	MinLayers = -1,
	MaxLayers = -1,	
};

function Populate(p, nlayers)	 
	if (p == nil or table.getn(p) < 1) then				
		return { { 0, 0, 0, 0, 0, 0 } };
	end
	if (p[1] == nil or table.getn(p[1]) ~= 3) then
		error("Parameter matrix must be (layers, 3)");
		return { { 0, 0, 0, 90, 0, 0 } };
	end

	return {
		{0, 0, 0,  0,  0,  0}, 
		{p[1][1], 0, 0, 90, 90, 90},		
	};
end

-----------------------------------------------------
-- UI

-- Optional display parameters
function GetLayerName(index)
	return "Bleh " .. index;
end
function GetLayerParameterName(index)
	return "H" .. index .. "O";
end
	
function IsParamApplicable(layer, layerParam)
	return true;
end

function GetDefaultValue(layer, layerParam)
	return layer * 10 + layerParam;
end
