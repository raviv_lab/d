Information = {
	Name = "Scripted Symmetry",
	Type = "Geometry/Symmetry/Model", -- Where model is single geometry
	NLP = 0,
	MinLayers = -1,
	MaxLayers = -1,
	--ExtraParameters = 0,
	--DisplayParameters = 0,
	--GPUCompatible = false,
};

-- SYMMETRY
function Populate(p, nlayers)
	return {
		{0, 0, 0,  0,  0,  0}, 
		{5, 0, 0, 90, 90, 90}
	};
end

--------------------------------------------------
-- GEOMETRY / MODEL

function PreCalculate(p, nlayers)
end

-- Calculate for MODEL, CalculateFF for GEOMETRY
function CalculateFF(qvec, p, nlayers)
end

function Derivative(qvec, p, nlayers, index)
end

-----------------------------------------------------

-- GPU kernel strings
GPUPreCalculate = ""; -- The kernel string
GPUCalculateFF = ""; -- The kernel string
GPUCalculate = ""; -- The kernel string

-----------------------------------------------------
-- UI

-- Optional display parameters
function GetLayerName(index)
	return "Layer " .. index;
end
function GetLayerParameterName(index)
	return "Parameter " .. index;
end
	
function IsParamApplicable(layer, layerParam)
	return true;
end

function GetDefaultValue(layer, layerParam)
	return 0.0;
end
