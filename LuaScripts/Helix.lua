Information = {
	Name = "Helical Thingy",
	Type = "Symmetry",
	NLP = 1,
	MinLayers = 4,
	MaxLayers = 4,
};

function Populate(p, nlayers)	 
	if (p == nil or nlayers ~= 4 or table.getn(p[1]) ~= 1) then				
		error("Parameter matrix must be 3x2, it is " .. nlayers .. "x" .. table.getn(p[1]));
	end
	
	--error("Params: " .. p[1][1] .. ", " .. p[2][1] .. ", " .. p[3][1]);

	radius		= p[1][1];
	pitch		= p[2][1];
	unitsPerPit	= p[3][1];
	reps		= p[4][1];
	
	angle		= 2.0 * math.pi / unitsPerPit;
	
	res = {};
	n = 1;
	
	for n=1,reps do
		res[n] = { radius * math.cos((n - 1) * angle ),
					radius * math.sin((n - 1) * angle ),
					(n - 1) * pitch / unitsPerPit,
					0, 0, (n - 1) * 360 / unitsPerPit};
		n = n + 1;
	end

	return res;
end

-----------------------------------------------------
-- UI

-- Optional display parameters
function GetLayerName(index)
	if index == 0 then
		return "Radius";
	elseif index == 1 then
		return "Pitch";
	elseif index == 2 then
		return "Units per pitch";
	elseif index == 3 then
		return "Repetitions";
	else	
		return "N/A";
	end
end
function GetLayerParameterName(index)
	if index == 0 then
		return "Parameter";
	else
		return "N/A"
	end
end
	
function IsParamApplicable(layer, layerParam)
	return true;
end

function GetDefaultValue(layer, layerParam)
	if layer == 0 then
		return 11.3;
	elseif layer == 1 then
		return 12.1;
	elseif layer == 2 then
		return 13.0;
	elseif layer == 3 then
		return 13.0;
	end
end
