Information = {
	Name = "p21P21P21",
	Type = "Symmetry",
	NLP = 1,
	MinLayers = 6,
	MaxLayers = 6,
};

function Populate(p, nlayers)	 
	if (p == nil or nlayers ~= 6 or table.getn(p[1]) ~= 1) then				
		error("Parameter matrix must be 3x3, it is " .. nlayers .. "x" .. table.getn(p[1]));
	end
	
	--error("Params: " .. p[1][1] .. ", " .. p[2][1] .. ", " .. p[3][1]);

	
	a		= p[1][1];
	b 		= p[2][1];
	c 		= p[3][1];
	x		= p[4][1]
	y		= p[5][1]
	z 		= p[6][1]
	
	res = {{	x * a, y , z * c ,	0.0,	0.0,	0.0},
	{	(0.5 + x) * a ,	-y,	(0.5-z) * c,	0.0,	0.0,	0.0},
	{	(0.5 - x) * a,	0.5*b + y,	-z * c,	0.0,	0.0,	0.0},
	{	-x*a,	0.5*b - y,	(0.5 + z) * c,	0.0,	0.0,	0.0},
	}
	
	return res;
end

-----------------------------------------------------
-- UI

-- Optional display parameters
function GetLayerName(index)
	if index == 0 then
		return "a";
	elseif 	index == 1 then
		return "b";
	elseif 	index == 2 then
		return "c";
	elseif 	index == 3 then
		return "x";
	elseif 	index == 4 then
		return "y";
	elseif 	index == 5 then
		return "z";		
	end
end

function GetLayerParameterName(index)
	if index == 0 then
		return "Parameter";
	else
		return "N/A"
	end
end
	
function IsParamApplicable(layer, layerParam)
	return true;
end

function GetDefaultValue(layer, layerParam)
	if layer == 0 then
		return 11.1;
	elseif layer == 1 then
		return 19.8;
	elseif layer == 2 then
		return 11.1;
	elseif layer == 3 then
		return 0.08;
	elseif layer == 3 then
		return 0.08;
	elseif layer == 4 then
		return 0.3 - 0.015*19.8;	
	elseif layer == 5 then
		return 0.045;	
	end
end
