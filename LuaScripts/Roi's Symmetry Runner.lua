--[[	Script for Roi's stuff	]]

-- Opens the console (shows stdout)
dplus.openconsole()

-- From http://lua-users.org/wiki/StringTrim
-- Trims white-space from string
function trim(s) -- trim4
  return s:match"^%s*(.*)":match"(.-)%s*$"
end

-- This should be a saved state that MUST already include a loaded Amplitude and Manual Symmetry!!!
-- The Manual Symmetry should have one line in the parameters section
stateFName = "X:\\Avi\\Drop Box\\Lua\\testState2.state"
-- This is the consolidated DOL file. At every line with an index of 1 it starts a new generate
fName = "X:\\Avi\\Drop Box\\Lua\\0DOL.txt";

-- Load the proper state
file = io.open(stateFName, "r");
if not file then
	print  ("File not found  (" .. stateFName .. ")");
	msgbox ("File not found  (" .. stateFName .. ")");
	return;
end
re = dplus.load(stateFName)
file.close(file)

-- Make sure DOL file exists
file = io.open(fName, "r");
if not file then
	print  ("DOL file not found (" .. fName .. ")");
	msgbox ("DOL file not found (" .. fName .. ")");
	return;
end
file.close(file)

-- Holds the entire contents of the DOL collection File
lines = {}
-- Holds the contents of the DOL collection subdivided into individual runs
DOLs  = {}
-- Load the DOL file into memory
io.input(fName);
for line in io.lines() do
	table.insert(lines, line)
end

prevPos = 1
space = "%s"
-- A counter that tracks the number of times we want to run with different DOLs
structureCntr = 0;
newDOL = true;
xyzabg = {}
rep = 1
ind = {"ind", "x", "y", "z", "a", "b", "g"}
-- Load the DOLs into separated into separated groups
for ctr = 1, #lines do
	l = lines[ctr]
	pp = 1
	prevPos = 1
	-- Parse an individual line
	-- Ignore initial and trailing whitespace
	l = trim(l)
	-- Reduce whitespace
	l = l:gsub('%s%s*', ' ')
	
	while prevPos < string.len(l) do
		pos, pos2 = string.find(l, "[%S]+", prevPos)
		if pos then
			prevPos = pos
			xyzabg[ind[pp]] = tonumber( string.sub(l, prevPos, string.find(l, "[%S]+", pos2) ) )
			prevPos = pos2+1
			pp = pp + 1
		else
			break
		end
	end -- while

	if pp ~= 8 then
		print("There is a screwed up DOL line at line " .. ctr .. "\n")
	end

	-- Determine if this is a new DOL section
	if not newDOL then
		if xyzabg["ind"] == 1 then
			newDOL = true
		end
	end

	if newDOL then
		print("Finished reading DOL #" .. structureCntr .. "\n" )
		structureCntr = structureCntr + 1
		newDOL = false
		rep = 1
		DOLs[structureCntr] = {}
	end

	DOLs[structureCntr][rep] = xyzabg
	xyzabg = {}
	rep = rep + 1

end -- for ctr
-- Prepare the model

tree = dplus.getparametertree()
ManSym = 0;
for i = 1, #tree.Models do
	if tree.Models[i].Type == ",26" then	-- Manual Symmetry
		ManSym = i
		break;
	end
end

if ManSym == 0 then
	print ("There is no Manual Symmetry in the saved state.\n");
	msgbox("There is no Manual Symmetry in the saved state.\n");
	return;
end


-- Start running the individual models
for i = 1, #DOLs do
	-- Use existing state, just redo the Manual Symmetry variables


	for j = 1, #DOLs[i] do
		
		tree.Models[ManSym].Parameters[j] = {}
		tree.Models[ManSym].Parameters[j] = { DOLs[i][j].x, DOLs[i][j].y, DOLs[i][j].z, DOLs[i][j].a, DOLs[i][j].b, DOLs[i][j].g }
		if not tree.Models[ManSym].Constraints[j] then
			tree.Models[ManSym].Constraints[j] = tree.Models[1].Constraints[j - 1]
		end
		if not tree.Models[ManSym].Mutables[j] then
			tree.Models[ManSym].Mutables[j] = tree.Models[1].Mutables[j - 1]
		end
		if not tree.Models[ManSym].Sigma[j] then
			tree.Models[ManSym].Sigma[j] = tree.Models[1].Sigma[j - 1]
		end
	end -- for j
	print(i .. "\n")
	-- Run calculation
	gen = dplus.generate(tree);
	svFileName ="Model " .. i .. ".dat"
	res = dplus.writedata(svFileName, gen)
	if not res then
		print("There was a problem with file " .. i .. ".\n");
	end

	-- Force memory release...
	print("Garbage to be collected: " .. collectgarbage("count")*1024)
	collectgarbage()
	print("Garbage left: " .. collectgarbage("count")*1024)

end -- for i


