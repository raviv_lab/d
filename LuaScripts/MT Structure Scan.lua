function file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

-- Opens the console
dplus.openconsole()
stateFName = "C:\\Delete\\Basic 14 AMP For Configurations.state"
svPath = "C:\\Delete\\All Configurations\\"

dummmy = dplus.load(stateFName)

domain = dplus.getparametertree();

helix = domain.Populations[1].Models[1]
dimLoc = helix.Children[1].Location



iRad = helix.Parameters[1][1]
iPit = helix.Parameters[2][1]
iBeta = dimLoc.beta
iAlpha = dimLoc.alpha

dRad = 0.1
dPit = 0.1
dAlp = 5 
dBet = 5 
for rd = -5, 5 do
	helix.Parameters[1][1] = iRad + rd * dRad;
	for pt = -5, 5 do
		helix.Parameters[2][1] = iPit + pt * dPit;
		for bet=-2, 2 do
			dimLoc.beta = iBeta + bet * dBet * math.pi / 180
			for alp=-2, 2 do
				dimLoc.alpha = iAlpha + alp * dAlp * math.pi / 180
				uin = helix.Parameters[3][1] .."\\".. iRad + rd * dRad ..  " ".. iPit + pt * dPit .. " ".. iBeta + bet * dBet .. " " .. iAlpha + alp * dAlp
				svFileName = svPath .. "\\" .. uin .. ".dat"

				if (file_exists(svFileName) ~= true ) then
				
					print("Starting " .. uin .. "\n")
					dummy = dplus.updateparametertree(domain);
					gen = dplus.generate()
					print("Finished " .. uin .. "\n")
					
					res = dplus.writedata(svFileName, gen)
				
					if not res then
						print("There was a problem with file " .. svFileName .. ".\n");
					end
				end
			end
		end

		collectgarbage()

	end
end
