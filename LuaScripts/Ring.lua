Information = {
	Name = "Ring",
	Type = "Symmetry",
	NLP = 1,
	MinLayers = 2,
	MaxLayers = 2,
};

function Populate(p, nlayers)	 
	if (p == nil or nlayers ~= 2 or table.getn(p[1]) ~= 1) then				
		error("Parameter matrix must be 2x1, it is " .. nlayers .. "x" .. table.getn(p[1]));
	end
	
	--error("Params: " .. p[1][1] .. ", " .. p[2][1] .. ", " .. p[3][1]);

	radius				= p[1][1];
	nUnits				= p[2][1];

	angle		= 2.0 * math.pi / nUnits;

	res = {};
	n = 1;
	
	for ind = 1, nUnits do

		x		= radius * math.sin(angle * ind);
		y		= radius * math.cos(angle * ind);
		z		= 0;
		alpha	= 0;
		beta	= 0;
		gamma	= angle * ind;

		--print(heightInd .. " " .. inPitchInd .. ": [" .. x .. ", " .. y ..  ", " .. z .. "]");
		
		res[ind] = {x,y,z,alpha,beta,gamma};
	end

	return res;

end

-----------------------------------------------------
-- UI

-- Optional display parameters
function GetLayerName(index)
	if index == 0 then
		return "Radius";
	elseif index == 1 then
		return "# Units";
	else	
		return "N/A";
	end
end
function GetLayerParameterName(index)
	if index == 0 then
		return "Parameter";
	else
		return "N/A"
	end
end
	
function IsParamApplicable(layer, layerParam)
	return true;
end

function GetDefaultValue(layer, layerParam)
	if layer == 0 then
		return 11.3;
	elseif layer == 1 then
		return 3;
	end
end

	