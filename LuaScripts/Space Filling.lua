Information = {
	Name = "Space Filling Script",
	Type = "Symmetry",
	NLP = 3,
	MinLayers = 3,
	MaxLayers = 3,
};

function Populate(p, nlayers)	 
	if (p == nil or nlayers ~= 3 or table.getn(p[1]) ~= 3) then				
		error("Parameter matrix must be 3x3, it is " .. nlayers .. "x" .. table.getn(p[1]));
	end
	
	--error("Params: " .. p[1][1] .. ", " .. p[2][1] .. ", " .. p[3][1]);

	angles = {0, 0, 0}; -- Angles in radians
	distances = {0, 0, 0}; -- Distances
	Ns = {0, 0, 0}; -- Average repetitions

	local i = 0;
	for i = 1,3 do
		distances[i]	= p[i][1];
		angles[i]		= p[i][2] * math.pi / 180.0;
		Ns[i]			= p[i][3];
		
		print(distances[i] .. " " .. angles[i] * 180 / math.pi .. " " .. Ns[i])
	end
	
	local sa = math.sin(angles[1]);
	local sb = math.sin(angles[2]);
	local sc = math.sin(angles[3]);
	local ca = math.cos(angles[1]);
	local cb = math.cos(angles[2]);
	local cc = math.cos(angles[3]);

	local cat = (ca - cb * cc)/(sb * sc); -- This can be greater than 1 in some cases causing #IND (e.g. angles = {49,60,110})
	
	vecs = {
		{distances[1], 0, 0},
		{distances[2] * cc, distances[2] * sc, 0},
		{distances[3] * cb, distances[3] * sb * cat, distances[3] * sb * math.sin(math.acos(cat))},	
	}
	
--[[
	print(cat)
	for i = 1,3 do
		table.foreach(vecs[i], print)
	end
]]
	res = {};
	ind = 0;

	for i = 0,Ns[1]-1 do
		for j = 0,Ns[2]-1 do
			for k = 0,Ns[3]-1 do
				ind = ind + 1;
				res[ind] = {
				i * vecs[1][1] + j * vecs[2][1] + k * vecs[3][1],
				i * vecs[1][2] + j * vecs[2][2] + k * vecs[3][2],
				i * vecs[1][3] + j * vecs[2][3] + k * vecs[3][3],
				0, 0, 0}
			end
		end
	end

	return res;

end

-----------------------------------------------------
-- UI

-- Optional display parameters
function GetLayerName(index)
	if index < 3 then
		return "Vector " .. (index+1);
	else	
		return "N/A";
	end
end
function GetLayerParameterName(index)
	if index == 0 then
		return "Distance";
	elseif index == 1 then
		return "Angle";
	elseif index == 2 then
		return "Repetitions";
	else
		return "N/A"
	end
end
	
function IsParamApplicable(layer, layerParam)
	return true;
end

function GetDefaultValue(layer, layerParam)
	if layerParam == 0 then
		return 2.5;
	elseif layerParam == 1 then
		return 90;
	elseif layerParam == 2 then
		return 2;
	end
	return -1;
end

	