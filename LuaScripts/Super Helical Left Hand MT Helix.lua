Information = {
	Name = "Super Helical Left Hand MT Helix",
	Type = "Symmetry",
	NLP = 1,
	MinLayers = 7,
	MaxLayers = 7,
};

function Populate(p, nlayers)	 
	if (p == nil or nlayers ~= 7 or table.getn(p[1]) ~= 1) then				
		error("Parameter matrix must be 7x1, it is " .. nlayers .. "x" .. table.getn(p[1]));
	end
	
	--error("Params: " .. p[1][1] .. ", " .. p[2][1] .. ", " .. p[3][1]);

	radius				= p[1][1];
	pitch				= p[2][1];
	unitsPerPitch		= p[3][1];
	unitsInPitch		= p[4][1];
	discreteHeight		= p[5][1];
	numHelixStarts		= p[6][1];
	superHelicalPitch	= p[7][1];
	
	longitudinalSpacing = (pitch * 2.0 / numHelixStarts);
	
	angle		= 2.0 * math.pi / unitsPerPitch;
	if(superHelicalPitch >  0.00001) then
		-- angleShift is the amount in radians per dimer by which single longitudinal layer misses the 2\pi mark
		angleShift = (2.0 * math.pi * longitudinalSpacing) / (superHelicalPitch * unitsPerPitch);
	else
		angleShift = 0.0;
	end
	
	res = {};
	n = 1;
	m = 1;
	
	ind = 0;

	for heightInd = 0, discreteHeight-1 do

		initialLayerShift = math.fmod(heightInd * (2.0 / numHelixStarts) * angleShift * unitsPerPitch, 2. * math.pi);
		
		hUnitsPerPitch = 2.0 * math.pi / (angle + angleShift * unitsPerPitch);

		initialZShift = heightInd * longitudinalSpacing;
		
		for inPitchInd = 0, unitsInPitch - 1 do
			theta = initialLayerShift + inPitchInd * (angle + angleShift);
			x		= radius * math.sin(theta);
			y		= radius * math.cos(theta);
			z		= initialZShift + (inPitchInd / unitsPerPitch) * pitch;
			alpha	= 0;
			beta	= 0;
			gamma	= 90. - 180. * theta / math.pi;

			--print(heightInd .. " " .. inPitchInd .. ": [" .. x .. ", " .. y ..  ", " .. z .. "]");
			
			res[ind+1] = {x,y,z,alpha,beta,gamma};
			ind = ind + 1;
		end
	end

	return res;

end

-----------------------------------------------------
-- UI

-- Optional display parameters
function GetLayerName(index)
	if index == 0 then
		return "Radius";
	elseif index == 1 then
		return "Pitch";
	elseif index == 2 then
		return "Units per Pitch";
	elseif index == 3 then
		return "Units in Pitch";
	elseif index == 4 then
		return "Discrete Height";
	elseif index == 5 then
		return "# Helix Starts";
	elseif index == 6 then
		return "Super Helical Pitch";
	else	
		return "N/A";
	end
end
function GetLayerParameterName(index)
	if index == 0 then
		return "Parameter";
	else
		return "N/A"
	end
end
	
function IsParamApplicable(layer, layerParam)
	return true;
end

function GetDefaultValue(layer, layerParam)
	if layer == 0 then
		return 11.3;
	elseif layer == 1 then
		return 12.1;
	elseif layer == 2 then
		return 13.0;
	elseif layer == 3 then
		return 13.0;
	elseif layer == 4 then
		return 8;
	elseif layer == 5 then
		return 3;
	elseif layer == 6 then
		return 0.0;
	end
end

	