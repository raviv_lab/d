Information = {
	Name = "Simple Grid",
	Type = "Symmetry",
	NLP = 2,
	MinLayers = 3,
	MaxLayers = 3,	
};

function Populate(p, nlayers)	 
	if (p == nil or nlayers ~= 3 or table.getn(p[1]) ~= 2) then				
		error("Parameter matrix must be 3x2, it is " .. nlayers .. "x" .. table.getn(p[1]));
	end
	
	--error("Params: " .. p[1][1] .. ", " .. p[2][1] .. ", " .. p[3][1]);

	distA = p[1][2];
	distB = p[2][2];
	distC = p[3][2];
	res = {};
	n = 1;
	
	for x=1,p[1][1] do
		for y=1,p[2][1] do
			for z=1,p[3][1] do
					res[n] = {(x - 1) * distA, 
					          (y - 1) * distB, 
							  (z - 1) * distC, 
							  0, 0, 0};
					n = n + 1;
			end
		end
	end

	return res;
end

-----------------------------------------------------
-- UI

-- Optional display parameters
function GetLayerName(index)
	if index == 0 then
		return "X Axis";
	elseif index == 1 then
		return "Y Axis";
	elseif index == 2 then
		return "Z Axis";
	else	
		return "N/A";
	end
end
function GetLayerParameterName(index)
	if index == 0 then
		return "Repetitions";
	elseif index == 1 then
		return "Distance";
	else
		return "N/A"
	end
end
	
function IsParamApplicable(layer, layerParam)
	return true;
end

function GetDefaultValue(layer, layerParam)
	if layerParam == 0 then
		if layer == 2 then -- Z Axis
			return 1;
		end
		return 2;
	elseif layerParam == 1 then
		return 1.23;
	end
end
