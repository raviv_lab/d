Information = {
	Name = "T = 7 vertices",
	Type = "Symmetry",
	NLP = 1,
	MinLayers = 1,
	MaxLayers = 1,
};

function Populate(p, nlayers)	 
	if (p == nil or nlayers ~= 1 or table.getn(p[1]) ~= 1) then				
		error("Parameter matrix must be 1x1, it is " .. nlayers .. "x" .. table.getn(p[1]));
	end
	
	--error("Params: " .. p[1][1] .. ", " .. p[2][1] .. ", " .. p[3][1]);

	radius		= p[1][1];
	
	
	coords = {{	1.66E-07,	11.18851987	,18.10340595,	328.2825312,	1.09E-05,	260.5105545},
{	-1.66E-07,	-11.18851987,	18.10340595,	31.71746883,	359.9999891,	8.4894455},
{	-1.66E-07,	11.18851987,	-18.10340595,	211.7174688	,359.9999891,	9},
{	1.66E-07,	-11.18851987,	-18.10340595,	148.2825312,	1.09E-05,	260.51055451},
{	18.10340595,	1.66E-07,	11.18851987,	359.9999792,	58.28253117,	278.4894278},
{	18.10340595,	-1.66E-07,	-11.18851987,	179.9999792,	58.28253117,	278.4894278},
{	-18.10340595,	-1.66E-07,	11.18851987,	2.08E-05,	301.7174688	,170.5105722},
{	-18.10340595,	1.66E-07,	-11.18851987,	180.0000208,	301.7174688,	170.5105722},
{	11.18851987,	18.10340595,	1.66E-07,	270.0000129,	31.71746883,	98.4894523},
{	-11.18851987,	18.10340595,	-1.66E-07,	269.9999871,	328.2825312,	350.5105477},
{	11.18851987,	-18.10340595,	-1.66E-07,	90.00001286,	31.71746883,	98.489452258},
{	-11.18851987,	-18.10340595,	1.66E-07,	89.99998714	,328.2825312,	350.5105477},
	}
	
	n = 1;
	reps = 12;
	res = {};
	for n=1,reps do
		res[n] = {radius  * coords[n][1], radius  * coords[n][2], radius  * coords[n][3], coords[n][4], coords[n][5], coords[n][6]}
		n = n + 1;
	end

	return res;
end

-----------------------------------------------------
-- UI

-- Optional display parameters
function GetLayerName(index)
	if index == 0 then
		return "Radius";
	else	
		return "N/A";
	end
end
function GetLayerParameterName(index)
	if index == 0 then
		return "Parameter";
	else
		return "N/A"
	end
end
	
function IsParamApplicable(layer, layerParam)
	return true;
end

function GetDefaultValue(layer, layerParam)
	if layer == 0 then
		return 1.0;
	end
end
