function trim(s) -- trim4
	return s:match"^%s*(.*)":match"(.-)%s*$"
end
function cout(s)
	print     (s .. "\n");
--              msgbox (s .. "\n");
end
function fif(condition, if_true, if_false)
	if condition then return if_true else return if_false end
end
dplus.openconsole()
stateFName = "D:\\MT\\D+\\Basic structure - No solvation.state"
svPath = "D:\\MT\\D+\\"

-- Load the proper state
file = io.open(stateFName, "r");
if not file then
	print  ("File not found  (" .. stateFName .. ")");
	msgbox ("File not found  (" .. stateFName .. ")");
	return;
end
re = dplus.load(stateFName)
file.close(file)

-- #tree.Models[1].Children[1].Children[1].Children == 3
-- tree.Models[1].Children[1].Children[1].Type == Manual Symmetry
-- tree.Models[1].Children[1].Type == Space-filling Symmetry
-- tree.Models[1].Type == Scripted Symmetry


tree = dplus.getparametertree()
-- Radius scan
dRad = 0.1
for rd = -5,5 do
	tree.Models[1].Parameters[1][1] = 11.3  + rd*dRad
	gen = dplus.generate(tree)
	cout  ("Finished radius =".. tree.Models[1].Parameters[1][1] ..  ".");
	svFileName = svPath.."radiusScan radius=".. tree.Models[1].Parameters[1][1]..  ".dat"
	res = dplus.writedata(svFileName, gen)
	collectgarbage()
	if not res then
		print("There was a problem with file " .. svFileName .. ".\n");
	end
end


tree = dplus.getparametertree()
-- Scan dimer angles
dAlp = 3
dBet = 3
dGam = 3
for gam=-10, 10 do
	for bet=-10, 10 do
		for alp=-10, 10 do
			for i = 1, #tree.Models[1].Children[1].Children[1].Children do
				locOld  =  tree.Models[1].Children[1].Children[1].Children[i].Location
				locOld.alpha = fif(alp * dAlp >= 0.0, alp * dAlp,   360.0 + alp * dAlp)
				locOld.beta    =  fif(bet *  dBet >= 0.0, bet *  dBet,360.0 + bet *  dBet)
				locOld.gamma =  fif(gam * dGam >= 0.0, gam * dGam,  360.0 + gam * dGam)
				tree.Models[1].Children[1].Children[1].Children[i].Location = locOld
			end
			--dplus.setparametertree(tree)
			gen = dplus.generate(tree)
			cout  ("Finished alp=".. alp * dAlp ..  " bet=".. bet *  dBet .. " gam=".. gam * dGam..".");
			svFileName = svPath.."angleScan alp=".. alp * dAlp ..  " bet=".. bet *  dBet .. " gam=".. gam * dGam .. ".dat"
			res = dplus.writedata(svFileName, gen)
			collectgarbage()
			if not res then
				print("There was a problem with file " .. svFileName .. ".\n");
			end
		end
	end
end

tree = dplus.getparametertree()
-- Pitch scan
dPitch = 0.1
for pt = -5,5 do
	tree.Models[1].Parameters[2][1] = 12.1  + pt*dPitch
	tree.Models[1].Children[1].Parameters[3][1] = tree.Models[1].Parameters[2][1] * 2.0/3.0
	gen = dplus.generate(tree)
	cout  ("Finished pitch =".. tree.Models[1].Parameters[2][1] ..  ".");
	svFileName = svPath.."pitchScan pitch=".. tree.Models[1].Parameters[2][1] .." displacement=" ..tree.Models[1].Children[1].Parameters[2][1]..  ".dat"
	res = dplus.writedata(svFileName, gen)
	collectgarbage()
	if not res then
		print("There was a problem with file " .. svFileName .. ".\n");
	end
end




