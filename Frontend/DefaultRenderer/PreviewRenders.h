#ifndef __PREVIEW_RENDERS_H
#define __PREVIEW_RENDERS_H

void SphericalPreviewScene();
void SlabPreviewScene();
void CuboidPreviewScene();
void HelixPreviewScene();
void DelixPreviewScene();
void CylindricalPreviewScene();
void CylindroidPreviewScene();

#endif