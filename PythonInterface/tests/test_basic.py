import os
import pytest
from json.decoder import JSONDecodeError
from dplus.CalculationData import CalculationData
from dplus.API import LocalCalculationAPI, WebCalculationAPI
from dplus.CalculationResult import CalculationResult

class TestCalcDataLoad:
    def test_load_from_OK_state(self):
        filename=os.path.abspath("sphere.state")
        data=CalculationData.load_from_state(filename)
        assert isinstance(data, CalculationData)

    def test_load_from_state_not_exist(self):
        with pytest.raises(FileNotFoundError):
            filename = os.path.abspath("XX.state")
            data = CalculationData.load_from_state(filename)

    def test_load_from_notJSON(self):
        with pytest.raises(JSONDecodeError):
            filename = os.path.abspath("test_basic.py")
            data = CalculationData.load_from_state(filename)

    def test_load_from_OK_pdb(self):
        filename=os.path.abspath("tiny.pdb")
        data=CalculationData.load_from_PDB(filename)
        assert isinstance(data, CalculationData)

    def test_load_from_pdb_not_exist(self):
        with pytest.raises(FileNotFoundError):
            filename = os.path.abspath("XX.pdb")
            data = CalculationData.load_from_PDB(filename)

    def test_load_from_not_pdb(self):
        with pytest.raises(NameError):
            filename = os.path.abspath("test_basic.py")
            data = CalculationData.load_from_PDB(filename)

@pytest.mark.incremental
class TestCalcDataInternal:
    def test_state_exists(self):
        filename = os.path.abspath("sphere.state")
        data = CalculationData.load_from_state(filename)
        assert isinstance(data.state, dict)

    def test_exists_model_list(self):
        filename = os.path.abspath("sphere.state")
        data = CalculationData.load_from_state(filename)
        assert isinstance(data.state['Domain']['Populations'][0]['Models'], list)
