import os
import pytest
from json.decoder import JSONDecodeError
from dplus.CalculationData import CalculationData
from dplus.API import LocalCalculationAPI, WebCalculationAPI
from dplus.CalculationResult import CalculationResult
from collections import OrderedDict

@pytest.fixture(scope='session')
def localAPI():
    exe_file = r"C:\Users\Devora\Sources\dplus\x64\Release"
    api = LocalCalculationAPI(exe_file)
    return api

@pytest.fixture(scope='session', params=["sphere.state"])
def state(request, localAPI):
    print(request.param)
    state_file = os.path.abspath(request.param)
    data = CalculationData.load_from_state(state_file)
    result = localAPI.generate(data)
    return {'result':result, 'data':data}

@pytest.fixture(scope='session', params=["tiny.pdb"])
def pdb(request, localAPI):
    print(request.param)
    pdb_file = os.path.abspath(request.param)
    data = CalculationData.load_from_PDB(pdb_file)
    result = localAPI.generate(data)
    return {'result':result, 'data':data}

def test_len1(state):
    assert (len(state['result'].graph) == len(state['data'].x))

def test_len2(pdb):
    assert (len(pdb['result'].graph) == len(pdb['data'].x))

def test_expected_outcome(state):
    results_vs_expected(state['result'].graph, 'sphere.out')

def results_vs_expected(res_graph, expected_file):
    headers=[]
    exp_graph=OrderedDict()
    with open(expected_file) as expected:
        for line in expected:
            if len(line.strip())==0:
                continue
            if line.startswith("#"):
                headers.append(line)
            else:
                x,y=line.split()
                exp_graph[float(x)]=float(y)
    for (k, v), (k2, v2) in zip(res_graph.items(), exp_graph.items()):
        assert(check_tuple(k, v, k2, v2))


def close_enough(value1, value2, tolerance=0.9):
    if (value1 > value2):
        ratio = float(value1) / value2
    else:
        ratio = float(value2) / value1

    if (ratio - 1.0 > tolerance):
        return False
    return True

def check_tuple(ox, oy, ex, ey):
    if ox != ex:
        return False
    if not close_enough(oy, ey):
        return False
    return True
