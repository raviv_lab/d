The DPLus Python Wrapper
=================

Version history:
------------
3.0.5
-----
Made FittingPreferences and DomainPreferences into classes, with validation of properties. This allows for user to be warned if they submit properties that aren't valid.
Reinstated load_from_pdb in GenerateInput

3.0.3
-----
*small fixes to enable webRunner to work again

3.0.2
-----
* Rename load_from_state_file to load_from_state
* Generate a new temporary session directory each time CalculationInput is created.
* Correctly take use_grid from models.

3.0.1
-----
* Add the license back. 

3.0.0
-----
* Bug fixes
* Better fit interface.
* Disabled Python fit.
* Version number 

0.3.5
----
* Encode and decode infinities better.
* Handle generate jobs better
* Fixed to fit

0.3.4
----
* Fixed a minor bug causing Fit to lock if there was a problem with the generate executable.

0.3.3
----
* Added Fitting in Python (just one optimization method)
* Added a Python representation of all the models


0.3.0
----
* Added tests
* Added access to the state and other input properties.
* Better error reporting if files do not exist.
* Allow getting model by name from the input.


0.2.2
----
* Create temporary session directories properly on both Windows and Linux.

0.2.1
----
* Start the generate processes with the proper current working directory.
* Raise an exception in case of generation errors.
* Added a meaningful README

0.2.0
----
* Web interface
* Return Amplitudes
* Put sessions in a temporary directory by default.
* Create a state from a PDB file

0.1.0
----
First version
* Return Amplitudes
* Put sessions in a temporary directory by default.
* Create a state from a PDB file

0.1.0
----
First version
