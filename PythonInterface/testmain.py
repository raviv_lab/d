from __future__ import print_function

import json
import time

from dplus.CalculationInput import FitInput, GenerateInput
from dplus.CalculationRunner import LocalRunner, WebRunner

def test_local():
    exe_directory = r"..\x64\release"
    sess_directory = r"C:\Users\devora.CHELEM\Sources\dplus_master_branch\WebApplication\media\users\local_test"
    return LocalRunner(session_directory=sess_directory)

def test_web():
    url=r'http://localhost:8000/'
    token='4bb25edc45a1f5e5cd9057754880acd043f44eae'
    return WebRunner(url, token)

def test_fit():
    API=test_local()
    caldata = FitInput.load_from_args_file(r'D:\UserData\devora\Sources\dplus\TestDPlus\states\successfulfitargs.json')
    result=API.fit(caldata)
    print(result.graph)


def test_generate():
    API = test_web()
    input = GenerateInput.load_from_state(r'C:\Users\devora.CHELEM\Desktop\metdata\sphere.state')
    print(json.dumps(input.args["args"]["state"], indent=4))
    print("Generating...")
    result = API.generate(input)
    print(result.graph)

def test_generate_with_modifications():
    API = test_web()
    input = GenerateInput.load_from_state('../example files/uhc.state')
    print("Changing radius to 2")
    cylinder = input.get_model("Cylinder")
    cylinder.layer_params[1]['Radius'].value = 3.0

    print("Generating")
    result = API.generate(input)
    print(result.graph)

def test_fit_simple():
    API = test_local()
    input = FitInput.load_from_state(r'C:\Users\devora.CHELEM\Desktop\fit stuff\fitparams.state')
    mutable = input.get_mutable_params()
    result = API.fit(input)
    print("Result is ", result)

def test_fit_complete():
    API = test_local()
    input = GenerateInput.load_from_state('../example files/uhc.state')
    cylinder = input.get_model("Cylinder")

    print("Original radius is ", cylinder.layer_params[1]['Radius'].value)
    result = API.generate(input)

    fit_input = FitInput(input.state, result.graph)
    cylinder = fit_input.get_model("Cylinder")
    cylinder.layer_params[1]['Radius'].value = 2
    cylinder.layer_params[1]['Radius'].mutable = True

    fit_result = API.fit(fit_input)
    fit_input.combine_results(fit_result)
    print("Result radius is ", cylinder.layer_params[1]['Radius'].value)



def test_avi():
    API=test_local()
    caldata = GenerateInput.load_from_state(r"D:\UserData\devora\Sources\dplus\PythonTests\Regression\storage\avi\MscSymmetry PDB/params.state")

    result=API.generate(caldata)
    print(result.graph)

def create_state_with_models():
    from dplus.DataModels import ModelFactory, Population
    from dplus.State import State
    from models import UniformHollowCylinder
    uhc=UniformHollowCylinder()
    s=State()
    s.Domain.populations[0].add_model(uhc)
    API = test_web()
    caldata = GenerateInput(s)
    result=API.generate(caldata)
    print(result.graph)

def test_pdb():
    API=test_local()
    caldata=GenerateInput.load_from_PDB(r'C:\Users\devora.CHELEM\Sources\dplus_master_branch\TestDPlus\states\1JFF.pdb', 5)
    print("created calc_data")
    print(caldata.state.DomainPreferences.grid_size)
    with open(r'C:\Users\devora.CHELEM\Desktop\metdata\pdbstate.state', 'w') as file:
        json.dump(caldata.state.serialize(), file)
    result=API.generate(caldata)
    print(result.graph)


if __name__=='__main__':
    test_pdb()


