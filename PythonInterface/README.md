The DPlus Python API
=================

The D+ Python API allows using the D+ backend from Python, instead of the ordinary D+ application.

The Python API works on both Windows and Linux.

Installation
------------

Installing the Python API is done using PIP:

    pip install dplus-api
    
The API was tested with Python 3.5 and newer. It *may* work with older versions of Python, although Python 2 is probably not supported.
 
Quick example
-------------

Here is an example that generates a signal from a state file created in D+:

    from dplus.CalculationRunner import LocalRunner
    from dplus.CalculationInput import GenerateInput
    
    api = LocalRunner()
    input = GenerateInput.load_from_state(<name of state file>)
    result = api.generate(input)
    
    print (result.graph)
    
`LocalRunner` is a wrapper that runs a local installation of the D+ backend.

`input` is the Python representation of the state file, ready for generation.

`api.generate(input)` generates the signal of the state file.

Finally, `result.graph` is an OrderedDict with the generated signal graph (`graph[x]` is the y value at that coordinate)


Accessing models and properties
-------------------------------

After reading a state file it is possible to access and modify all its properties.
Please refer to the D+ documentation to understand what models and properties are.

You can access a model like so:

    model = input.get_model(<model-name-or-ptr>)
    
Now you can access all the model properties, for example:

* `model.model_ptr` is the model pointer.
* `model.name` is the model's name
* `model.layer_params[0]['Radius']` is the Radius parameter of layer 0.

You can also access parameter values:

   param = model.layer_params[1]['Radius']
   print(param.value)
   param.mutable = True
   
This code prints the Radius parameter value and marks the parameter as Mutable.


Fitting a signal
----------------

It is also possible to fit the parameters to a signal. 
One way to do that is to prepare everything in D+, set the mutable parameters, load a signal file and:
 
     api = LocalRunner()
     input = FitInput.load_from_state(<name of state file>)
     result = api.fit(input)
     input.combine_results(result)
     
This code will load a state file that already contains a reference to a signal file, run the fit process and combine the results back into the state.

It is also possible to modify the state in Python:

     input = GenerateInput.load_from_state(...)
     model = input.get_model(...)
     param = model.extra_params['Solvent']
     param.value = 40
     param.mutable = True
     generate_result = api.generate(input)
     
     fit_input = FitInput(input.state, generate_result.graph)
     api.fit(fit_input)

The various state classes
---------------

### `State`
The `State` class contains the following properties:

* `DomainPreferences`
* `FittingPreferences`

These are dictionaries containing the same kind of information as the D+ dialogs. Refer to the D+ documentation for further information.

* `Domain`

This is the top domain model.

### `Model`
The `Model` class contains the following properties:
 
 * `model_ptr`
 * `name`
 * `Children`
 * `layer_params`
 * `extra_params`
 * `location_params`
 
Not all properties are available for all models. Models without children do not have the `Children` property, for example.

Again, please refer to the D+ documentation for further information.

### `Param`
The `Param` class contains the following:

* `value`
* `sigma`
* `constraints`
* `mutable`

Please refer to the D+ documentation for further information.

