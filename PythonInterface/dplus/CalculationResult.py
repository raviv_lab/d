from collections import OrderedDict


# TODO: Handling of amp files and pdbs

class Result(object):
    def __init__(self, result, calc_data):
        '''
        :param result: the output json from a run of Generate
        :param calc_data: the CalculationInput we are getting a result from.
        required for the x's of the graph, and in the case of Fitting for the parameter tree as well
        '''
        self._raw_result = result
        self._calc_data = calc_data
        self._graph = self._get_graph()

    def to_dplus(self):
        return self._raw_result

    @property
    def graph(self):
        return self._graph

    def _get_graph(self):
        graph = OrderedDict()
        x = self._calc_data.x
        if len(x) != len(self._raw_result['Graph']):
            raise ValueError("Result graph size mismatch")
        for i in range(len(x)):
            graph[x[i]] = self._raw_result['Graph'][i]
        return graph

    @property
    def error(self):
        if "error" in self._raw_result:
            return self._raw_result["error"]
        return {"code":0, "message":"no error"}


class FitResult(Result):
    '''
    A  Result object that also contains a graph and parameter tree
    FitResult overwrites to_dplus to match the Pythonic Fit
    '''

    def __init__(self, result, calc_data):
        super().__init__(result, calc_data)
        self._param_tree = self._calc_data._state.get_simple_json()

    @property
    def parameter_tree(self):
        return self._param_tree


    def to_dplus(self):
        '''
        overwrites Result's to_dplus replacing it with the expected result from Fit
        :return: a dictionary with a Parameter Tree and a graph.
        '''
        dict = {
            "ParameterTree": self.parameter_tree,
            "Graph": self._raw_result['Graph']
        }
        return dict

    def _get_graph(self):
        try:
            return super()._get_graph()
        except:
            return None  # TODO: supposedly, a missing result graph is occasionally acceptable in Fit. This needs to be verified


class GenerateResult(Result):
    '''
    A Result object that also contains a graph and headers
    '''

    def __init__(self, result, calc_data):
        super().__init__(result, calc_data)
        self._parse_headers()

    @property
    def headers(self):
        return self._headers

    def _parse_headers(self):
        header_dict = OrderedDict()
        headers = self._raw_result['Headers']
        for header in headers:
            header_dict[header['ModelPtr']] = header['Header']
        self._headers = header_dict


class CalculationResult(object):
    def __init__(self, calc_data, result):
        try:
            self._raw_result = result['result']
        except KeyError:
            self._raw_result=result
        self._calc_data = calc_data
        self._parse_headers()
        self._get_graph()
        self._get_parameter_tree()

    @property
    def y(self):
        return self._raw_result['Graph']

    def _get_parameter_tree(self):

        try:
            calc_data=self._calc_data
            self._parameter_tree=self._raw_result['ParameterTree']

        except KeyError: #paramtertree does not exist in generate results
            pass

    def _parse_headers(self):
        header_dict = OrderedDict()
        try:
            headers=self._raw_result['Headers']
            for header in headers:
                header_dict[header['ModelPtr']] = header['Header']
        except:
            pass
        self._headers=header_dict

    def _get_graph(self):
        graph = OrderedDict()
        x=self._calc_data.x
        try:
            if len(x)!=len(self._raw_result['Graph']):
                raise ValueError("Result graph size mismatch")
            for i in range(len(x)):
                graph[x[i]] = self._raw_result['Graph'][i]
        except KeyError: #sometimes Fit doesn't return a graph
            print("No graph returned")
        
        self._graph=graph

    @property
    def headers(self):
        return self._headers

    @property
    def graph(self):
        return self._graph

    def get_pdb(self):
        raise NotImplementedError

    def get_amp(self):
        raise NotImplementedError

    @property
    def error(self):
        if "error" in self._raw_result:
            return self._raw_result["error"]
        return {"code":0, "message":"no error"}

