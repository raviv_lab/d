from __future__ import absolute_import

import codecs
import platform
import subprocess
import tempfile
import uuid
import os
import json
import requests
import time
import hashlib

from .CalculationResult import CalculationResult

_is_windows = platform.system() == 'Windows'

def make_session_directories(directory):
    if not os.path.exists(directory):
        os.makedirs(os.path.join(directory, 'pdb'))
        os.makedirs(os.path.join(directory, 'amp'))

class LocalCalculationAPI(object):
    def __init__(self, exe_directory=None, file_directory=tempfile.mkdtemp()):
        self._exe_directory = exe_directory
        if not exe_directory:
            try:
                self._exe_directory = self._find_dplus()
            except Exception as e:
                raise ValueError("Can't locate D+, please specify the D+ backend directory manually", e)
        self._check_exe_directory()


        make_session_directories(file_directory)
        self._file_directory = file_directory
        self._session= os.path.basename(os.path.normpath(file_directory))

    def _find_dplus(self):
        if not _is_windows:
            raise ValueError("Can't find D+ on non Windows machine")

        raise NotImplementedError("D+ automatic lookup not implemented yet")

    def _get_program_path(self, calculation_type):
        program_path = os.path.join(self._exe_directory, calculation_type)
        if _is_windows:
            program_path += ".exe"
        return program_path

    def _check_exe_directory(self):
        generate_path = self._get_program_path('generate')
        fit_path = self._get_program_path('fit')
        if not os.path.isfile(fit_path) or not os.path.isfile(generate_path):
            raise ValueError("The directory %s does not seem to contain the D+ backend executables" % self._exe_directory)


    def _run(self, calc_data, calculation_type):
        # save args:
        filename = os.path.join(self._file_directory, "args.json")
        with open(filename, 'w') as outfile:
            json.dump(calc_data.args, outfile)
            
        # run executable:
        program_path = self._get_program_path(calculation_type)
        if not os.path.isfile(program_path):
            raise FileNotFoundError
        process = subprocess.Popen([program_path, self._file_directory], cwd=self._exe_directory)
        process.communicate()

        # get results:
        filename = os.path.join(self._file_directory, "data.json")
        with codecs.open(filename, 'r', encoding='utf8') as f:
            result = json.load(f)
        return result

    def fit(self, calc_data):
        result = self._run(calc_data, 'fit')
        calc_result = CalculationResult(calc_data, result)
        return calc_result

    def generate(self, calc_data):
        result=self._run(calc_data, 'generate')
        calc_result = CalculationResult(calc_data, result)
        return calc_result

    def get_amp(self, modelptr):
        ptr_string = '%08d.amp' % (int(modelptr))
        filepath = os.path.join(self._file_directory, 'cache', ptr_string)
        result= open(filepath)

    def get_amp(self, modelptr):
        ptr_string = '%08d.pdb' % (int(modelptr))
        filepath = os.path.join(self._file_directory, 'pdb', ptr_string)
        result= open(filepath)


class WebCalculationAPI(object):
    def __init__(self, url, token):
        self.url = url+r'api/v1/'
        self.header = {'Authorization': "Token "+str(token)}
        self.session = "?ID="+str(uuid.uuid4())


    def _check_files(self, calc_data):
        def calc_file_hash(filepath): #taken from stackexchange 1869885/calculating-sha1-of-a-file
                sha = hashlib.sha1()
                with open(filepath, 'rb') as f:
                    while True:
                        block = f.read(2 ** 10)  # Magic number: one-megabyte blocks.
                        if not block: break
                        sha.update(block)
                    return sha.hexdigest().upper()

        #{'files': [{'size': 607866, 'filename': 'D:\\Downloads\\1JFF.PDB', 'hash': '731869F52CAA765452F51629ABBF3E4BD0AC79D2'}]}
        filedict={'files':[]}
        for filename in calc_data.filenames:
            dict={}
            dict['filename']=filename
            dict['size']=os.stat(filename).st_size
            dict['hash']=calc_file_hash(filename)
            filedict['files'].append(dict)
        data = json.dumps(filedict)
        response= requests.post(url=self.url+'files', data=data, headers=self.header)
        statuses= json.loads(response.text)

        files_missing=[]
        for filename in calc_data.filenames:
            if statuses[filename]['status']=='MISSING':
                files_missing.append((filename, statuses[filename]['id']))

        self._upload_files(files_missing)

    def _upload_files(self, filenames):
        for filename, id in filenames:
            url = self.url + 'files/' + str(id)
            files = {'file': open(filename, 'rb')}
            response = requests.post(url=url, headers=self.header, files=files)

    def _run(self, calc_data, calculation_type):
        # check if files
        self._check_files(calc_data)

        # PUT generate
        data = json.dumps(calc_data.args['args'])
        test = requests.put(url=self.url + calculation_type + self.session, data=data, headers=self.header)

        # GET job status
        while True:
            jobstatus = requests.get(url=self.url + 'job' + self.session, headers=self.header)
            res = json.loads(jobstatus.text)
            if res['result']['isRunning'] == False:
                break
            time.sleep(1)

            # GET generate
        response = requests.get(url=self.url + calculation_type + self.session, headers=self.header)
        result = json.loads(response.text)
        return result

    def generate(self, calc_data):
        res=self._run(calc_data, 'generate')
        calc_result= CalculationResult(calc_data, res['result'])
        return calc_result

    def fit(self, calc_data):
        res=self._run(calc_data, 'fit')
        calc_result = CalculationResult(calc_data, res['result'])
        return calc_result



    def get_amp(self, modelptr):
        pass
    def get_pdb(self, modelptr):
        pass

