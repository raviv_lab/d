import json
import sys
import math
from dplus.metadata import meta_models, hardcode_models, type_to_int, model_index_dict, int_to_type


class Constraints:
    def __init__(self, max_val=math.inf, min_val=-math.inf):
        if max_val <= min_val:
            raise ValueError("Constraints' upper bound must be greater than lower bound")
        self.MaxValue = max_val
        self.MinValue = min_val
        self.isConstrained= False
        if max_val!=math.inf or min_val != -math.inf:
            self.isConstrained=True

    @staticmethod
    def from_json(json):
        c=Constraints(json["MaxValue"], json["MinValue"])
        return c

    def to_dict(self):
        return {"MaxValue":self.MaxValue,
                "MinValue":self.MinValue}

class Param:
    def __init__(self, value=0, sigma=0, mutable=False, constraints=Constraints()):
        try:
            self.value = float(value)
            self.sigma = float(sigma)
        except:
            raise ValueError("non-number value creeping into param" + str(value) + " " + str(sigma))
        self.mutable = mutable
        self.constraints = constraints


    def to_dict(self):
        return {"Value": self.value,
                "isMutable": self.mutable,
                "isConstrained": self.constraints.isConstrained,
                "consMin": self.constraints.MinValue,
                "consMax": self.constraints.MaxValue,
                "consMinIndex": -1,
                "consMaxIndex": -1,
                "linkIndex": -1,
                "sigma": self.sigma}

    def __str__(self):
        return str(self.to_dict())

    def __repr__(self):
        return str(self.to_dict())


class Model:
    _model_ptr_index = 0

    def __init__(self):
        self.name = ""
        self.use_grid = False
        self.model_ptr = Model._model_ptr_index
        Model._model_ptr_index += 1

    def _init_from_metadata(self):
        pass

    def serialize(self):
        mydict = {"ModelPtr": self.model_ptr, "Name": self.name, "Use_Grid": self.use_grid,
                  "nExtraParams": 0, "nLayers": 0, "nlp": 0,  # this is default, overwritten by modelWithParams
                  "Type": int_to_type(
                      self.index)}  # i have no idea why type is preceded by a comma or whether it always should be...
        return mydict

    def __str__(self):
        return (str(self.serialize()))

    def load_from_json(self, json):
        # first, check that the type matches the model's type index and everything is in order
        # Domains and populations don't have metadata, their type_index is -1, skip this section
        if self.index == -1:
            pass
        else:
            type_index = type_to_int(json["Type"])

            if type_index != self.index:
                raise ValueError("Model type index mismatch")

        # override instance values
        try:
            self.name = json["Name"]
        except KeyError:
            pass  # we don't require names
        self.model_ptr = json["ModelPtr"]
        self.use_grid = json.get("Use_Grid", False)

    def get_mutable_params(self):
        return None

    def set_mutable_params(self, mut_arr):
        pass

    def basic_json_params(self, useGrid):
        params = []
        # add default location params
        for i in range(6):
            params.append(Param().to_dict())

        # add useGrid
        if useGrid:
            params.append(Param(1).to_dict())
        else:
            params.append(Param(0).to_dict())
        # add number of layers
        params.append(Param(1).to_dict())
        return {
            "ModelPtr": self.model_ptr,
            "Parameters": params,
            "Submodels": []
        }


class ModelWithChildren(Model):
    def __init__(self):
        super().__init__()
        self.Children = []

    def serialize(self):
        mydict = super().serialize()

        mydict.update(
            {
                "Children": [child.serialize() for child in self.Children]
            }
        )
        return mydict

    def __str__(self):
        return (str(self.serialize()))

    def load_from_json(self, json):
        super().load_from_json(json)
        for child in json["Children"]:
            childmodel = ModelFactory.create_model_from_json(child)
            self.Children.append(childmodel)

    def basic_json_params(self, useGrid):
        basic_dict = super().basic_json_params(useGrid)
        for child in self.Children:
            basic_dict["Submodels"].append(child.basic_json_params(useGrid))
        return basic_dict


class ModelWithParams(Model):
    def __init__(self):
        super().__init__()
        self.layer_params = []
        self.location_params = {}
        self.use_grid = False
        self.extra_params = {}
        self._init_from_metadata()

    def _init_from_metadata(self):
        super()._init_from_metadata()

        # layer params:

        layerinfo = self.metadata["layers"]["layerInfo"]
        params = self.metadata["layers"]["params"]
        for layer in layerinfo:
            if layer["index"] == -1:
                # This is just an indication of the default layer when more layers are added
                # it is not an actual layer.
                self._default_layer = Param(value=layer["defaultValues"][param_index])
                continue
            layer_dict = {}
            for param_index, parameter in enumerate(params):
                layer_dict[parameter] = Param(value=layer["defaultValues"][param_index])
            self.layer_params.append(layer_dict)
        self.layer_param_index_map = self.metadata["layers"]["params"]

        # extra params:
        self.extra_param_index_map = []
        e_params = self.metadata["extraParams"]
        for index, param in enumerate(e_params):
            self.extra_param_index_map.append(param["name"])
            self.extra_params[param["name"]] = Param(value=param["defaultValue"])

        location_vals = ["alpha", "beta", "gamma", "x", "y", "z"]
        for val in location_vals:
            self.location_params[val] = Param()

    def parameters_to_json_arrays(self):
        json_dict = {"Parameters": [], "Constraints": [], "Mutables": [], "Sigma": [],
                     "ExtraParameters": [], "ExtraConstraints": [], "ExtraMutables": [], "ExtraSigma": [],
                     "Location": {}, "LocationConstraints": {}, "LocationMutables": {}, "LocationSigma": {}}

        # layerparams
        for layer in self.layer_params:
            param_array = []
            constr_array = []
            mut_array = []
            sigma_array = []
            for i, param_name in enumerate(self.layer_param_index_map):
                param = layer[param_name]
                param_array.append(param.value)
                constr_array.append(param.constraints.to_dict())
                mut_array.append(param.mutable)
                sigma_array.append(param.sigma)
            json_dict["Parameters"].append(param_array)
            json_dict["Constraints"].append(constr_array)
            json_dict["Mutables"].append(mut_array)
            json_dict["Sigma"].append(sigma_array)

        # extraparams
        for i, param_name in enumerate(self.extra_param_index_map):
            param = self.extra_params[param_name]
            json_dict["ExtraParameters"].append(param.value)
            json_dict["ExtraConstraints"].append(param.constraints.to_dict())
            json_dict["ExtraMutables"].append(param.mutable)
            json_dict["ExtraSigma"].append(param.sigma)

        # locationparams
        for param_name in self.location_params:
            param = self.location_params[param_name]
            json_dict["Location"][param_name] = param.value
            json_dict["LocationConstraints"][param_name] = param.constraints.to_dict()
            json_dict["LocationMutables"][param_name] = param.mutable
            json_dict["LocationSigma"][param_name] = param.sigma

        # some additional things that are necessary for stupid reasons:
        try:
            json_dict["nlp"] = len(self.layer_params[0])
        except IndexError:  # there are no layers, only extra params
            json_dict["nlp"] = 0
        json_dict["nLayers"] = len(self.layer_params)
        json_dict["nExtraParams"] = len(self.extra_params)

        return json_dict

    def load_from_json(self, json):
        super().load_from_json(json)

        self.use_grid = json["Use_Grid"]

        for layer_index in range(len(json["Parameters"])):
            for param_index in range(len(json["Parameters"][layer_index])):
                param = Param(value=json["Parameters"][layer_index][param_index],
                              mutable=json["Mutables"][layer_index][param_index],
                              sigma=json["Sigma"][layer_index][param_index],
                              constraints=Constraints.from_json(json["Constraints"][layer_index][param_index]))
                try:
                    self.layer_params[layer_index][self.layer_param_index_map[param_index]] = param
                except IndexError:
                    if len(json["Parameters"]) > self.metadata["layers"]["max"] and self.metadata["layers"][
                        "max"] != -1:
                        raise ValueError(
                            "Not allowed to set more than " + str(self.metadata["layers"]["max"]) + " layers")

                    # otherwise go ahead and add the layer
                    self.layer_params.append({})
                    self.layer_params[layer_index][self.layer_param_index_map[param_index]] = param

        for param_index in range(len(json["ExtraParameters"])):
            param = Param(value=json["ExtraParameters"][param_index], mutable=json["ExtraMutables"][param_index],
                          sigma=json["ExtraSigma"][param_index],
                          constraints=Constraints.from_json(json["ExtraConstraints"][param_index]))
            self.extra_params[self.extra_param_index_map[param_index]] = param

        for param_index in json["Location"]:
            param = Param(value=json["Location"][param_index], mutable=json["LocationMutables"][param_index],
                          sigma=json["LocationSigma"][param_index],
                          constraints=Constraints.from_json(json["LocationConstraints"][param_index]))
            self.location_params[param_index] = param

    def __str__(self):
        return str(self.serialize())

    def serialize(self):
        mydict = super().serialize()

        mydict.update(
            self.parameters_to_json_arrays()
        )
        return mydict

    def get_mutable_params(self):
        mut_array = []
        # layer params
        for layer in self.layer_params:
            for param_name in layer:
                if layer[param_name].mutable:
                    mut_array.append(layer[param_name])
        # extra params
        for param_name in self.extra_params:
            if self.extra_params[param_name].mutable:
                mut_array.append(self.extra_params[param_name])

        # location params
        for param_name in self.location_params:
            if self.location_params[param_name].mutable:
                mut_array.append(self.location_params[param_name])

        return mut_array

    def set_mutable_params(self, mut_array):
        index = 0
        # layer params
        for layer in self.layer_params:
            for param_name in layer:
                if layer[param_name].mutable:
                    layer[param_name] = mut_array[index]
                    index += 1

        # extra params
        for param_name in self.extra_params:
            if self.extra_params[param_name].mutable:
                self.extra_params[param_name] = mut_array[index]
                index += 1

        # location params
        for param_name in self.location_params:
            if self.location_params[param_name].mutable:
                self.location_params[param_name] = mut_array[index]
                index += 1

        print(index, len(mut_array))

    def basic_json_params(self, useGrid):
        basic_dict = super().basic_json_params(useGrid)
        params = []
        # add location params
        location_vals = ["alpha", "beta", "gamma", "x", "y", "z"]
        for index, val in enumerate(location_vals):
            params.append(self.location_params[val].to_dict())

        # add useGrid
        if useGrid:
            params.append(Param(1).to_dict())
        else:
            params.append(Param(0).to_dict())
        # add number of layers
        params.append(Param(len(self.layer_params)).to_dict())

        # add params:
        for param in self.layer_param_index_map:
            for layer in self.layer_params:
                params.append(layer[param].to_dict())

        # add extra params
        for param in self.extra_param_index_map:
            params.append(self.extra_params[param].to_dict())

        basic_dict["Parameters"] = params
        return basic_dict


class ModelWithFile(Model):
    def __init__(self):
        super().__init__()
        self.filenames = []
        self.filename = ""

    def serialize(self):
        mydict = super().serialize()

        mydict.update(
            {
                "Filename": self.filename,
            }
        )

        try:
            mydict.update(
                {
                    "Centered": self.centered,
                }
            )
        except (AttributeError, KeyError) as err:  # not everything has centered
            pass

        try:
            mydict.update(
                {
                    "AnomFilename": self.anomfilename,
                }
            )
        except (AttributeError, KeyError) as err:  # not everything has an anomfilename
            pass
        return mydict

    def __str__(self):
        return (str(self.serialize()))

    def load_from_json(self, json):
        self.filename = json["Filename"]
        self.filenames.append(self.filename)

        # TODO: various optional additonal fields that really should be handled in a better way
        try:
            self.centered = json["Centered"]
        except (AttributeError, KeyError) as err:  # not everything has centered
            pass

        try:
            self.anomfilename = json["AnomFilename"]
            self.filenames.append(json["AnomFilemame"])
        except (AttributeError, KeyError) as err:  # not everything has an anomfilename
            pass


def get_model_tuple(metadata):
    model_list = []
    if "isLayerBased" in metadata:
        model_list.append(ModelWithParams)
    if metadata["category"] == 9:  # symmetry
        model_list.append(ModelWithChildren)
    if metadata["name"] in model_index_dict:
        model_list.append(ModelWithFile)
    return tuple(model_list)


class ScriptedSymmetry(Model):
    # TODO: this is sufficient for running against existing backend, but does NOT implement running with python fit
    def __init__(self, **fields):
        self.__dict__.update(fields)

    def load_from_json(self, json):
        # print(vars(self))
        self.json = json
        if "Children" in json:
            self.Children = []
            for child in json["Children"]:
                childmodel = ModelFactory.create_model_from_json(child)
                self.Children.append(childmodel)

    def serialize(self):
        return_dict = {}
        for key in self.json:
            return_dict[key] = self.__dict__[key]

        if "Children" in self.json:
            return_dict["Children"] = [child.serialize() for child in self.Children]

        return return_dict

    '''
    def get_mutable_params(self):
        mut_array = []
        # layer params
        for layer in self.layer_params:
            for param_name in layer:
                if layer[param_name].mutable:
                    mut_array.append(layer[param_name])
        # extra params
        for param_name in self.extra_params:
            if self.extra_params[param_name].mutable:
                mut_array.append(self.extra_params[param_name])

        # location params
        for param_name in self.location_params:
            if self.location_params[param_name].mutable:
                mut_array.append(self.location_params[param_name])

        return mut_array

    def set_mutable_params(self, mut_array):
        index = 0
        # layer params
        for layer in self.layer_params:
            for param_name in layer:
                if layer[param_name].mutable:
                    layer[param_name] = mut_array[index]
                    index += 1

        # extra params
        for param_name in self.extra_params:
            if self.extra_params[param_name].mutable:
                self.extra_params[param_name] = mut_array[index]
                index += 1

        # location params
        for param_name in self.location_params:
            if self.location_params[param_name].mutable:
                self.location_params[param_name] = mut_array[index]
                index += 1

        print(index, len(mut_array))

    def basic_json_params(self, useGrid):
        basic_dict = super().basic_json_params(useGrid)
        params=[]
        # add location params
        location_vals = ["alpha", "beta", "gamma", "x", "y", "z"]
        for index, val in enumerate(location_vals):
            params.append(self.location_params[val].to_dict())

        #add useGrid
        if useGrid:
            params.append(Param(1).to_dict())
        else:
            params.append(Param(0).to_dict())
        # add number of layers
        params.append(Param(len(self.layer_params)).to_dict())

        #add params:
        for param in self.layer_param_index_map:
            for layer in self.layer_params:
                params.append(layer[param].to_dict())

        #add extra params
        for param in self.extra_param_index_map:
            params.append(self.extra_params[param].to_dict())

        basic_dict["Parameters"]=params
        return basic_dict
    '''


class ModelFactory:
    models_arr = []
    from types import ModuleType
    models = ModuleType('models')
    sys.modules['models'] = models

    @classmethod
    def add_model(cls, metadata):
        no_space_name = "".join(metadata["name"].split())
        no_space_name = no_space_name.replace("-", "")

        modeltuple = get_model_tuple(metadata)

        # replace name with type_name
        metadata["type_name"] = metadata.pop("name")
        metadata["metadata"] = metadata.copy()
        myclass = type(no_space_name, modeltuple,
                       metadata)

        ModelFactory.models_arr.append(myclass)
        setattr(ModelFactory.models, no_space_name, myclass)

    @classmethod
    def create_model_from_json(cls, json):
        model_index_str = json["Type"]
        if model_index_str in ["Scripted Geometry", "Scripted Model"]:
            raise NotImplemented(
                "Tal says:Scripted models and geometries are remnants of a yet unimplemented feature (script models, e.g., written in Python). They should be obliterated from existence for now, only to be revived if python models work.")

        if model_index_str == "Scripted Symmetry":
            m = ScriptedSymmetry(**json)
            m.load_from_json(json)
            return m

        model_index = type_to_int(model_index_str)

        for model in ModelFactory.models_arr:  # TODO: Turn this into a dictionary at some point
            if model.index == model_index:
                m = model()
                m.load_from_json(json)
                return m

        raise ValueError("Model not found")

    @classmethod
    def create_model(cls, name_or_index):
        no_space_name = "_".join(name_or_index.split())
        for model in ModelFactory.models_arr:
            if model.type_index == name_or_index or model.type_name == name_or_index or model.type_name == no_space_name:
                return model

        raise ValueError("Model not found")


class Population(ModelWithChildren):
    index = -1

    def __init__(self):
        super().__init__()
        self.population_size = 0
        self.population_size_mut = False

    @property
    def models(self):
        return self.Children

    def add_model(self, model):
        self.models.append(model)

    def serialize(self):
        mydict = super().serialize()
        mydict["Models"] = mydict.pop("Children")

        newdict = {
            "PopulationSize": self.population_size,
            "PopulationSizeMut": self.population_size_mut,
            "ModelPtr": self.model_ptr,
            "Models": mydict["Models"]
        }

        return newdict

    def load_from_json(self, json):
        self.model_ptr = json["ModelPtr"]
        for model in json["Models"]:
            self.Children.append(ModelFactory.create_model_from_json(model))
        self.population_size = json["PopulationSize"]
        self.population_size_mut = json["PopulationSizeMut"]
        self.population_size_param = Param(value=self.population_size, mutable=self.population_size_mut)


class Domain(ModelWithChildren):
    index = -1

    def __init__(self):
        super().__init__()
        self.scale = 1
        self.scale_mut = False
        self.geometry = "Domains"
        self.populations.append(Population())

    @property
    def populations(self):
        return self.Children

    def serialize(self):
        # we need to completely override the dictionary returned by model
        # (which includes nlayers and other extraneous fields
        mydict = super().serialize()
        mydict["Populations"] = mydict.pop("Children")

        newdict = {
            "ModelPtr": self.model_ptr,
            "Scale": self.scale,
            "ScaleMut": self.scale_mut,
            "Geometry": self.geometry,
            "Populations": mydict["Populations"]
        }

        return newdict

    def load_from_json(self, json):
        self.populations[:]=[] #by default Domain creates an empty population. However if we are loading from json we don't want this empty population
        for population in json["Populations"]:
            popu = Population()
            popu.load_from_json(population)
            self.Children.append(popu)
        self.scale = json["Scale"]
        self.scale_mut = json["ScaleMut"]
        self.scale_param = Param(value=self.scale, mutable=self.scale_mut)
        self.geometry = json["Geometry"]
        self.model_ptr = json["ModelPtr"]
        mytry = 1

    def basic_json_params(self, useGrid):
        basic_dict = super().basic_json_params(useGrid)
        # we need to add in parameters to the domain
        basic_dict["Parameters"].append(self.scale_param.to_dict())
        for population in self.Children:
            basic_dict["Parameters"].append(population.population_size_param.to_dict())
        return basic_dict


# load all existing models when module is loaded
for model in hardcode_models:
    ModelFactory.add_model(model)

for model in meta_models:
    ModelFactory.add_model(model)
