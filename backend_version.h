//
// This files contains the version of the backend.
//
// It will be updated manually and replaces the old gitrev.h.
//

#ifndef BACKEND_VERSION_H
#define BACKEND_VERSION_H

// The version is used in RC files for resources, so it must be broken into elements
#define BACKEND_VERSION_MAJOR 3
#define BACKEND_VERSION_MINOR 2
#define BACKEND_VERSION_REVISION 1
#define BACKEND_VERSION_BUILD 15


// Double macro expansion taken from here: http://stackoverflow.com/a/5459929/871910
// Lengthy macro names are here to prevent pollution the global namespace with these macro names
#define BACKEND_VERSION_STR_HELPER(x) #x
#define BACKEND_VERSION_STR(x) BACKEND_VERSION_STR_HELPER(x)

#define BACKEND_VERSION BACKEND_VERSION_STR(BACKEND_VERSION_MAJOR) ", " BACKEND_VERSION_STR(BACKEND_VERSION_MINOR) ", " BACKEND_VERSION_STR(BACKEND_VERSION_REVISION) ", " BACKEND_VERSION_STR(BACKEND_VERSION_BUILD)


#endif
