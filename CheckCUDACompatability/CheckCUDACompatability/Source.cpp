
#include <iostream>
#include <windows.h>
#include <string>

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

std::string GetLastErrorAsString()
{
	//Get the error message, if any.
	DWORD errorMessageID = ::GetLastError();
	if (errorMessageID == 0)
		return std::string(); //No error message has been recorded

	LPSTR messageBuffer = nullptr;
	size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

	std::string message(messageBuffer, size);

	//Free the buffer.
	LocalFree(messageBuffer);

	return message;
}

typedef void(*funcType)();

#include "../cuda/Header.h"

int main()
{
	std::cout << "Started program" << std::endl;

	HMODULE hMod = NULL;

	{
		pure_cpp_func();

		cpp_call_cuda_api();
	}
	
	{
		std::string dllName = "cuda.dll";
		hMod = LoadLibraryA(dllName.c_str());
		if (!hMod)
		{

			std::cout
				<< "Was unable to load \"" << dllName << "\".\n"
				<< GetLastErrorAsString()
				<< std::endl;
			return 0;
		}
		std::cout << "Loaded \"" << dllName << "\"." << std::endl;

		std::cout << "\nSleeping for 1 second..." << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(1));
		std::cout << "Awake!" << std::endl;

		funcType pure_cpp_func = (funcType)GetProcAddress(hMod, "pure_cpp_func");

		if (!pure_cpp_func)
		{
			std::cout << "Failed to load pure_cpp_func\n"
				<< GetLastErrorAsString()
				<< std::endl;
			return 0;
		}

		pure_cpp_func();

		std::cout << "\nSleeping for 1 second..." << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(1));
		std::cout << "Awake!" << std::endl;

		funcType cpp_call_cuda_api = (funcType)GetProcAddress(hMod, "cpp_call_cuda_api");
		if (!cpp_call_cuda_api)
		{
			std::cout << "Failed to load cpp_call_cuda_api\n"
				<< GetLastErrorAsString()
				<< std::endl;

			return 0;
		}

		cpp_call_cuda_api();
	}
	return 0;
}