#include "Header.h"

#include <cuda_runtime.h>

#include <iostream>

#define gpuErrchk(ans) gpuAssert((ans), __FILE__, __LINE__);
inline cudaError_t gpuAssert(cudaError_t code, const char *file, int line, bool abort = false)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
	return code;
}

void pure_cpp_func()
{
	std::cout << "pure_cpp_func" << std::endl;
}

void cpp_call_cuda_api()
{
	std::cout << "cpp_call_cuda_api" << std::endl;

	int devCount;
	cudaError_t t = gpuErrchk(cudaGetDeviceCount(&devCount));

	if (devCount <= 0 || t != cudaSuccess)
	{
		if (t != cudaSuccess)
		{

		}
		else
			std::cout << "No compatible GPU detected." << std::endl;
		return;
	}

	std::cout << "Found " << devCount << " device(s).\n" << std::endl;

	for (int i = 0; i < devCount; i++)
	{
		cudaDeviceProp prop;
		cudaGetDeviceProperties(&prop, i);

		printf("Device Number: %d\n", i);
		printf("  Device name: %s\n", prop.name);
		printf("  Compute capability: %d.%d\n", prop.major, prop.minor);
		printf("  Memory Clock Rate (KHz): %d\n",
			prop.memoryClockRate);
		printf("  Memory Bus Width (bits): %d\n",
			prop.memoryBusWidth);
		printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
			2.0*prop.memoryClockRate*(prop.memoryBusWidth / 8) / 1.0e6);
		
	}
}
