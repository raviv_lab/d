#undef EXPORTED
#undef EXPORTED_BE
#ifdef _WIN32

#ifdef BACKEND
	#define EXPORTED __declspec(dllimport)
#else
	#define EXPORTED __declspec(dllimport)
#endif // BACKEND

#endif

extern "C"
{

	void EXPORTED pure_cpp_func();

	void EXPORTED cpp_call_cuda_api();

}