import json

from django.http import JsonResponse

from dplus.CalculationRunner import LocalRunner
from dplus.CalculationInput import GenerateInput, FitInput
from dplus.State import State
from django.conf import settings
import time

#TODO:
#start_new_job, V
# check_file_status, Vish
# delete_job, Vish
# get_exe_results, V
# create_metadata_file, Vish
# get_job_status V

class JobRunningException(Exception):
    def __init__(self):
        self.value = "there is a job already running in this session"

    def __str__(self):
        return repr(self.value)


def create_metadata_file(folder_path):
    '''
    currently no reason for this not to be called from jobs, but in the future perhaps metadata creation will be via python?
    for now, just gonna call the original
    '''
    from raw_backend.oldjobs import create_metadata_file as cmf
    cmf(folder_path)

def start_new_job(type, Dsession, args):
    local = LocalRunner(settings.EXE_DIR, Dsession.directory)
    try:
        if type=='Fit':
            calc_data = FitInput.web_load(args)
            job= local.fit_async(calc_data)
            Dsession.running_job=job
        if type=='Generate':
            calc_data = GenerateInput.web_load(args)
            job = local.generate_async(calc_data)
            Dsession.running_job=job
        return modify_return_json()
    except JobRunningException:
        res_dict = {"error":{"code": 2, "message": "A job is already running"}}
        return modify_return_json(res_dict)
        raise JobRunningException


def get_exe_results(Dsession):
    '''
    previously: load result files from folder
    now: ??? either continue to load result files from folder, or get the result from API?
    '''

    running_job = Dsession.running_job
    #if running_job.OK:
    return modify_return_json(running_job.get_result())
    #else:
    #return #TODO: some kind of error handling

def get_job_status(Dsession):
    '''
    get the job status json
    '''
    running_job = Dsession.running_job
    #if running_job.OK:
    status=running_job.get_status()
    return modify_return_json(status)
    #else:
    #    return #TODO: some kind of error handling


def delete_job(Dsession):
    '''
    end any currently running job on this session, freeing the session to be used for a new job
    '''
    running_job = Dsession.running_job
    running_job.abort()
    res_dict = {"error": {"code": 1, "message": "The job was manually stopped"}}
    return modify_return_json(res_dict)
    #if running_job.OK:
    #    running_job.abort()
    #    res_dict = {"error": {"code": 1, "message": "The job was manually stopped"}}
    #    return modify_return_json(res_dict)
    #else:
    #    return #TODO: some kind of error handling

def check_file_status(Dsession):
    '''
    check whether current job has finished saving amp and pdb files
    The abpove desription not accurate it busywaits until files are ready
    should possible be handled by LocalAPI
    should it be folded into get_job_status, somehow?
    '''
    running_job = Dsession.running_job
    #if running_job.OK:
    status=False
    while not status:
        status=running_job.get_file_status()
        time.sleep(1)



def get_pdb(Dsession, pointer):
    '''
    this function was handled in views, not in jobs, but it should perhaps be handled differently with a LocalAPI
    '''
    pass

def get_amp(Dsession, pointer):
    '''
    this function was handled in views, not in jobs, but it should perhaps be handled differently with a LocalAPI
    '''
    pass


###FUNCTIONS THAT NO LONGER BELONG IN WEBAPPLICATION####



set_http_code = {0: 200, 1: 200, 2: 500, 3: 404, 4: 500, 5: 500, 6: 404, 7: 400, 8: 500, 9: 400, 10: 404, 11: 400,
                     12: 400, 13: 400, 14: 500, 15: 400, 16: 400, 17: 500, 18: 500, 19: 500, 20: 500}


set_http_reason = {
    0: "OK",
    1: "The job was manually stopped",
    2: "The job is already running",
    3: "The job cannot be found",
    4: "Too many models have been allocated",
    5: "Error accessing backend",
    6: "The file cannot be found",
    7: "Bad model/renderer container",
    8: "The backend does not support this operation",
    9: "Illegal JSON passed to Backend",
    10: "The model was not found within the container or job",
    11: "The requested model is of an incorrect type ",
    12: "The specified model type is invalid or unknown",
    13: "The sub-model or parent model are incompatible with each other",
    14: "The selected model cannot be fit using the model fitter",
    15: "The input parameter tree is invalid",
    16: "The function or method has been called with invalid arguments",
    17: "A fitting job was created, but there are no parameters to change",
    18: "The fitter was unable to find a better set of parameters",
    19: "A general error has occurred (crash/unknown error)",
    20: "Accelerators do not exist on computer (is this a GPU-capable machine?)"
    # 21: "Could not contact server"
    # 22: "Invalid activation code"
    # 23: "Internal server error"
}


def modify_return_json(response_json={'result': ''}):
    jr = JsonResponse(response_json)
    try:
        if response_json["error"]:
            jr.status_code = set_http_code[response_json['error']['code']]
            jr.reason_phrase = set_http_reason[response_json['error']['code']]
    except:
        try:
            if response_json["result"]:
                pass
        except:
            response_json = {"result": response_json}
            jr = JsonResponse(response_json)
    finally:
        return jr