import json

import math
from chelem.ajax.responses import JsonResponseNotAuthorized
from django.conf import settings
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from file_management.syncing_names import fix_filenames
from sendfile import sendfile
import os
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from database_items.models import DplusSession
from raw_backend.jobs import start_new_job, check_file_status, get_exe_results, delete_job, create_metadata_file, \
    get_job_status
from dplus.CalculationRunner import LocalRunner

__author__ = "DevoraW"

class MyDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook(self, obj):
        if "inf" in obj.values() or "-inf" in obj.values():
            for key, value in obj.items():
                if value=="inf":
                    obj[key]=math.inf
                elif value=="-inf":
                    obj[key]=-math.inf
        return obj

def get_body_json(body_bytes):
    body_str = body_bytes.decode('utf-8')
    body_json = json.loads(body_str, cls=MyDecoder)
    return body_json


def metadata(request):
    folder_path = settings.MEDIA_ROOT
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    try:
        filename = folder_path + r"/metadata.json"
        f = open(filename, 'r', encoding='utf8')
    except FileNotFoundError:
        create_metadata_file(folder_path)
        f = open(filename, 'r', encoding='utf8')
    data = json.load(f)
    f.close()
    res_dict = {'result': data}
    return JsonResponse(res_dict)


@api_view(['GET', 'PUT'])
@csrf_exempt
def generate(request):
    Dsession = DplusSession.get_or_create_session(request)
    try:
        if request.method == 'PUT':
            body_json = get_body_json(request.body)
            args = fix_filenames(body_json, request.user)
            return start_new_job('Generate', Dsession, args)
        if request.method == 'GET':
            return get_exe_results(Dsession)
    except:
        jr=JsonResponse({"error": {"code": 5, "message": "Some kind of issue in the webapp"}})
        jr.status_code=500
        return jr



@api_view(['GET', 'PUT'])
@csrf_exempt
def fit(request):
    Dsession = DplusSession.get_or_create_session(request)
    try:
        if request.method == 'PUT':
            body_json = get_body_json(request.body)
            args = fix_filenames(body_json, request.user)
            return start_new_job('Fit', Dsession, args)
        if request.method == 'GET':
            return get_exe_results(Dsession)
    except:
        jr = JsonResponse({"error": {"code": 5, "message": "Some kind of issue in the webapp"}})
        jr.status_code = 500
        return jr

@api_view(['GET', 'DELETE'])
@csrf_exempt
def job(request):
    Dsession = DplusSession.get_or_create_session(request)
    if request.method == 'DELETE':
        return delete_job(Dsession)
    if request.method == 'GET':
        return get_job_status(Dsession)



@api_view(['GET'])
@csrf_exempt
def pdb(request, modelptr):
    Dsession = DplusSession.get_or_create_session(request)
    #check_file_status(Dsession) #TODO
    ptr_string = '%08d.pdb' % (int(modelptr))
    filepath = os.path.join(Dsession.directory, 'pdb', ptr_string)
    # TODO: Check if something bad happened
    return sendfile(request, filepath, attachment=True, attachment_filename='returnedfile.pdb',
                    mimetype='application/octet-stream')


@api_view(['GET'])
@csrf_exempt
def amplitude(request, modelptr):
    Dsession = DplusSession.get_or_create_session(request)
    #check_file_status(Dsession) #TODO
    ptr_string = '%08d.amp' % (int(modelptr))
    filepath = os.path.join(Dsession.directory, 'cache', ptr_string)
    # TODO: Check if something bad happened
    return sendfile(request, filepath, attachment=True, attachment_filename='returnedfile.amp',
                    mimetype='application/octet-stream')


@csrf_exempt
def login_get_token(request):
    '''
    this function is an artifact from when token authentication was preceded by a login
    now that the token is provided from the side of the UI, this code is no longer used
    I am leaving it here because it could still be useful or relevant in the future
    '''
    req = get_body_json(request.body)
    username = req["username"]
    password = req["password"]
    user = authenticate(username=username, password=password)
    if user is None:
        return JsonResponseNotAuthorized()

    token, created = Token.objects.get_or_create(user=user)
    return JsonResponse(dict(token=token.key))


'''
//these are functions used in the former, non-exe based code.
def perform_call(functionName, args={}):
    call=dict(base_call)
    call['function']=functionName
    call['args']=args
    ctx = zmq.Context.instance()
    sock = ctx.socket(zmq.REQ)
    sock.connect('tcp://localhost:5555')
    sock.send_json(call)
    return sock.recv_string()


def return_json(response):
    response_json = json.loads(response)
    if response_json['error']['code'] == 0:
        res_dict={'result':response_json['result']}
        jr=JsonResponse(res_dict)
        print_returned_json(res_dict)
    else:
        res_dict={'error':response_json['error']}
        jr = JsonResponse(res_dict)
        jr.status_code=set_http_code[response_json['error']['code']]
    return jr

base_call = { 'client-id': '1',
	     'client-data': '',
	     'function': '',
	     'args': {} }
'''
