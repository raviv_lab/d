#include "Amplitude.h"
#include "Grid.h"
#include "boost/filesystem/fstream.hpp"
#include "boost/filesystem.hpp"

#include "PeriodicSplineSolver.h"

#include <Eigen/Eigenvalues>

#include <chrono>
//#include <boost/timer/timer.hpp>
//#include <boost/chrono/chrono.hpp>

#include <iostream>
using std::ios;

namespace fs = boost::filesystem;
#define _USE_MATH_DEFINES
#include <math.h>

#ifndef M_2PI
#define M_2PI 6.28318530717958647692528676656
#endif

#pragma region GridOld: The (slightly) optimized std::vector grid

GridOld::GridOld(unsigned short gridSize, double qMax) : Grid(gridSize, qMax) {
	InitializeGrid();
}

void GridOld::InitializeGrid() {
	trnsfm.resize(actualGridSize);

	//////////////////////////////////////////////////////////////////////////////
	double qRange = qmax + double(Extras / 2) * stepSize;	
#pragma omp parallel for schedule(dynamic, trnsfm.size() / (omp_get_num_procs() * 4))
	for(int xi = 0; xi < (int)actualGridSize; xi++) {

          double qx = ((double)(xi) - ((double)(actualGridSize-1) / 2.0)) * stepSize;
		unsigned short yS = 2 * (unsigned short)((sqrt(qRange * qRange - qx * qx) / stepSize)) + 1;
		(trnsfm[xi]).resize(yS);
		double yRange = (double)((yS-1) / 2) * stepSize;
#pragma omp parallel for schedule(dynamic)
		for(int yi = 0; yi < (int)yS; yi++) {
                  double qy = ((double)(yi) - ((double)(yS-1) / 2.0)) * stepSize;
                  unsigned short zS = 2 * (unsigned short)(sqrt(qRange * qRange - qx * qx - qy * qy) / stepSize) + 1;
			(trnsfm[xi][yi]).resize(zS);
		}
	}
	///////////////////////////////////////////////////////////////////////
}

GridOld::GridOld(std::istream& stream, std::string &header) {
	versionNum = 0;
	unsigned int numBytes = 0;

	// Disabled, unnecessary
	//stream.seekg(0, ios::beg);

	char desc[2] = {0};
	unsigned int offset = 0;

	if(stream.peek() == '#') {
		stream.read(desc, 2);
		if(desc[1] == '@') { // We have an offset
			stream.read((char *)&offset, sizeof(unsigned int));
			std::string del;
			getline(stream, del);
		} else if(desc[1] != '\n') {
			std::string tmphead;
			getline(stream, tmphead);
			header.append(tmphead + "\n");
		}
	}

	// Read the header
	std::string head;
	while(stream.peek() == '#') {
		getline(stream, head);
		header.append(head + "\n");
	}

	if(offset > 0) {
		// Skip the header
		stream.seekg(offset, ios::beg);
	} else {
	}

	// Version
	stream >> versionNum;

	// Check if current binary file - no backward compatibility
	if(versionNum < 11)
		return;	// We should change it to use GridV10Old

	stream >> numBytes;
	if(numBytes != sizeof(std::complex<double>))
		return;

	// Fill grid parameters
	//stream >> stepSize;	// Removed for version 11
	stream >> actualGridSize;
	stream >> Extras;

	gridSize = actualGridSize - Extras;
	
	// Ignore the newline
	stream.ignore(100, '\n');
	stream.read((char *)&(stepSize), sizeof(double));

	qmax = stepSize * double(gridSize) / 2.0;

	InitializeGrid();

	if(!InnerImportBinaryData(stream)) {
		gridSize = 0;
		actualGridSize = 0;
		stepSize = 0.0;
		qmax = 0.0;
		return;
	}	
}

GridOld::~GridOld() {
	trnsfm.resize(0);
}

unsigned short GridOld::GetDimX() const {
	return (unsigned short)trnsfm.size();
}

unsigned short GridOld::GetDimY(unsigned short x) const {
	return (unsigned short)trnsfm[x].size();
}

unsigned short GridOld::GetDimZ(unsigned short x, unsigned short y) const {
	return (unsigned short)trnsfm[x][y].size();
}

void GridOld::Set(double x, double y, double z, std::complex<double> val) {
	unsigned short xx, yy, zz;

	xx = (unsigned short)floor((GetDimX() / 2)			+ (x / stepSize));
	yy = (unsigned short)floor((GetDimY(xx) / 2)		+ (y / stepSize));
	zz = (unsigned short)floor((GetDimZ(xx, yy) / 2)	+ (z / stepSize));

	// Correct or throw error?
	if(xx < 0)
		xx = 0;
	if(xx > GetDimX() - 1)
		xx = GetDimX() - 1;
	if(yy < 0)
		yy = 0;
	if(yy > GetDimY(xx) - 1)
		yy = GetDimY(xx) - 1;
	if(zz < 0)
		zz = 0;
	if(zz > GetDimZ(xx, yy) - 1)
		zz = GetDimZ(xx, yy) - 1;

	trnsfm[xx][yy][zz] = val;
}

std::complex<double> GridOld::GetCart(double x, double y, double z) const {
	bool bBoundaryChecks = (qmax - sqrt(x*x + y*y + z*z) ) < 3.0 * stepSize;

	double xd = double(actualGridSize - 1) / 2.0 + x / stepSize;
	int xx = int(floor(xd));
	int xxmax = actualGridSize - 1;
	int xc = xx;
	if ( xx < trnsfm[0].size() ) {
		xc = (trnsfm[0].size() > 0) ? 0 : 1;
	}
	if ( xx > xxmax - 1){
		xc = xxmax - 1;
	} // Base for x

	double yd = double(GetDimY(xc)-1) / 2.0 + y / stepSize;
	int yy = int(floor(yd));
	int yymax = int(GetDimY(xc)-1);
	int yc = yy;
	if ( yy < 0 ) {
		yc = 0;
	}
	if ( yy > yymax ){
		yc = yymax;
	} // Base for y


	double zd = double(GetDimZ(xc, yc)-1) / 2.0 + z / stepSize;
	int zz = int(floor(zd));
	int zc = zz;
	int zzmax = int(GetDimZ(xc, yc)-1);
	if ( zz < 0 ) {
		zc = 0;
	}
	if ( zz > zzmax ){
		zc = zzmax;
	} // Base for z

	// These are necessary mainly when using symmetries
	if(bBoundaryChecks) {
 		if( GetDimY(xc)				> yc + 1 &&
			GetDimY(xc + 1)			> yc + 1 &&
			GetDimZ(xc + 1, yc + 1)	> zc + 1 &&
 			GetDimZ(xc,     yc + 1)	> zc + 1 &&
			GetDimZ(xc + 1, yc)		> zc + 1
 		   ) { // has full cube
			   unsigned short yx1 = GetDimY(xc+1)/2,		// The y=0 index for x+1
				   zx1 = GetDimZ(xc+1, yc  )/2,	// The z=0 index for (x+1, y)
				   zy1 = GetDimZ(xc  , yc+1)/2,	// The z=0 index for (x, y+1)
				   zyx1= GetDimZ(xc+1, yc+1)/2,	// The z=0 index for (x+1, y+1)
				   ySteps = yc - GetDimY(xc)/2,
				   zSteps = zc - GetDimZ(xc, yc)/2;
			   if( GetDimY(xc+1) > (2 * ySteps + 2) && yx1 + ySteps >=0 && 
				   GetDimZ(xc+1, yx1 + ySteps	 ) > zx1  + zSteps + 1 && zx1 + zSteps >= 0 &&
				   GetDimZ(xc+1, yx1 + ySteps + 1) > zyx1 + zSteps + 1 &&
				   GetDimZ(xc, yc + 1) > zy1 + zSteps + 1 &&  
				   zy1 + zSteps >= 0 && zyx1 + zSteps >= 0) { // Second full-cube condition
					   // Calculate normally
					   ;
			   } else {
				   // TODO::Calculate CORRECTLY!!!!
				   return trnsfm[xc][yc][zc];
			   }
		} else {
			// TODO::Calculate CORRECTLY!!!!
			return trnsfm[xc][yc][zc];
		}
	}

	int yx1 = GetDimY(xc+1)/2,			// The y=0 index for x+1
		zx1 = GetDimZ(xc+1, yc  )/2,	// The z=0 index for (x+1, y)
		zy1 = GetDimZ(xc  , yc+1)/2,	// The z=0 index for (x, y+1)
		zyx1= GetDimZ(xc+1, yc+1)/2,	// The z=0 index for (x+1, y+1)
		ySteps = yc - GetDimY(xc)/2,
		zSteps = zc - GetDimZ(xc, yc)/2;

	xd -= double(xx);
	yd -= double(yy);
	zd -= double(zz);

	std::complex<double> c00, c10, c01, c11, c1, c0;
	const std::complex<double> *v000, *v001, *v010, *v011, *v100, *v101, *v110, *v111;
	v000 = &(trnsfm[xc][yc][zc]);
	v001 = v000 + 1; //&(trnsfm[xc][yc][zc+1]);
	v010 = &(trnsfm[xc][yc + 1][zy1 + zSteps]);
	v011 = v010 + 1; //&(trnsfm[xc][yc + 1][zy1 + zSteps + 1]);
	v100 = &(trnsfm[xc + 1][yx1 + ySteps][zx1 + zSteps]);
	v101 = v100 + 1; //&(trnsfm[xc + 1][yx1 + ySteps][zx1 + zSteps + 1]);
	v110 = &(trnsfm[xc + 1][yx1 + ySteps + 1][zyx1 + zSteps]);
	v111 = v110 + 1; //&(trnsfm[xc + 1][yx1 + ySteps + 1][zyx1 + zSteps + 1]);

	c00 = *v000 * (1.0 - xd) + *v100 * xd;
	c10 = *v010 * (1.0 - xd) + *v110 * xd;
	c01 = *v001 * (1.0 - xd) + *v101 * xd;
	c11 = *v011 * (1.0 - xd) + *v111 * xd;

	c0 = c00 * (1.0 - yd) + c10 * yd;
	c1 = c01 * (1.0 - yd) + c11 * yd;

	return (c0 * (1.0 - zd) + c1 * zd);
	
}

void GridOld::Fill(Amplitude *amp, void *prog, void *progArgs, double progMin, double progMax, int *pStop) {
	progressFunc progFunc = (progressFunc)prog;

	const unsigned short dimx = GetDimX();
	const double xFac = (double(dimx - 1) / 2.0) * stepSize;

	int iprog = 0;

#pragma omp parallel for schedule(dynamic, dimx / (omp_get_num_procs() * 4))
	for(int xi = 0; xi < (int)dimx; xi++) {
		double qx, qy, qz;
		double zFac, yFac;
		const unsigned short dimy = GetDimY(xi);
		qx = (double(xi) * stepSize- xFac);

		// Stop check
		if(pStop && *pStop)
			continue;

		// Progress report
		if(progFunc) {
			#pragma omp critical
 			{			
				progFunc(progArgs, (progMax - progMin) * (double(++iprog) / double(dimx)) + progMin);
 			}
		}

		yFac = (double(dimy - 1) / 2.0) * stepSize;
		for(unsigned short yi = 0; yi < dimy; yi++) {
			const unsigned short dimz = GetDimZ(xi, yi);
			qy = (double(yi) * stepSize - yFac);

			zFac = (double(dimz - 1) / 2.0) * stepSize;
			for(unsigned short zi = 0; zi < dimz; zi++) {
				qz = (double(zi) * stepSize - zFac);
				
				
				trnsfm[xi][yi][zi] = amp->calcAmplitude(qx, qy, qz);				
			}
		}
	}
}

bool GridOld::ExportBinaryData(std::ostream& stream, const std::string& header) const {
	if(!stream)
		return false;

	// Versions 0 and 1 are text
	// Version 1 has "\n" in the data region
	// Versions >= 10 contain the data as binary
	// Version 4 is reserved for files converted from formated to binary
	// Version 2 is reserved for files converted from binary to formatted
	// Version 11 uses the new form of grid and removed other crap

	unsigned int versionNum = 11;

	char DESCRIPTOR[3] = "#@";
	unsigned int headlen = (unsigned int)(2 * sizeof(char) + sizeof(unsigned int) + sizeof(char) + 
		                   header.length() * sizeof(char) + sizeof(char)); // descriptor + \n + header length + \n

	stream.write(DESCRIPTOR, 2 * sizeof(char));
	stream.write((const char *)&headlen, sizeof(unsigned int)); // 32-bit, always
	
	stream << "\n" << header << "\n";

	// File version number
	stream << versionNum << "\n";
	// Bytes per element
	stream << sizeof(std::complex<double>) << "\n";
	// Delta Q
	//stream << stepSize << "\n";
	// Size of trnsfm --> txSize
	unsigned short dimx = GetDimX();
	stream << dimx << "\n";	
	stream << Extras << "\n";
	stream.write((const char *)&(stepSize), sizeof(double));

	// Later, for file-based grids
	/*
	headlen = (unsigned int)stream.tellp(); // Say we're 32-bit
	stream.seekp(2, ios::beg);
	stream.write((const char *)&headlen, sizeof(unsigned int)); // 32-bit, always
	stream.seekp(headlen, ios::beg);
	*/

	unsigned short xS, yS;
	xS = GetDimX();
	for(unsigned short i = 0; i < xS; i++) {
		yS = GetDimY(i);
		for(unsigned short j = 0; j < yS; j++) {				
			stream.write((const char *)&(trnsfm[i][j][0]), sizeof(std::complex<double>) * GetDimZ(i, j));
		}
	}

	return true;
}

void GridOld::Add(Grid* other, double scale) {
	
	GridOld *rhs = dynamic_cast<GridOld*>(other);
	if(!rhs)
		return;

	if(GetSize() != rhs->GetSize())
		return;

	const unsigned short dimx = GetDimX();

#pragma omp parallel for
	for(int xi = 0; xi < (int)dimx; xi++) {
		const unsigned short dimy = GetDimY(xi);

		for(unsigned short yi = 0; yi < dimy; yi++) {
			const unsigned short dimz = GetDimZ(xi, yi);

			for(unsigned short zi = 0; zi < dimz; zi++) {
				trnsfm[xi][yi][zi] += scale * rhs->trnsfm[xi][yi][zi];
			}
		}
	}

}

void GridOld::Scale(double scale) {
	const unsigned short dimx = GetDimX();

#pragma omp parallel for
	for(int xi = 0; xi < (int)dimx; xi++) {
		const unsigned short dimy = GetDimY(xi);

		for(unsigned short yi = 0; yi < dimy; yi++) {
			const unsigned short dimz = GetDimZ(xi, yi);

			for(unsigned short zi = 0; zi < dimz; zi++) {
				trnsfm[xi][yi][zi] *= scale;
			}
		}
	}


}

bool GridOld::Validate() const {
	if(gridSize == 0)
		return false;

	bool res = true;

	const unsigned short dimx = GetDimX();

#pragma omp parallel for schedule(dynamic, dimx / (omp_get_num_procs() * 8))
	for(int xi = 0; xi < (int)dimx; xi++) {
		if(!res)
			continue;

		const unsigned short dimy = GetDimY(xi);

		for(unsigned short yi = 0; yi < dimy; yi++) {
			const unsigned short dimz = GetDimZ(xi, yi);

			for(unsigned short zi = 0; zi < dimz; zi++) {
				if(trnsfm[xi][yi][zi] != trnsfm[xi][yi][zi] || 
					trnsfm[xi][yi][zi].real() == std::numeric_limits<double>::infinity() ||
					trnsfm[xi][yi][zi].real() == -std::numeric_limits<double>::infinity() ||
					trnsfm[xi][yi][zi].imag() == std::numeric_limits<double>::infinity() ||
					trnsfm[xi][yi][zi].imag() == -std::numeric_limits<double>::infinity()) {
						res = false;
						break;
				}
			}
			if(!res)
				break;
		}
	}
	if(!res) {
		std::cout << "Grid contains invalid numbers!" << std::endl;
	}

	return res;
}

bool GridOld::ImportBinaryData(std::istream &stream, std::string &header) {
	unsigned int versionNum = 0;
	unsigned int numBytes = 0;

	// Disabled, unnecessary
	//stream.seekg(0, ios::beg);

	char desc[2] = {0};
	unsigned int offset = 0;

	if(stream.peek() == '#') {
		stream.read(desc, 2);
		if(desc[1] == '@') { // We have an offset
			stream.read((char *)&offset, sizeof(unsigned int));
			std::string del;
			getline(stream, del);
		} else if(desc[1] != '\n') {
			std::string tmphead;
			getline(stream, tmphead);
			header.append(tmphead + "\n");
		}
	}

	// Read the header
	std::string head;
	while(stream.peek() == '#') {
		getline(stream, head);
		header.append(head + "\n");
	}

	if(offset > 0) {
		// Skip the header
		stream.seekg(offset, ios::beg);
	} else {
	}

	// Version
	stream >> versionNum;

	// Check if a binary file
	if(versionNum != 4 || versionNum < 10)
		return false;
	
	stream >> numBytes;
	if(numBytes != sizeof(std::complex<double>))
		return false;

	// Delta Q
	unsigned short tmpGridSize, tmpExtras;
	stream >> tmpGridSize;
	if(versionNum > 10)
		stream >> tmpExtras;
	
	if(actualGridSize != tmpGridSize)
		return false;
	if(versionNum > 10 && Extras != tmpExtras)
		return false;

	// Ignore the newline
	stream.ignore(100, '\n');
	stream.read((char *)&(stepSize), sizeof(double));

	return InnerImportBinaryData(stream);
}

bool GridOld::InnerImportBinaryData(std::istream& stream) {
	const unsigned short dimx = GetDimX();
	long long pp = 0;


	// Amplitude values
	const unsigned short sX = GetDimX();
	for(unsigned short i = 0; i < sX; i++) {
		const unsigned short sY = GetDimY(i);
		for(unsigned short j = 0; j < sY; j++) {
			stream.read((char *)&(trnsfm[i][j][0]), sizeof(std::complex<double>) * GetDimZ(i, j));
			pp = stream.tellg();
			if(stream.eof()) {
				pp = pp;
			}
		}
	}

	return !stream.eof();
}

bool GridOld::IndicesToVectors(unsigned short xi, unsigned short yi, unsigned short zi, OUT double &x, OUT double &y, OUT double &z) {
	if(xi < 0 || yi < 0 || zi < 0)
		return false;

	if(xi >= GetDimX() || yi >= GetDimY(xi) || zi >= GetDimZ(xi, yi))
		return false;

	x = double(GetDimX(      )-1) / 2.0 + double(xi) / stepSize;
	y = double(GetDimY(xi    )-1) / 2.0 + double(yi) / stepSize;
	z = double(GetDimZ(xi, yi)-1) / 2.0 + double(zi) / stepSize;

	return true;
}

std::complex<double> GridOld::GetSphr( double r, double th, double ph ) const {
	double st = sin(th);
	return GetCart(r * st * cos(ph), r * st * sin(ph), r * cos(th));
}

#pragma endregion

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////// 
#pragma region Grid for v10 amps

GridV10Old::GridV10Old(unsigned short gridSize, double qMax) : GridOld (gridSize, qMax) {
	Extras = 7;	// Up until file version 10 (incl) this was 7
	actualGridSize = gridSize + Extras;

	// Since grid is in the range [-qmax, qmax], stepsize = 2qmax / gridsize
	stepSize = 2.0 * qMax / double(gridSize); 

	InitializeGrid();

}

GridV10Old::GridV10Old(std::istream& stream, std::string &header) {
	unsigned int versionNum = 0;
	unsigned int numBytes = 0;

	// Disabled, unnecessary
	//stream.seekg(0, ios::beg);

	// Skip the header
	std::string head;
	while(stream.peek() == '#') {
		getline(stream, head);
		header.append(head);
	}

	// Version
	stream >> versionNum;

	if(versionNum < 11) {
		Extras = 7;

	}

	// Check if a binary file
	if(versionNum != 4 && versionNum < 10)
		return;

	stream >> numBytes;
	if(numBytes != sizeof(std::complex<double>))
		return;

	// Fill grid parameters
	stream >> stepSize;
	stream >> actualGridSize;

	gridSize = actualGridSize - Extras;

	qmax = stepSize * (gridSize) / 2.0;

	InitializeGrid();

	if(!InnerImportBinaryData(stream)) {
		gridSize = 0;
		stepSize = 0.0;
		qmax = 0.0;
		return;
	}	

}

void GridV10Old::InitializeGrid() {
	trnsfm.resize(actualGridSize);

	//////////////////////////////////////////////////////////////////////////////
	double qRange = qmax + double(Extras / 2) * stepSize;	
#pragma omp parallel for schedule(dynamic, trnsfm.size() / (omp_get_num_procs() * 4))
	for(int xi = 0; xi < (int)actualGridSize; xi++) {

		double qx = (double(xi) - (double(actualGridSize-1) / 2.0)) * stepSize;
		unsigned short yS = 2 * (unsigned short)(sqrt(qRange * qRange - qx * qx) / stepSize) + Extras;
		(trnsfm[xi]).resize(yS);
		double yRange = double((yS-1) / 2) * stepSize;
#pragma omp parallel for schedule(dynamic)
		for(int yi = 0; yi < (int)yS; yi++) {
			double qy = (double(yi) - (double(yS-1) / 2.0)) * stepSize;
			unsigned short zS = 2 * (unsigned short)(sqrt(qRange * qRange - qx * qx - qy * qy) / stepSize) + Extras;
			(trnsfm[xi][yi]).resize(zS);
		}
	}
	///////////////////////////////////////////////////////////////////////

}

bool GridV10Old::ExportBinaryData(std::ostream& stream, const std::string& header) const {
	if(!stream)
		return false;

	// Versions 0 and 1 are text
	// Version 1 has "\n" in the data region
	// Versions >= 10 contain the data as binary
	// Version 4 is reserved for files converted from formated to binary
	// Version 2 is reserved for files converted from binary to formatted
	unsigned int versionNum = 10;

	stream << header << "\n";
	// File version number
	stream << versionNum << "\n";
	// Bytes per element
	stream << sizeof(std::complex<double>) << "\n";
	// Delta Q
	stream << stepSize << "\n";
	// Size of trnsfm --> txSize
	unsigned short dimx = GetDimX(), dimy;
	stream << dimx << "\n";		
	// txSize times size of trnsfm[i] --> tySize[txSize]
	for(unsigned short i = 0; i < dimx; i++)
		stream << GetDimY(i) << " ";
	stream << "\n";
	// txSize * tySize.size() sizes of tz trnsfrm[i][j].size()
	for(unsigned short i = 0; i < dimx; i++) {
		dimy = GetDimY(i);
		for(unsigned short j = 0; j < dimy; j++)
			stream << GetDimZ(i, j) << " ";
	}
	stream << "\n";

	unsigned short xS, yS;
	xS = GetDimX();
	for(unsigned short i = 0; i < xS; i++) {
		yS = GetDimY(i);
		for(unsigned short j = 0; j < yS; j++) {				
			stream.write((const char *)&(trnsfm[i][j][0]), sizeof(std::complex<double>) * GetDimZ(i, j));
		}
	}

	return true;
}

bool GridV10Old::InnerImportBinaryData(std::istream& file) {
	const unsigned short dimx = GetDimX();

	// Internal structure is built from a dimX array of dimY sizes
	// and then dimX*dimY[i] arrays of dimZ sizes
	u64 elementsToSkip = 0;
	for(unsigned short i = 0; i < dimx; i++)
		elementsToSkip += GetDimY(i);

	// THESE LINES SPED UP READING FROM 0.63 to 0.5
	// Skip the grid dimensions, and ignore whitespace
	file.ignore(10 * dimx, '\n');
	file.ignore(10 * elementsToSkip, '\n');
	file.ignore(10 * elementsToSkip, '\n');
	// Move back so that the ignore in GridOld doesn't move into the data
	file.seekg(-2, ios::cur);

	return GridOld::InnerImportBinaryData(file);
}

std::complex<double> GridV10Old::GetCart(double x, double y, double z) const {
	double xd = double(trnsfm.size()-1) / 2.0 + x / stepSize;
	int xx = int(floor(xd));
	unsigned short xxmax = (unsigned short)(trnsfm.size()-1);
	int xc = xx;
	if ( xx < 0 ) {
		xc = 0;
	}
	if ( xx > xxmax ){
		xc = xxmax;
	} // Base for x

	double yd = double(trnsfm[xc].size()-1) / 2.0 + y / stepSize;
	int yy = (unsigned short)(floor(yd));
	unsigned short yymax = (unsigned short)(trnsfm[xc].size()-1);
	unsigned short yc = yy;
	if ( yy < 0 ) {
		yc = 0;
	}
	if ( yy > yymax ){
		yc = yymax;
	} // Base for y


	double zd = double(trnsfm[xc][yc].size()-1) / 2.0 + z / stepSize;
	int zz = int(floor(zd));
	int zc = zz;
	unsigned short zzmax = (unsigned short)(trnsfm[xc][yc].size()-1);
	if ( zz < 0 ) {
		zc = 0;
	}
	if ( zz > zzmax ){
		zc = zzmax;
	} // Base for z

	if(trnsfm.size() > xc + 1 && trnsfm[xc].size() > yc + 1 && trnsfm[xc + 1].size() > yc + 1 &&
		trnsfm[xc + 1][yc + 1].size() > zc + 1 && trnsfm[xc][yc].size() > zc + 1 &&
		trnsfm[xc][yc + 1].size() > zc + 1 && trnsfm[xc + 1][yc].size() > zc + 1 &&
		xc == xx && yc == yy && zc == zz
		) { // has full cube (not quite, see second if)

			unsigned short yx1 = (unsigned short)trnsfm[xc+1].size()/2,		// The y=0 index for x+1
				zx1 = (unsigned short)trnsfm[xc+1][yc].size()/2,	// The z=0 index for (x+1, y)
				zy1 = (unsigned short)trnsfm[xc][yc+1].size()/2,	// The z=0 index for (x, y+1)
				zyx1= (unsigned short)trnsfm[xc+1][yc+1].size()/2,	// The z=0 index for (x+1, y+1)
				ySteps = yc - (unsigned short)trnsfm[xc].size()/2,
				zSteps = zc - (unsigned short)trnsfm[xc][yc].size()/2;
			if(trnsfm[xc+1].size() > yx1 + ySteps + 1 && yx1 + ySteps >=0 && 
				trnsfm[xc+1][yx1 + ySteps].size() > zx1 + zSteps + 1 && zx1 + zSteps >= 0 &&
				trnsfm[xc+1][yx1 + ySteps + 1].size() > zyx1 + zSteps + 1 && trnsfm[xc][yc + 1].size() > zy1 + zSteps + 1 && 
				zy1 + zSteps >= 0 && zyx1 + zSteps >= 0) {

					xd -= double(xx);
					yd -= double(yy);
					zd -= double(zz);

					std::complex<double> c00, c10, c01, c11, c1, c0, v000, v001, v010, v011, v100, v101, v110, v111;
					v000 = trnsfm[xc][yc][zc];
					v001 = trnsfm[xc][yc][zc+1];
					v010 = trnsfm[xc][yc + 1][zy1 + zSteps];
					v011 = trnsfm[xc][yc + 1][zy1 + zSteps + 1];
					v100 = trnsfm[xc + 1][yx1 + ySteps][zx1 + zSteps];
					v101 = trnsfm[xc + 1][yx1 + ySteps][zx1 + zSteps + 1];
					v110 = trnsfm[xc + 1][yx1 + ySteps + 1][zyx1 + zSteps];
					v111 = trnsfm[xc + 1][yx1 + ySteps + 1][zyx1 + zSteps + 1];

					c00 = v000 * (1.0 - xd) + v100 * xd;
					c10 = v010 * (1.0 - xd) + v110 * xd;
					c01 = v001 * (1.0 - xd) + v101 * xd;
					c11 = v011 * (1.0 - xd) + v111 * xd;

					c0 = c00 * (1.0 - yd) + c10 * yd;
					c1 = c01 * (1.0 - yd) + c11 * yd;
					// To be done by enclosing class
					return /*exp(Im * (Q.dot(1.0 * R))) * */(c0 * (1.0 - zd) + c1 * zd);
			}
	}

	unsigned short xc2 = xc + 1;
	if ( xc2 > xxmax)
		xc2 = xc;
	if ( trnsfm[xc2].size()-1 < yc )
		xc2 = xc;
	if ( trnsfm[xc2][yc].size()-1 < zc )
		xc2 = xc;
	if (xc2 == xc ) {
		xc2 = xc - 1;
		if ( xc2 < 0 )
			xc2 = xc;
		if ( trnsfm[xc2].size()-1 < yc )
			xc2 = xc;
		if ( trnsfm[xc2][yc].size()-1 < zc )
			xc2 = xc;
	} //Second point of interpolation for x

	unsigned short yc2 = yc + 1;
	if (yc2 > yymax)
		yc2 = yc;
	if ( trnsfm[xc][yc2].size()-1 < zc )
		yc2 = yc;
	if (yc2 == yc ){
		yc2 = yc - 1;
		if (yc2 < 0 )
			yc2 = yc;
		if ( trnsfm[xc][yc2].size()-1 < zc )
			yc2 = yc;
	} //Second point of interpolation for y

	unsigned short zc2 = zc + 1;
	if (zc2 > zzmax){
		zc2 = zc - 1;
		if (zc2 < 0 )
			zc2 = zc;
	} //Second point of interpolation for z

	std::complex<double> c00 = trnsfm[xc][yc][zc];
	if (xc != xc2 )
		c00 += (double( xx - xc ) / double( xc2 - xc ) ) * (trnsfm[xc2][yc][zc] - trnsfm[xc][yc][zc]);
	if (yc != yc2 )
		c00 += (double( yy - yc ) / double( yc2 - yc ) ) * (trnsfm[xc][yc2][zc] - trnsfm[xc][yc][zc]);
	if (zc != zc2 )
		c00 += (double( zz - zc ) / double( zc2 - zc ) ) * (trnsfm[xc][yc][zc2] - trnsfm[xc][yc][zc]);

	return c00;
}

GridV10Old::~GridV10Old() {
	trnsfm.resize(0);
}

std::complex<double> GridV10Old::GetSphr( double r, double th, double ph ) const {
	double st = sin(th);
	return GetCart(r * st * cos(ph), r * st * sin(ph), r * cos(th));
}

#pragma endregion

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////// 

#pragma region GridB: Contiguous memory allocation, GridOld structure

GridB::GridB(unsigned short gridSize, double qMax) : Grid(gridSize, qMax) {
	InitializeGrid();
}

GridB::GridB(std::istream& stream, std::string &header) {
	versionNum = 0;
	unsigned int numBytes = 0;

	// Disabled, unnecessary
	//stream.seekg(0, ios::beg);

	char desc[2] = {0};
	unsigned int offset = 0;

	if(stream.peek() == '#') {
		stream.read(desc, 2);
		if(desc[1] == '@') { // We have an offset
			stream.read((char *)&offset, sizeof(unsigned int));
			std::string del;
			getline(stream, del);
		} else if(desc[1] != '\n') {
			std::string tmphead;
			getline(stream, tmphead);
			header.append(tmphead + "\n");
		}
	}

	// Read the header
	std::string head;
	while(stream.peek() == '#') {
		getline(stream, head);
		header.append(head + "\n");
	}

	if(offset > 0) {
		// Skip the header
		stream.seekg(offset, ios::beg);
	} else {
	}

	// Version
	stream >> versionNum;

	// Check if current binary file - no backward compatibility
	if(versionNum < 11)
		return;	// We should change it to use GridV10Old

	stream >> numBytes;
	if(numBytes != sizeof(std::complex<double>))
		return;

	// Fill grid parameters
	//stream >> stepSize;
	stream >> actualGridSize;
	stream >> Extras;

	gridSize = actualGridSize - Extras;

	// Ignore the newline
	stream.ignore(100, '\n');
	stream.read((char *)&(stepSize), sizeof(double));
	qmax = stepSize * (gridSize) / 2.0;

	InitializeGrid();

	if(!InnerImportBinaryData(stream)) {
		gridSize = 0;
		actualGridSize = 0;
		stepSize = 0.0;
		qmax = 0.0;
		return;
	}

}

GridB::~GridB() {
	sphereEnds.resize(0);
	if(data)
		delete data;
}

void GridB::SetSphr(double r, double th, double ph, std::complex<double> val) {
	double st = sin(th);
	SetCart(r * st * cos(ph), r * st * sin(ph), r * cos(th), val);
}

void GridB::SetCart(double x, double y, double z, std::complex<double> val) {
	unsigned short xx, yy, zz;

	xx = (unsigned short)floor((GetDimX() / 2)			+ (x / stepSize));
	yy = (unsigned short)floor((GetDimY(xx) / 2)		+ (y / stepSize));
	zz = (unsigned short)floor((GetDimZ(xx, yy) / 2)	+ (z / stepSize));

	// Correct or throw error?
	if(xx < 0)
		xx = 0;
	if(xx > GetDimX() - 1)
		xx = GetDimX() - 1;
	if(yy < 0)
		yy = 0;
	if(yy > GetDimY(xx) - 1)
		yy = GetDimY(xx) - 1;
	if(zz < 0)
		zz = 0;
	if(zz > GetDimZ(xx, yy) - 1)
		zz = GetDimZ(xx, yy) - 1;
	long long pos = XYtoIndex(xx,yy) + zz;
	data[pos] = val.real();
	data[pos+1] = val.imag();
}

std::complex<double> GridB::GetCart(double x, double y, double z) const {
	bool bBoundaryChecks = (qmax - sqrt(x*x + y*y + z*z) ) < 3.0 * stepSize;

	double xd = double(actualGridSize - 1) / 2.0 + x / stepSize;
	int xx = int(floor(xd));
	int xxmax = int(actualGridSize - 1);
	int xc = xx;
	if ( xx < 1 ) {
		xc = 1;
	}
	if ( xx > xxmax ){
		xc = xxmax;
	} // Base for x

	const int ydim = GetDimY(xc);

	double yd = double(ydim-1) / 2.0 + y / stepSize;
	int yy = int(floor(yd));
	int yymax = ydim - 1;
	int yc = yy;
	if ( yy < 0 ) {
		yc = 0;
	}
	if ( yy > yymax ){
		yc = yymax;
	} // Base for y

	const int zdim = GetDimZ(xc, yc);

	double zd = double(zdim - 1) / 2.0 + z / stepSize;
	int zz = int(floor(zd));
	int zc = zz;
	int zzmax = zdim - 1;
	if ( zz < 0 ) {
		zc = 0;
	}
	if ( zz > zzmax ){
		zc = zzmax;
	} // Base for z

	// These are necessary mainly when using symmetries
	if(bBoundaryChecks) {
		if( GetDimY(xc)				> yc + 1 &&
			GetDimY(xc + 1)			> yc + 1 &&
			GetDimZ(xc + 1, yc + 1)	> zc + 1 &&
			GetDimZ(xc,     yc + 1)	> zc + 1 &&
			GetDimZ(xc + 1, yc)		> zc + 1
			) { // has full cube
				int yx1 = GetDimY(xc+1)/2,			// The y=0 index for x+1
					zx1 = GetDimZ(xc+1, yc  )/2,	// The z=0 index for (x+1, y)
					zy1 = GetDimZ(xc  , yc+1)/2,	// The z=0 index for (x, y+1)
					zyx1= GetDimZ(xc+1, yc+1)/2,	// The z=0 index for (x+1, y+1)
					ySteps = yc - GetDimY(xc)/2,
					zSteps = zc - GetDimZ(xc, yc)/2;
				if( GetDimY(xc+1) > (2 * ySteps + 2) && yx1 + ySteps >=0 && 
					GetDimZ(xc+1, yx1 + ySteps	 ) > zx1  + zSteps + 1 && zx1 + zSteps >= 0 &&
					GetDimZ(xc+1, yx1 + ySteps + 1) > zyx1 + zSteps + 1 &&
					GetDimZ(xc, yc + 1) > zy1 + zSteps + 1 &&  
					zy1 + zSteps >= 0 && zyx1 + zSteps >= 0) { // Second full-cube condition
						// Calculate normally
						;
				} else {
					// TODO::Calculate CORRECTLY!!!!
	return *(std::complex<double>*)(data + XYtoIndex(xc, yc) + 2 *zc);

					//return trnsfm[xc][yc][zc];
				}
		} else {
			// TODO::Calculate CORRECTLY!!!!
			//return trnsfm[xc][yc][zc];
			return *(std::complex<double>*)(data + XYtoIndex(xc, yc) + 2 *zc);

		}
	}

	int yx1 = GetDimY(xc+1)/2,		// The y=0 index for x+1
		zx1 = GetDimZ(xc+1, yc)/2,	// The z=0 index for (x+1, y)
		zy1 = GetDimZ(xc, yc+1)/2,	// The z=0 index for (x, y+1)
		zyx1= GetDimZ(xc+1, yc+1)/2,	// The z=0 index for (x+1, y+1)
		ySteps = yc - ydim/2,
		zSteps = zc - zdim/2;


	xd -= double(xx);
	yd -= double(yy);
	zd -= double(zz);

	// TODO: Shouldn't all these be like the following?
	// v001 = v000 + 1;
	// and not 
	// v001 = v000++;
	std::complex<double> c00, c10, c01, c11, c1, c0, *v000, *v001, *v010, *v011, *v100, *v101, *v110, *v111;
	v000 = (std::complex<double>*)(data + XYtoIndex(xc, yc) + 2 *zc);
	v001 = v000+1;//(std::complex<double>*)(data + XYtoIndex(xc, yc) + 2 *(zc+1));
	v010 = (std::complex<double>*)(data + XYtoIndex(xc, yc + 1) + 2 * (zy1 + zSteps));
	v011 = v010+1;//(std::complex<double>*)(data + XYtoIndex(xc, yc + 1) + 2 * (zy1 + zSteps + 1));
	v100 = (std::complex<double>*)(data + XYtoIndex(xc + 1, yx1 + ySteps) + 2 * (zx1 + zSteps));
	v101 = v100+1;//(std::complex<double>*)(data + XYtoIndex(xc + 1, yx1 + ySteps) + 2 * (zx1 + zSteps + 1));
	v110 = (std::complex<double>*)(data + XYtoIndex(xc + 1, yx1 + ySteps + 1) + 2 * (zyx1 + zSteps));
	v111 = v110+1;//(std::complex<double>*)(data + XYtoIndex(xc + 1, yx1 + ySteps + 1) + 2 * (zyx1 + zSteps + 1));

	c00 = *v000 * (1.0 - xd) + *v100 * xd;
	c10 = *v010 * (1.0 - xd) + *v110 * xd;
	c01 = *v001 * (1.0 - xd) + *v101 * xd;
	c11 = *v011 * (1.0 - xd) + *v111 * xd;

	c0 = c00 * (1.0 - yd) + c10 * yd;
	c1 = c01 * (1.0 - yd) + c11 * yd;

	return (c0 * (1.0 - zd) + c1 * zd);
}

void GridB::Add(Grid* other, double scale) {
	GridB *rhs = dynamic_cast<GridB*>(other);
	if(!rhs)
		return;

	if(GetSize() != rhs->GetSize())
		return;

#pragma omp parallel for
	for(long long pos = 0; pos < totalsz; pos++) {
		data[pos] += scale * rhs->data[pos];
	}

}

void GridB::Scale(double scale) {
#pragma omp parallel for schedule(dynamic, totalsz / (omp_get_num_procs() * 8))
	for(long long pos = 0; pos < totalsz; pos++)
		data[pos] *= scale;
}

void GridB::Fill(Amplitude *amp, void *prog, void *progArgs, double progMin, double progMax, int *pStop) {
	progressFunc progFunc = (progressFunc)prog;

	const unsigned short dimx = GetDimX();
	const double xFac = (double(dimx - 1) / 2.0) * stepSize;

	int iprog = 0;

#pragma omp parallel for schedule(dynamic, dimx / (omp_get_num_procs() * 4))
	for(int xi = 0; xi < (int)dimx; xi++) {
		
		std::complex<double> rs(0.0,0.0);
		double qx, qy, qz;
		double zFac, yFac;
		const unsigned short dimy = GetDimY(xi);
		qx = (double(xi) * stepSize- xFac);

		// Stop check		
		if(pStop && *pStop)
			continue;

		// Progress report
		if(progFunc) {
#pragma omp critical
			{			
				progFunc(progArgs, (progMax - progMin) * (double(++iprog) / double(dimx)) + progMin);
			}
		}

		yFac = (double(dimy - 1) / 2.0) * stepSize;
		for(unsigned short yi = 0; yi < dimy; yi++) {
			long long pos = XYtoIndex(xi,yi);
			const unsigned short dimz = GetDimZ(xi, yi);
			qy = (double(yi) * stepSize - yFac);

			zFac = (double(dimz - 1) / 2.0) * stepSize;
			for(unsigned short zi = 0; zi < dimz; zi++) {
				qz = (double(zi) * stepSize - zFac);
				rs = amp->calcAmplitude(qx, qy, qz);
				data[pos++] = rs.real();
				data[pos++] = rs.imag();
			}
		}
	}
}

bool GridB::Validate() const {
	bool res = true;
#pragma omp parallel for schedule(dynamic, totalsz / (omp_get_num_procs() * 8))
	for(long long pos = 0; pos < totalsz; pos++) {
		if(!res)
			continue;
		if(data[pos] != data[pos] || 
		data[pos] == std::numeric_limits<double>::infinity() ||
		data[pos] == -std::numeric_limits<double>::infinity() )
		res = false;
	}
	if(!res) {
		std::cout << "Grid contains invalid numbers!" << std::endl;
	}

	return res;
}

bool GridB::ExportBinaryData(std::ostream& stream, const std::string& header) const {
	if(!stream)
		return false;

	// Versions 0 and 1 are text
	// Version 1 has "\n" in the data region
	// Versions >= 10 contain the data as binary
	// Version 4 is reserved for files converted from formated to binary
	// Version 2 is reserved for files converted from binary to formatted
	// Version 11 uses the new form of grid and removed other crap

	unsigned int versionNum = 11;

	char DESCRIPTOR[3] = "#@";
	unsigned int headlen = (unsigned int)(2 * sizeof(char) + sizeof(unsigned int) + sizeof(char) + 
		                   header.length() * sizeof(char) + sizeof(char)); // descriptor + \n + header length + \n

	stream.write(DESCRIPTOR, 2 * sizeof(char));
	stream.write((const char *)&headlen, sizeof(unsigned int)); // 32-bit, always
	
	stream << "\n" << header << "\n";

	// File version number
	stream << versionNum << "\n";
	// Bytes per element
	stream << sizeof(std::complex<double>) << "\n";
	// Size of trnsfm --> txSize
	unsigned short dimx = GetDimX();
	stream << dimx << "\n";	
	stream << Extras << "\n";
	stream.write((const char *)&(stepSize), sizeof(double));

	// Later, for file-based grids
	/*
	headlen = (unsigned int)stream.tellp(); // Say we're 32-bit
	stream.seekp(2, ios::beg);
	stream.write((const char *)&headlen, sizeof(unsigned int)); // 32-bit, always
	stream.seekp(headlen, ios::beg);
	*/

	stream.write((const char *)(data), sizeof(double) * totalsz);

	return stream.good();
}

bool GridB::ImportBinaryData(std::istream& stream, std::string &header) {
	unsigned int versionNum = 0;
	unsigned int numBytes = 0;

	// Disabled, unnecessary
	//stream.seekg(0, ios::beg);

	char desc[2] = {0};
	unsigned int offset = 0;

	if(stream.peek() == '#') {
		stream.read(desc, 2);
		if(desc[1] == '@') { // We have an offset
			stream.read((char *)&offset, sizeof(unsigned int));
			std::string del;
			getline(stream, del);
		} else if(desc[1] != '\n') {
			std::string tmphead;
			getline(stream, tmphead);
			header.append(tmphead + "\n");
		}
	}

	// Read the header
	std::string head;
	while(stream.peek() == '#') {
		getline(stream, head);
		header.append(head + "\n");
	}

	if(offset > 0) {
		// Skip the header
		stream.seekg(offset, ios::beg);
	} else {
	}

	// Version
	stream >> versionNum;

	// Check if a binary file
	if(/*versionNum != 4 || */versionNum < 10)
		return false;

	stream >> numBytes;
	if(numBytes != sizeof(std::complex<double>))
		return false;

	// Delta Q
	unsigned int tmpGridSize, tmpExtras;
	stream >> tmpGridSize;
	if(versionNum > 10)
		stream >> tmpExtras;

	if(actualGridSize != tmpGridSize)
		return false;
	if(versionNum > 10 && Extras != tmpExtras)
		return false;

	// Ignore the newline
	stream.ignore(100, '\n');
	stream.read((char *)&(stepSize), sizeof(double));
	return InnerImportBinaryData(stream);
}

double GridB::Evaluate(unsigned short x, unsigned short y, unsigned short z, int im) const {
	return (data[XYtoIndex(x,y) + z + im]);
}

bool GridB::InnerImportBinaryData(std::istream& file) {
// 	int curpos = file.tellg();
// 	file.seekg(0, ios::end);
// 	int endpos = file.tellg();
// 	int deltaPos = 0;
// 	int leftover = endpos - curpos;
// 	file.seekg(curpos, ios::beg);


	// Amplitude values
	file.read((char *)(data), sizeof(double) * totalsz);
 	/*const unsigned short sX = GetDimX();
 	for(unsigned short i = 0; i < sX; i++) {
 		const unsigned short sY = GetDimY(i);
 		for(unsigned short j = 0; j < sY; j++) {
 			deltaPos = GetDimZ(i, j);
 			file.read((char *)&(data[curpos]), sizeof(std::complex<double>) * deltaPos);
 			curpos += deltaPos;
 		}			
 	}*/

	return !file.eof();
}

unsigned short GridB::GetDimX() const {
	return actualGridSize;
}

unsigned short GridB::GetDimY(unsigned short x) const {
	return (unsigned short)sphereEnds[x].size();
}

unsigned short GridB::GetDimZ(unsigned short x, unsigned short y) const {
	return (unsigned short)sphereEnds[x][y];

}

void GridB::InitializeGrid() {
	sphereEnds.resize(actualGridSize);
	coordMap.resize(actualGridSize);
	totalsz = 0;

	unsigned long long tmpTotalsz = 0;
	double qRange = qmax + double(Extras / 2) * stepSize;	

	// This screws up coordmap, which must be set up once
//#pragma omp parallel for schedule(dynamic, actualGridSize / (omp_get_num_procs() * 4)) reduction(+:tmpTotalsz)
	for(unsigned short xi = 0; xi < actualGridSize; xi++) {

		double qx = (double(xi) - (double(actualGridSize-1) / 2.0)) * stepSize;
		unsigned short yS = 2 * (unsigned short)(sqrt(qRange * qRange - qx * qx) / stepSize) + 1;
		sphereEnds[xi].resize(yS);
		coordMap[xi].resize(yS);
		double yRange = double((yS-1) / 2) * stepSize;

		for(unsigned short yi = 0; yi < yS; yi++) {
			double qy = (double(yi) - (double(yS-1) / 2.0)) * stepSize;
			unsigned short zS = 2 * (unsigned short)(sqrt(qRange * qRange - qx * qx - qy * qy) / stepSize) + 1;
			sphereEnds[xi][yi] = zS;
			coordMap[xi][yi] = 2 * tmpTotalsz;
			tmpTotalsz += zS;
		}
	}

	totalsz = 2 * tmpTotalsz; // To account for both the real and imaginary values
	try {
		data = new double[totalsz];		
	} catch(...) {
		data = NULL;
	}

	if(!data) { // TODO: Report to user
		printf("ERROR: Unable to allocate enough space for data\n");
		gridSize = 0;
		actualGridSize = 0;
		return;
	}

	memset(data, 0, totalsz * sizeof(double));

	center = (actualGridSize - 1) / 2;
	xxmax = actualGridSize - 1;
}

inline long long GridB::XYtoIndex(unsigned short x, unsigned short y) const {
	//printf("Requesting %d, %d --> %d\n", x, y, coordMap[x][y]);

	return coordMap[x][y];
}

bool GridB::IndicesToVectors(unsigned short xi, unsigned short yi, unsigned short zi, OUT double &x, OUT double &y, OUT double &z) {
	if(xi < 0 || yi < 0 || zi < 0)
		return false;

	if(xi >= GetDimX() || yi >= GetDimY(xi) || zi >= GetDimZ(xi, yi))
		return false;

	x = double(GetDimX(      )-1) / 2.0 + double(xi) / stepSize;
	y = double(GetDimY(xi    )-1) / 2.0 + double(yi) / stepSize;
	z = double(GetDimZ(xi, yi)-1) / 2.0 + double(zi) / stepSize;

	return true;

}

double *GridB::GetPointerWithIndices() { 
	// Input (in hex form)
	// real part = [ XXXX YYYY ZZZZ DYDY ]
	// imag part = [ 0000 0000 0000 DZDZ ]

	u64 *dataAsULL = (u64 *)data;

	size_t indexCtr = 0;

	const unsigned short dimx = GetDimX();
	for(unsigned short xi = 0; xi < dimx; xi++) {
		const unsigned short dimy = GetDimY(xi);

		for(unsigned short yi = 0; yi < dimy; yi++) {
			const unsigned short dimz = GetDimZ(xi, yi);

			for(unsigned short zi = 0; zi < dimz; zi++) {
				dataAsULL[indexCtr++] = (((u64)xi << 48) | ((u64)yi << 32) | ((u64)zi << 16) | (u64)dimy);
				dataAsULL[indexCtr++] = (u64)dimz;				
			}
		}
	}

	return data;
}

std::complex<double> GridB::GetSphr( double r, double th, double ph ) const {
	double st = sin(th);
	return GetCart(r * st * cos(ph), r * st * sin(ph), r * cos(th));
}

bool GridB::RunAfterCache() {
	if(data) {
		delete data;
		data = NULL;
	}
	return true;
}

bool GridB::RunBeforeReadingCache() {
	try {
		data = new double[totalsz];		
	} catch(...) {
		data = NULL;
	}

	if(!data) { // TODO: Report to user
		printf("ERROR: Unable to allocate enough space for data\n");
		gridSize = 0;
		actualGridSize = 0;
		return false;
	}
	return true;
}

bool GridB::RotateGrid( double thet, double ph, double psi ) {
  return false;
  //throw std::exception("The method or operation is not implemented. - RotateGrid - GridB");
}

bool GridB::RunAfterReadingCache() {
	return true;
}

#pragma endregion

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////// 

#pragma region "GridSuperblock: A grid structure of 2x2x2 superblocks"

GridSuperblock::GridSuperblock(unsigned short gridSize, double qMax) : Grid(gridSize, qMax) {
	InitializeGrid();
}

GridSuperblock::GridSuperblock(std::istream& stream, std::string &header) {
	versionNum = 0;
	unsigned int bytesPerElement = 0;

	char desc[2] = {0};
	unsigned int offset = 0;

	if(stream.peek() == '#') {
		stream.read(desc, 2);
		if(desc[1] == '@') { // We have an offset
			stream.read((char *)&offset, sizeof(unsigned int));
			std::string del;
			getline(stream, del);
		} else if(desc[1] != '\n') {
			std::string tmphead;
			getline(stream, tmphead);
			header.append(tmphead + "\n");
		}
	}

	// Read the header
	std::string head;
	while(stream.peek() == '#') {
		getline(stream, head);
		header.append(head + "\n");
	}

	if(offset > 0) {
		// Skip the header
		stream.seekg(offset, ios::beg);
	} else {
	}
	// Version
	stream >> versionNum;

	// Check if current binary file - no backward compatibility
	if(versionNum != 111)
		return;

	stream >> bytesPerElement;
	if(bytesPerElement != sizeof(Superblock))
		return;

	// Fill grid parameters
	stream >> stepSize;
	stream >> actualGridSize;
	stream >> Extras;

	gridSize = actualGridSize - Extras;

	qmax = stepSize * (gridSize) / 2.0;

	InitializeGrid();

	if(!InnerImportBinaryData(stream)) {
		gridSize = 0;
		actualGridSize = 0;
		stepSize = 0.0;
		qmax = 0.0;
		return;
	}	

}

GridSuperblock::~GridSuperblock() {
	sphereEnds.resize(0);
	if(data)
		delete data;
}

void GridSuperblock::Set(double x, double y, double z, std::complex<double> val) {
	// It's a protected method, destined to future removal
	// Not implementing it.
}

std::complex<double> GridSuperblock::GetCart(double x, double y, double z) const {
	double xd = double(actualGridSize-1) / 2.0 + x / stepSize;
	int xx = int(floor(xd));
	unsigned short xxmax = (unsigned short)(actualGridSize-1);
	int xc = xx;
	if ( xx < 0 ) {
		xc = 0;
	}
	if ( xx > xxmax ){
		xc = xxmax;
	} // Base for x

	const unsigned short ydim = GetDimY(xc);

	double yd = double(ydim-1) / 2.0 + y / stepSize;
	int yy = int(floor(yd));
	unsigned short yymax = (unsigned short)(ydim-1);
	int yc = yy;
	if ( yy < 0 ) {
		yc = 0;
	}
	if ( yy > yymax ){
		yc = yymax;
	} // Base for y

	const unsigned short zdim = GetDimZ(xc, yc);

	double zd = double(zdim-1) / 2.0 + z / stepSize;
	int zz = int(floor(zd));
	int zc = zz;
	unsigned short zzmax = (unsigned short)(zdim-1);
	if ( zz < 0 ) {
		zc = 0;
	}
	if ( zz > zzmax ){
		zc = zzmax;
	} // Base for z


	unsigned short yx1 = GetDimY(xc+1)/2,		// The y=0 index for x+1
		zx1 = GetDimZ(xc+1, yc)/2,	// The z=0 index for (x+1, y)
		zy1 = GetDimZ(xc, yc+1)/2,	// The z=0 index for (x, y+1)
		zyx1= GetDimZ(xc+1, yc+1)/2,	// The z=0 index for (x+1, y+1)
		ySteps = yc - ydim/2,
		zSteps = zc - zdim/2;


	xd -= double(xx);
	yd -= double(yy);
	zd -= double(zz);

	const double omxd = (1.0 - xd), omyd = (1.0 - yd), omzd = (1.0 - zd);

	// The conditions should be something like xc % 2 == 0 (because superblocks are 2x2x2)

	if(true/* They all fall under the same superblock */) {
		// TODO: Say we got the right index (I dunno)
		long long pos = 0; //XYZtoIndex(xc, yc, zc);

		std::complex<double> c0, c1;

		std::complex<double> c00 (data[pos].interpolant.r000 * omxd + data[pos].interpolant.r100 * xd,
								  data[pos].interpolant.i000 * omxd + data[pos].interpolant.i100 * xd);
		std::complex<double> c10 (data[pos].interpolant.r010 * omxd + data[pos].interpolant.r110 * xd,
								  data[pos].interpolant.i010 * omxd + data[pos].interpolant.i110 * xd);
		std::complex<double> c01 (data[pos].interpolant.r001 * omxd + data[pos].interpolant.r101 * xd,
								  data[pos].interpolant.i001 * omxd + data[pos].interpolant.i101 * xd);	
		std::complex<double> c11 (data[pos].interpolant.r011 * omxd + data[pos].interpolant.r111 * xd,
								  data[pos].interpolant.i011 * omxd + data[pos].interpolant.i111 * xd);	

		c0 = c00 * omyd + c10 * yd;
		c1 = c01 * omyd + c11 * yd;

		return (c0 * omzd + c1 * zd);
	} else if(false /* They fall under two different superblocks */) {
		// TODO
	} else { /* They fall under four different superblocks */
		// TODO
	}
}

void GridSuperblock::Add(Grid* other, double scale) {
	GridSuperblock *rhs = dynamic_cast<GridSuperblock*>(other);
	if(!rhs)
		return;

	if(GetSize() != rhs->GetSize())
		return;

#pragma omp parallel for
	for(long long pos = 0; pos < numElems; pos++) {
		for(int i = 0; i < 16; i++)
			data[pos].vals[i] += scale * rhs->data[pos].vals[i];
	}

}

void GridSuperblock::Scale(double scale) {
#pragma omp parallel for
	for(long long pos = 0; pos < numElems; pos++)
		for(int i = 0; i < 16; i++)
			data[pos].vals[i] *= scale;
}

void GridSuperblock::Fill(Amplitude *amp, void *prog, void *progArgs, double progMin, double progMax, int *pStop) {
	progressFunc progFunc = (progressFunc)prog;

	const unsigned short dimx = GetDimX();
	const double xFac = (double(dimx - 1) / 2.0) * stepSize;

	// TODO: This for loop should be re-ordered for superblocks

#pragma omp parallel for //schedule(dynamic, numElems / (omp_get_num_procs() * 4))
	for(long long pos = 0; pos < numElems; pos++)
		;
	/*
	for(unsigned short xi = 0; xi < dimx; xi++) {
		
		std::complex<double> rs(0.0,0.0);
		double qx, qy, qz;
		double zFac, yFac;
		const unsigned short dimy = GetDimY(xi);
		qx = (double(xi) * stepSize- xFac);

		// TODO: Implement progress bar and stop

		yFac = (double(dimy - 1) / 2.0) * stepSize;
		for(unsigned short yi = 0; yi < dimy; yi++) {
			long long pos = XYtoIndex(xi,yi);
			const unsigned short dimz = GetDimZ(xi, yi);
			qy = (double(yi) * stepSize - yFac);

			zFac = (double(dimz - 1) / 2.0) * stepSize;
			for(unsigned short zi = 0; zi < dimz; zi++) {
				qz = (double(zi) * stepSize - zFac);
				rs = amp->calcAmplitude(qx, qy, qz);
				data[pos++] = rs.real();
				data[pos++] = rs.imag();
			}
		}
	}*/
}

bool GridSuperblock::Validate() const {
	bool res = true;

#pragma omp parallel for
	for(long long pos = 0; pos < numElems; pos++) {
		if(!res)
			continue;
		for(int i = 0; i < 16; i++) {
			if(	data[pos].vals[i] != data[pos].vals[i] || 
				data[pos].vals[i] == std::numeric_limits<double>::infinity() ||
				data[pos].vals[i] == -std::numeric_limits<double>::infinity() )
					res = false;
		}
	}
	if(!res) {
		std::cout << "Grid contains invalid numbers!" << std::endl;
	}

	return res;
}

bool GridSuperblock::ExportBinaryData(std::ostream& stream, const std::string& header) const {
	if(!stream)
		return false;

	// Versions 0 and 1 are text
	// Version 1 has "\n" in the data region
	// Versions >= 10 contain the data as binary
	// Version 4 is reserved for files converted from formated to binary
	// Version 2 is reserved for files converted from binary to formatted
	// Version 11 uses the new form of grid and removed other crap
	// Version 11.1 (111) is the superblock ordering

	unsigned int versionNum = 111;

	char DESCRIPTOR[3] = "#@";
	unsigned int headlen = (unsigned int)(2 * sizeof(char) + sizeof(unsigned int) + sizeof(char) + 
		                   header.length() * sizeof(char) + sizeof(char)); // descriptor + \n + header length + \n

	stream.write(DESCRIPTOR, 2 * sizeof(char));
	stream.write((const char *)&headlen, sizeof(unsigned int)); // 32-bit, always
	
	stream << "\n" << header << "\n";

	// File version number
	stream << versionNum << "\n";
	// Bytes per element
	stream << sizeof(Superblock) << "\n";
	// Delta Q
	stream << stepSize << "\n";
	stream << actualGridSize << "\n";	
	stream << Extras << "\n";

	stream.write((const char *)(data), sizeof(Superblock) * numElems);

	return true;
}

bool GridSuperblock::ImportBinaryData(std::istream& stream, std::string &header) {
	unsigned int versionNum = 0;
	unsigned int bpe = 0;

	char desc[2] = {0};
	unsigned int offset = 0;

	if(stream.peek() == '#') {
		stream.read(desc, 2);
		if(desc[1] == '@') { // We have an offset
			stream.read((char *)&offset, sizeof(unsigned int));
			std::string del;
			getline(stream, del);
		} else if(desc[1] != '\n') {
			std::string tmphead;
			getline(stream, tmphead);
			header.append(tmphead + "\n");
		}
	}

	// Read the header
	std::string head;
	while(stream.peek() == '#') {
		getline(stream, head);
		header.append(head + "\n");
	}

	if(offset > 0) {
		// Skip the header
		stream.seekg(offset, ios::beg);
	} else {
	}

	// Version
	stream >> versionNum;

	// Check if a binary file
	if(versionNum != 111)
		return false;

	stream >> bpe;
	if(bpe != sizeof(Superblock))
		return false;

	// Delta Q
	unsigned short tmpGridSize, tmpExtras;
	double tmpStepSize;
	stream >> tmpStepSize;
	stream >> tmpGridSize;
	stream >> tmpExtras;

	if(fabs(1.0 - (stepSize / tmpStepSize)) > 1.0e-6)
		return false;
	if(actualGridSize != tmpGridSize)
		return false;
	if(Extras != tmpExtras)
		return false;

	return InnerImportBinaryData(stream);
}

bool GridSuperblock::InnerImportBinaryData(std::istream& file) {
	// Ignore the newline
	//file.ignore(100, '\n');

	// Amplitude values
	file.read((char *)(data), sizeof(Superblock) * numElems);

	return !file.eof();
}

unsigned short GridSuperblock::GetDimX() const {
	return actualGridSize;
}

unsigned short GridSuperblock::GetDimY(unsigned short x) const {
	return (unsigned short)sphereEnds[x].size();
}

unsigned short GridSuperblock::GetDimZ(unsigned short x, unsigned short y) const {
	return sphereEnds[x][y];

}

void GridSuperblock::InitializeGrid() {
	sphereEnds.resize(actualGridSize);
	coordMap.resize(actualGridSize);
	numElems = 0;

	unsigned long long tmpTotalsz = 0;
	double qRange = qmax + double(Extras / 2) * stepSize;	

	// TODO: Get actual values for numElems here
	/*
	// Do not parallelize this loop
	for(unsigned short xi = 0; xi < actualGridSize; xi++) {

		double qx = (double(xi) - (double(actualGridSize-1) / 2.0)) * stepSize;
		unsigned short yS = 2 * unsigned short((sqrt(qRange * qRange - qx * qx) / stepSize) + 0.5);
		(sphereEnds[xi]).resize(yS);
		coordMap[xi].resize(yS);
		double yRange = double((yS-1) / 2) * stepSize;

		for(unsigned short yi = 0; yi < yS; yi++) {
			double qy = (double(yi) - (double(yS-1) / 2.0)) * stepSize;
			unsigned short zS = 2 * unsigned short(sqrt(qRange * qRange - qx * qx - qy * qy) / stepSize);
			sphereEnds[xi][yi] = zS;
			coordMap[xi][yi] = 2 * tmpTotalsz;
			tmpTotalsz += zS;
		}
	}*/

	numElems = tmpTotalsz; // To account for both the real and imaginary values
	try {
		data = new Superblock[numElems];		
	} catch(...) {
		data = NULL;
	}

	if(!data) { // TODO: Report to user
		printf("ERROR: Unable to allocate enough space for data\n");
		gridSize = 0;
		actualGridSize = 0;
		return;
	}

	center = (actualGridSize - 1) / 2;
	xxmax = actualGridSize - 1;
}

inline long long GridSuperblock::XYtoIndex(unsigned short x, unsigned short y) const {
	//printf("Requesting %d, %d --> %d\n", x, y, coordMap[x][y]);

	return coordMap[x][y];
}

#pragma endregion

std::complex<double> SphereGrid::GetCart( double x, double y, double z ) const {
	double r  = sqrt(x*x + y*y + z*z);
	if(fabs(z) > r) r = fabs(z);	// Correct if (z/r) is greater than 1.0 by a few bits
	double th = acos(z / r);
	double ph = atan2(y, x);
	if(ph < 0.0)
		ph += M_2PI;
	return GetSphr(r, th, ph);
}

inline std::complex<double> SphereGrid::InterpolateThetaPhiPlane(const u16 rI, const double rri, const u16 tI, const double theta, const u16 pI, const double phi) const {
	std::complex<double> *phi00, *phi01, *phi10, *phi11;
	std::complex<double> plane, theta0, theta1;
	const u16 tI1 = tI + 1;

	if(tI == 0 || tI == sizeMap[rI].size() - 1) // Theta is zero or Pi
		return std::complex<double>(data[coordMap[rI][tI]], data[coordMap[rI][tI] + 1]);

	// The index of the same phi when theta is +1
	u16 t1pI = u16(phi * rri * sin(M_PI * double(tI1) / double(sizeMap[rI].size() - 1)) / phiStepSizes[rI][tI1]);
	bool phi0Loop = (pI   < sizeMap[rI][tI]  - 1),
		 phi1Loop = (t1pI < sizeMap[rI][tI1] - 1);

	// Interpolate phi first
	phi00 = (std::complex<double> *)(data + coordMap[rI][tI] ) + pI;
	phi01 = (std::complex<double> *)(data + coordMap[rI][tI] ) + (phi0Loop ? pI + 1 : 0) ;
	phi10 = (std::complex<double> *)(data + coordMap[rI][tI1]) + pI;
	phi11 = (std::complex<double> *)(data + coordMap[rI][tI1]) + (phi1Loop ? t1pI + 1 : 0);

	double tht = M_PI * double(tI) / double(sizeMap[rI].size() - 1);

	// First phi pair
	if(phi00 == phi01 || sizeMap[rI][tI] == 1) {
		theta0 = *phi00;
	} else {
		double tRad = rri * sin(M_PI * double(tI) / double(sizeMap[rI].size() - 1));
		double phi0  = M_2PI * double(pI) / double(sizeMap[rI][tI]);
		double ratio = (phi - phi0)  * tRad / phiStepSizes[rI][tI];
		theta0 = ((1.0 - ratio) * (*phi00)) + (ratio * (*phi01));
	} // First phi pair

	// Second phi pair
	if(phi10 == phi11 || sizeMap[rI][tI1] == 1) {
		theta1 = *phi10;
	} else {
		double tRad = rri * sin(M_PI * double(tI1) / double(sizeMap[rI].size() - 1));
		double phi1 = M_2PI * double(t1pI) / double(sizeMap[rI][tI1]);
		double ratio = (phi1 - phi) * tRad / phiStepSizes[rI][tI1];
		theta1 = ((1.0 - ratio) * (*phi10)) + (ratio * (*phi11));
	} // Second phi pair

	// Interpolate the two phi pairs (theta)
	{
		double ratio = (theta - tht) * rri / thetaStepSizes[rI];
		plane = ((1.0 - ratio) * theta0) + (ratio * theta1);
	} // Interpolate the two phi pairs (theta)

	return plane;
}

std::complex<double> SphereGrid::GetSphr( double rr, double theta, double phi ) const {
	// Note: theta and phi are in radians, while all the calculations are in nm^{-1}
	std::complex<double> res, firstPlane, secondPlane;

	// Origin
	if(rr == 0.0) {
		return std::complex<double>(data[0], data[1]);
	}

	u16 rI = u16(rr / stepSize);
	double rri = double(rI) * stepSize;

	const u16 tI = u16(theta * rri / thetaStepSizes[rI]); // = u16(theta * hCircR / (M_PI * thetaStepSizes[rI]));
	const u16 pI = u16(phi * rri * sin(M_PI * double(tI) / double(sizeMap[rI].size() - 1)) / phiStepSizes[rI][tI]);

	// First interpolation on the inner theta-phi plane
	firstPlane = InterpolateThetaPhiPlane(rI, rri, tI, theta, pI, phi);
	
	if(rI == GetDimX() - 1)
		return firstPlane;

	// Second interpolation on the outer theta-phi plane
	double rri1 = rri + stepSize;
	u16 rtI = u16(theta * rri1 / thetaStepSizes[rI+1]);
	u16 rpI = u16(phi * rri1 * sin(M_PI * double(rtI) / double(phiStepSizes[rI+1].size() - 1)) / phiStepSizes[rI+1][rtI]);
	secondPlane = InterpolateThetaPhiPlane(rI + 1, rri1, rtI, theta, rpI, phi);

	// Interpolate between the two planes
	double ratio = (rr - rri) / stepSize;
	return ((1.0 - ratio) * firstPlane) + (ratio * secondPlane);
	
}

void SphereGrid::Fill( Amplitude *amp, void *prog, void *progArgs, double progMin, double progMax, int *pStop ) {

	progressFunc progFunc = (progressFunc)prog;

	const unsigned short dimx = GetDimX();

	int iprog = 0;


#pragma omp parallel for schedule(guided, dimx / (omp_get_num_procs() * 16))
	for(int xi = 0; xi < (int)dimx; xi++) {

		std::complex<double> rs(0.0,0.0);
		double rI;
		const unsigned short dimy = GetDimY(xi);

		// Stop check		
		if(pStop && *pStop)
			continue;

		// Progress report
		if(progFunc) {
#pragma omp critical
			{			
				progFunc(progArgs, (progMax - progMin) * (double(++iprog) / double(dimx)) + progMin);
			}
		}

		rI = double(xi) * stepSize;

		for(unsigned short yi = 0; yi < dimy; yi++) {
			long long pos = coordMap[xi][yi];
			const unsigned short dimz = GetDimZ(xi, yi);
			double cst = ((dimy == 1) ? 1.0 : cos(M_PI * double(yi) / double(dimy - 1))),
				   snt = ((dimy == 1) ? 0.0 : sin(M_PI * double(yi) / double(dimy - 1)));
			
			for(unsigned short zi = 0; zi < dimz; zi++) {
				double csp = cos(M_2PI * double(zi) / double(dimz)),
					   snp = sin(M_2PI * double(zi) / double(dimz));
				rs = amp->calcAmplitude(rI * snt * csp, rI * snt * snp, rI * cst);
				data[pos++] = rs.real();
				data[pos++] = rs.imag();
			}
		}
	}

}

bool SphereGrid::ExportBinaryData( std::ostream& stream, const std::string& header ) const {
	if(!stream)
		return false;

	// Versions 0 and 1 are text
	// Version 1 has "\n" in the data region
	// Versions >= 10 contain the data as binary
	// Version 4 is reserved for files converted from formated to binary
	// Version 2 is reserved for files converted from binary to formatted
	// Version 11 uses the new form of grid and removed other crap
	// Version 12 uses the new form of spherical grid and removed other crap

	unsigned int versionNum = 12;

	char DESCRIPTOR[3] = "#@";
	unsigned int headlen = (unsigned int)(2 * sizeof(char) + sizeof(unsigned int) + sizeof(char) + 
		                   header.length() * sizeof(char) + sizeof(char)); // descriptor + \n + header length + \n

	stream.write(DESCRIPTOR, 2 * sizeof(char));
	stream.write((const char *)&headlen, sizeof(unsigned int)); // 32-bit, always
	
	stream << "\n" << header << "\n";

	// File version number
	stream << versionNum << "\n";
	// Bytes per element
	stream << sizeof(std::complex<double>) << "\n";
	// Size of trnsfm --> txSize
	unsigned short dimx = GetDimX();
	stream << dimx << "\n";	
	stream << Extras << "\n";
	stream.write((const char *)&(stepSize), sizeof(double));

	// Later, for file-based grids
	/*
	headlen = (unsigned int)stream.tellp(); // Say we're 32-bit
	stream.seekp(2, ios::beg);
	stream.write((const char *)&headlen, sizeof(unsigned int)); // 32-bit, always
	stream.seekp(headlen, ios::beg);
	*/

	stream.write((const char *)(data), sizeof(double) * totalsz);

	return stream.good();

}

bool SphereGrid::ImportBinaryData( std::istream& stream, std::string &header ) {
	unsigned int versionNum = 0;
	unsigned int numBytes = 0;

	// Disabled, unnecessary
	//stream.seekg(0, ios::beg);

	char desc[2] = {0};
	unsigned int offset = 0;

	if(stream.peek() == '#') {
		stream.read(desc, 2);
		if(desc[1] == '@') { // We have an offset
			stream.read((char *)&offset, sizeof(unsigned int));
			std::string del;
			getline(stream, del);
		} else if(desc[1] != '\n') {
			std::string tmphead;
			getline(stream, tmphead);
			header.append(tmphead + "\n");
		}
	}

	// Read the header
	std::string head;
	while(stream.peek() == '#') {
		getline(stream, head);
		header.append(head + "\n");
	}

	if(offset > 0) {
		// Skip the header
		stream.seekg(offset, ios::beg);
	} else {
	}

	// Version
	stream >> versionNum;

	// Check if a binary file
	if(/*versionNum != 4 ||*/ versionNum < 10)
		return false;

	if(versionNum == 11)	// Decide what to do later (orthogonal grid)
		return false;

	stream >> numBytes;
	if(numBytes != sizeof(std::complex<double>))
		return false;

	// Delta Q
	unsigned int tmpGridSize, tmpExtras;
	stream >> tmpGridSize;
	if(versionNum > 10)
		stream >> tmpExtras;

	if(actualGridSize != tmpGridSize)
		return false;
	if(versionNum > 10 && Extras != tmpExtras)
		return false;

	gridSize = (actualGridSize - Extras) * 2;

	// Ignore the newline
	stream.ignore(100, '\n');
	stream.read((char *)&(stepSize), sizeof(double));
	return InnerImportBinaryData(stream);
}

void SphereGrid::Add( Grid* other, double scale ) {
	SphereGrid *rhs = dynamic_cast<SphereGrid*>(other);
	if(!rhs)
		return;

	if(GetSize() != rhs->GetSize())
		return;

#pragma omp parallel for
	for(long long pos = 0; pos < totalsz; pos++) {
		data[pos] += scale * rhs->data[pos];
	}
}

void SphereGrid::Scale( double scale ) {
#pragma omp parallel for schedule(dynamic, totalsz / (omp_get_num_procs() * 8))
	for(long long pos = 0; pos < totalsz; pos++)
		data[pos] *= scale;
}

bool SphereGrid::Validate() const {
	bool res = true;

#pragma omp parallel for schedule(dynamic, totalsz / (omp_get_num_procs() * 8))
	for(long long pos = 0; pos < totalsz; pos++) {
		if(!res)
			continue;
		if(data[pos] != data[pos] || 
			data[pos] == std::numeric_limits<double>::infinity() ||
			data[pos] == -std::numeric_limits<double>::infinity() )
			res = false;
	}

	if(!res) {
		std::cout << "Grid contains invalid numbers!" << std::endl;
	}

	return res;
}

bool SphereGrid::IndicesToVectors( unsigned short xi, unsigned short yi, unsigned short zi, OUT double &r, OUT double &thet, OUT double &phi ) {
	r = double(xi) * stepSize;
	thet = M_PI  * double(yi) / double(sizeMap[xi].size() - 1);
	phi  = M_2PI * double(zi) / double(sizeMap[xi][yi]);
	return true;
}

double * SphereGrid::GetPointerWithIndices() {
	// Input (in hex form)	// Numbers are indices and max indices
	// real part = [ THTH DYDY PHPH DZDZ ]
	// imag part = [ 0000 0000 0000 RRRR ]

	u64 *dataAsULL = (u64 *)data;

	size_t indexCtr = 0;

	const unsigned short dimx = GetDimX();
	for(unsigned short ri = 0; ri < dimx; ri++) {
		const unsigned short dimy = GetDimY(ri);

		for(unsigned short ti = 0; ti < dimy; ti++) {
			const unsigned short dimz = GetDimZ(ri, ti);

			for(unsigned short pi = 0; pi < dimz; pi++) {
				dataAsULL[indexCtr++] = (((u64)ti << 48) | ((u64)dimy << 32) | ((u64)pi << 16) | (u64)dimz);
				dataAsULL[indexCtr++] = (u64)ri;
			}
		}
	}

	return data;
}

void SphereGrid::SetSphr( double x, double y, double z, std::complex<double> val ) {
	u16 ri, ti, pi;
	ri = (u16)floor(x / stepSize);
	ti = (u16)floor(y / thetaStepSizes[ri]);
	pi = (u16)floor(z / phiStepSizes[ri][ti]);

	if(ri > GetDimX() - 1) {
		ri = GetDimX() - 1;
	}
	if(ti > GetDimY(ri) - 1) {
		ti = GetDimY(ri) - 1;
	}
	if(pi > GetDimZ(ri, ti) - 1) {
		pi = GetDimZ(ri, ti) - 1;
	}

	long long pos = coordMap[ri][ti] + pi;
	data[pos] = val.real();
	data[pos+1] = val.imag();
}

void SphereGrid::SetCart( double x, double y, double z, std::complex<double> val ) {
	double r  = sqrt(x*x + y*y + z*z);
	if(fabs(z) > r) r = fabs(z);	// Correct if (z/r) is greater than 1.0 by a few bits
	double th = acos(z / r);
	double ph = atan2(y, x);
	if(ph < 0.0)
		ph += M_2PI;

	return SetSphr(r, th, ph, val);
}

SphereGrid::SphereGrid(unsigned short gridsize, double qMax) : Grid(gridsize, qMax)  {
	// Override the some of Grid constructor
	// Up until file version 10 (incl) this was 7; version 11 it was 11
	Extras = 3;
	gridSize = gridsize;

	InitializeGrid();
}

SphereGrid::SphereGrid( std::istream& stream, std::string &header ) {
	versionNum = 0;
	unsigned int numBytes = 0;

		char desc[2] = {0};
	unsigned int offset = 0;

	if(stream.peek() == '#') {
		stream.read(desc, 2);
		if(desc[1] == '@') { // We have an offset
			stream.read((char *)&offset, sizeof(unsigned int));
			std::string del;
			getline(stream, del);
		} else if(desc[1] != '\n') {
			std::string tmphead;
			getline(stream, tmphead);
			header.append(tmphead + "\n");
		}
	}

	// Read the header
	std::string head;
	while(stream.peek() == '#') {
		getline(stream, head);
		header.append(head + "\n");
	}

	if(offset > 0) {
		// Skip the header
		stream.seekg(offset, ios::beg);
	} else {
	}

	// Version
	stream >> versionNum;

	// Check if current binary file - no backward compatibility
	if(versionNum < 12)
		return;	// We should change it to use GridB

	stream >> numBytes;
	if(numBytes != sizeof(std::complex<double>))
		return;

	// Fill grid parameters
	//stream >> stepSize;
	stream >> actualGridSize;
	stream >> Extras;

	gridSize = (actualGridSize - Extras) * 2;

	// Ignore the newline
	stream.ignore(100, '\n');
	stream.read((char *)&(stepSize), sizeof(double));
	qmax = stepSize * (gridSize) / 2.0;

	InitializeGrid();

	if(!InnerImportBinaryData(stream)) {
		gridSize = 0;
		actualGridSize = 0;
		stepSize = 0.0;
		qmax = 0.0;
		return;
	}

}

SphereGrid::~SphereGrid() {
	if(data)
		delete data;
	data = NULL;
	radii.resize(0);
	coordMap.resize(0);
	thetaStepSizes.resize(0);
	phiStepSizes.resize(0);
}

void SphereGrid::InitializeGrid() {
	totalsz = 0;

	// Since grid is in the range [-qmax, qmax], stepsize = 2qmax / gridsize
	stepSize = qmax / double(gridSize / 2); 
	actualGridSize = gridSize / 2 + Extras;

	// Add Maps
	coordMap.resize(actualGridSize);
	sizeMap.resize(actualGridSize);
	phiStepSizes.resize(actualGridSize);
	thetaStepSizes.resize(actualGridSize);
	for(int i = 0; i < actualGridSize; i++) {
		double ri = double(i) * stepSize;
		int szTh = ceil(M_PI * ri / stepSize) + 1;
		if(szTh < 3)
			szTh = 3;
		thetaStepSizes[i] = (M_PI * ri / double(szTh - 1));	// |q| / thetaSize
		if(i == 0)
			szTh = 1;
		phiStepSizes[i].resize(szTh);
		coordMap[i].resize(szTh);
		sizeMap[i].resize(szTh);
		
		for(int j = 0; j < szTh; j++) {
			coordMap[i][j] = 2 * totalsz;
			//double thetaIJ = double(j) * thetaStepSizes[i];
			double circ = M_2PI * ri * sin(M_PI * double(j) / double(szTh - 1) );
			int szPhi = ceil(circ / stepSize);
			if(szPhi < 4)
				szPhi = 4;
			if(j == 0 || j == szTh - 1) {
				szPhi = 1;
				phiStepSizes[i][j] = 0.0;
			} else {
				// 2 pi r' (r' = r sin theta)
				//        / szPhi
				phiStepSizes[i][j] = circ / double(szPhi);
			}
			totalsz += szPhi;
			sizeMap[i][j] = szPhi;
		}
	}

	totalsz *= 2;	// Complex
	try {
		data = new double[totalsz];		
	} catch(...) {
		data = NULL;
	}
	if(!data) { // TODO: Report to user
		printf("ERROR: Unable to allocate enough space for data\n");
		gridSize = 0;
		actualGridSize = 0;
		return;
	}

	memset(data, 0, totalsz * sizeof(double));

// 	center = (actualGridSize - 1) / 2;
// 	xxmax = actualGridSize - 1;

	// DEBUG LINES
	{
		long long outerLayer1 = 0, outerLayer2 = 0;
		int ind = this->actualGridSize - 1;
		for(int i = 0; i < sizeMap[ind].size(); i++) {
			outerLayer1 += sizeMap[ind][i];
		}
		ind--;
		for(int i = 0; i < sizeMap[ind].size(); i++) {
			outerLayer2 += sizeMap[ind][i];
		}
		printf("\nTotal cells: %lld\nTotal cells on outer layers:\n\tOuter Layer: %lld complex numbers = %lld\n\tOneLayerIn:  %lld complex numbers = %lld\n",
			totalsz, outerLayer1, outerLayer1 * sizeof(std::complex<double>), outerLayer2, outerLayer2 * sizeof(std::complex<double>));
	}


}

bool SphereGrid::InnerImportBinaryData( std::istream& file ) {
	file.read((char *)(data), sizeof(double) * totalsz);

	return file.good();
}

unsigned short SphereGrid::GetDimX() const {
	return u16(sizeMap.size());
}

unsigned short SphereGrid::GetDimY( unsigned short x ) const {
	return u16(sizeMap[x].size());
}

unsigned short SphereGrid::GetDimZ( unsigned short x, unsigned short y ) const {
	return u16(sizeMap[x][y]);
}

double* SphereGrid::GetDataPointer() {
	return data;
}

bool SphereGrid::RunAfterCache() {
	if(data) {
		delete data;
		data = NULL;
	}
	return true;
}

bool SphereGrid::RunBeforeReadingCache() {
	try {
		data = new double[totalsz];		
	} catch(...) {
		data = NULL;
	}

	if(!data) { // TODO: Report to user
		printf("ERROR: Unable to allocate enough space for data\n");
		gridSize = 0;
		actualGridSize = 0;
		return false;
	}

	return true;
}

bool SphereGrid::RotateGrid( double thet, double ph, double psi ) {
	return false;

	//throw std::exception("The method or operation is not implemented. - RotateGrid - SphereGrid");
	clock_t starttm, endtm;
	double timeItTook;

	starttm = clock();

	double *newData;
	try {
		newData = new double[totalsz];		
	} catch(...) {
		newData = NULL;
		return false;
	}

	memset(newData, 0, totalsz * sizeof(double));

	const unsigned short dimx = GetDimX();

	int iprog = 0;

#pragma omp parallel for schedule(guided, dimx / (omp_get_num_procs() * 16))
	for(int xi = 0; xi < (int)dimx; xi++) {

		std::complex<double> rs(0.0,0.0);
		double rI;
		const unsigned short dimy = GetDimY(xi);

		rI = double(xi) * stepSize;

		for(unsigned short yi = 0; yi < dimy; yi++) {
			long long pos = coordMap[xi][yi];
			const unsigned short dimz = GetDimZ(xi, yi);
			double newTheta =((dimy == 1) ? 0.0 : (M_PI * double(yi) / double(dimy - 1)) ) - thet;
			// Make sure that the rotated angle is still [0,pi)
			if(newTheta >= M_PI) {
				newTheta = std::fmod(newTheta, M_PI);
			} else if(newTheta < 0.0) {
				newTheta = M_PI + std::fmod(newTheta, M_PI);	// TODO:: Confirm this works
			}

			for(unsigned short zi = 0; zi < dimz; zi++) {
				double newPhi = M_2PI * double(zi) / double(dimz) - ph;
				// Make sure that the rotated angle is still [0,2pi)
				if(newPhi >= M_2PI) {
					newPhi = std::fmod(newPhi, M_2PI);
				} else if(newPhi < 0.0) {
					newPhi = M_2PI + std::fmod(newPhi, M_2PI);	// TODO:: Confirm this works
				}

				rs = this->GetSphr(rI, newTheta, newPhi);
				newData[pos++] = rs.real();
				newData[pos++] = rs.imag();
			}
		}
	}

	std::swap(data, newData);

	delete newData;

	endtm = clock();
	timeItTook =  ((double)(endtm - starttm)) / CLOCKS_PER_SEC;
	std::cout << "\nTook "<< timeItTook <<" seconds to rotate grid!\n";

	return true;
}

bool SphereGrid::RunAfterReadingCache() {
	return true;
}

#ifdef URIS_GRID
typedef uint32_t U32;
typedef uint64_t U64;

U32 icbrt64(U64 x) {
	int s;
	U32 y;
	U64 b;

	y = 0;
	for (s = 63; s >= 0; s -= 3) {
		y += y;
		b = 3*y*((U64) y + 1) + 1;
		if ((x >> s) >= b) {
			x -= b << s;
			y++;
		}
	}
	return y;
}


/************************************************************************/
/* JacobianSphereGrid: Each shell is a square on the theta-phi plane    */
/************************************************************************/
std::complex<double> JacobianSphereGrid::InterpolateThetaPhiPlane( const u16 ri, const double theta, const double phi ) const {
	long long base, tI, pI;
	double tPh, tTh;

	IndicesFromRadians(ri, theta, phi, tI, pI, base, tTh, tPh);

	// Use the pre-calculated splines on phi to setup the points to interpolate on theta
	Eigen::VectorXcd nodes;
	Eigen::VectorXcd ds;
	int points = 4;
	int mInd = 1;	// The number of points below the lowest neighbor we would like to take
	nodes.resize(points);

	int phiPoints = phiDivisions * ri;
	int thePoints = thetaDivisions * ri;

	// The index of the point above the value in the phi axis
	int pI1 = (pI + 1) % phiPoints;

	std::complex<double> p1, p2;
	std::complex<float> d1, d2;

	for(int i = -mInd; i < points - mInd; i++) {
		// This is actually incorrect at the edges.
		// If we deviate from [0,pi], we have to add pi to phi
		// See addition in 6 lines that deals with this
		int theInd = (thePoints + 1 + tI + i) % (thePoints + 1);
		long long pos1, pos2;
		// Calculate using spline
		if(theInd == tI + i) {

			pos1 = 2*(base + phiPoints * theInd + pI );
			pos2 = 2*(base + phiPoints * theInd + pI1);

		} else {
			// There is a "2*" and a "/2" that have been removed (canceled out)
			// Assumes phiPoints is even
			if(tI + i > thePoints)
				theInd = (tI + i) - (thePoints+1);
			pos1 = 2*(base + phiPoints * theInd + pI );
			pos2 = 2*(base + phiPoints * theInd + pI1);

			pos1 += ((2*pI  >= phiPoints) ? -phiPoints : phiPoints);
			pos2 += ((2*pI1 >= phiPoints) ? -phiPoints : phiPoints);
		}


		// TODO: Deal with the case where the theta points loop around [0,pi)
		//			The index for phi then has to be moved by pi.

		d1 = *(std::complex<float>*)(&interpolantCoeffs[pos1]);
		d2 = *(std::complex<float>*)(&interpolantCoeffs[pos2]);

		p1 = *(std::complex<double> *)(&data[pos1]);
		p2 = *(std::complex<double> *)(&data[pos2]);

		nodes[i+mInd] = p1 + std::complex<double>(d1) * tPh + 
						(3.0 * (p2 - p1) - 2.0 * std::complex<double>(d1) - std::complex<double>(d2)) * (tPh*tPh) + 
						(2.0 * (p1 - p2) + std::complex<double>(d1 + d2)) * (tPh*tPh*tPh);
	}

	// Calculate the spline for theta using 'points' nodes
	if(points != 4) {
		ds = EvenySpacedSplineSolver(&nodes[0], nodes.size());
	} else {
		ds.resize(4);
		EvenlySpacedFourPointSpline(nodes[0], nodes[1], nodes[2], nodes[3], &ds[mInd], &ds[mInd+1]);
	}

	// Return the value at (theta,phi)
	return nodes[mInd] + ds[mInd] * tTh + 
		(3.0 * (nodes[mInd+1] - nodes[mInd]) - 2.0 * ds[mInd] - ds[mInd+1]) * (tTh*tTh) + 
		(2.0 * (nodes[mInd] - nodes[mInd+1]) + ds[mInd] + ds[mInd+1]) * (tTh*tTh*tTh);
}

void JacobianSphereGrid::SetCart( double x, double y, double z, std::complex<double> val ) {
	double qq = sqrt(x*x + y*y + z*z);
	if(fabs(z) > qq) qq = fabs(z);	// Correct if (z/r) is greater than 1.0 by a few bits
	double th = acos(z / qq);
	double ph = atan2(y, x);
	if(ph < 0.0)
		ph += M_2PI;

	SetSphr(qq, th, ph, val);
}

void JacobianSphereGrid::SetSphr( double q, double th, double ph, std::complex<double> val ) {
	// Not sure this is necessary, trying to make sure we don't miss a level
	double *fptr = &q;
	(*((long long *)fptr)) += 8;

	long long ind = (long long)(q / stepSize);
	long long thI, phI;

	if(ind == 0) {
		data[0] = val.real();
		data[1] = val.imag();
		return;
	}

	thI  = (long long)(double(thetaDivisions * ind + 1) * (th / M_PI ));
	phI  = (long long)(double(phiDivisions   * ind) * (ph / M_2PI));

	long long pos = IndexFromIndices(ind, thI, phI);
	data[pos  ] = val.real();
	data[pos+1] = val.imag();
}

void JacobianSphereGrid::InitializeGrid() {
	thetaDivisions = 3;
	phiDivisions = 6;

	interpolantCoeffs = NULL;

	// Since grid is in the range [-qmax, qmax], stepsize = 2qmax / gridsize
	stepSize = qmax / double(gridSize / 2); 
	actualGridSize = gridSize / 2 + Extras;

	long long i = actualGridSize;
	totalsz = (phiDivisions * i * (i+1) * (3 + thetaDivisions + 2 * thetaDivisions * i)) / 6;
	totalsz++;	// Add the origin
	totalsz *= 2;	// Complex

	try {
		data = new double[totalsz];		
	} catch(...) {
		data = NULL;
	}
	if(!data) { // TODO: Report to user
		printf("ERROR: Unable to allocate enough space for data\n");
		gridSize = 0;
		actualGridSize = 0;
		return;
	}

	memset(data, 0, totalsz * sizeof(double));

	printf("Total number of cells: %lld\n\tcells on outer layer: %lld\n",
		totalsz/2, (thetaDivisions*i+1)*(phiDivisions*i));
}

bool JacobianSphereGrid::InnerImportBinaryData( std::istream& file ) {
	file.read((char *)(data), sizeof(double) * totalsz);
	if(!file.good())
		data = NULL;
	return file.good();
}

JacobianSphereGrid::JacobianSphereGrid( unsigned short gridsize, double qMax ) {
	Extras = 3;
	gridSize = gridsize;
	qmax = qMax;

//	rotatedTheta = rotatedPhi = 0.0;

	InitializeGrid();
}

JacobianSphereGrid::JacobianSphereGrid( std::istream& stream, std::string &header ) {
	versionNum = 0;
	unsigned int numBytes = 0;
	data = NULL;
	interpolantCoeffs = NULL;

	char desc[2] = {0};
	unsigned int offset = 0;

	if(stream.peek() == '#') {
		stream.read(desc, 2);
		if(desc[1] == '@') { // We have an offset
			stream.read((char *)&offset, sizeof(unsigned int));
			std::string del;
			getline(stream, del);
		} else if(desc[1] != '\n') {
			std::string tmphead;
			getline(stream, tmphead);
			header.append(tmphead + "\n");
		}
	}

	// Read the header
	std::string head;
	while(stream.peek() == '#') {
		getline(stream, head);
		header.append(head + "\n");
	}

	if(offset > 0) {
		// Skip the header
		stream.seekg(offset, ios::beg);
	} else {
	}

	// Version
	stream >> versionNum;

	// Check if current binary file - no backward compatibility
	if(versionNum < 13) {
		std::cout << "The file version does not match the grid... " << versionNum << std::endl;
		return;	// We should change it to use GridB or something else
	}

	stream >> numBytes;
	if(numBytes != sizeof(std::complex<double>))
		return;

	// Fill grid parameters
	// stream >> stepSize;
	stream >> actualGridSize;
	stream >> Extras;

	gridSize = (actualGridSize - Extras) * 2;

	// Ignore the newline
	stream.ignore(100, '\n');
	stream.read((char *)&(stepSize), sizeof(double));
//	stream.read((char *)&(existingRotation), sizeof(double)*9);
//	std::cout << existingRotation << std::endl;
	qmax = stepSize * (gridSize) / 2.0;

	InitializeGrid();

	if(!InnerImportBinaryData(stream)) {
		gridSize = 0;
		actualGridSize = 0;
		stepSize = 0.0;
		qmax = 0.0;
		return;
	}
	CalculateSplines();
}

JacobianSphereGrid::~JacobianSphereGrid() {
	if(data)
		delete data;
	data = NULL;
	if(interpolantCoeffs)
		delete	interpolantCoeffs;
	interpolantCoeffs = NULL;
}

unsigned short JacobianSphereGrid::GetDimX() const {
	return u16(actualGridSize);
}

unsigned short JacobianSphereGrid::GetDimY( unsigned short x ) const {
	if(x == 0) {
		return 1;
	}
	return (thetaDivisions * x + 1);
}

unsigned short JacobianSphereGrid::GetDimZ( unsigned short x, unsigned short y ) const {
	if(x == 0) {
		return 1;
	}
	return (phiDivisions * x);
}

std::complex<double> JacobianSphereGrid::GetCart( double x, double y, double z ) const {
	double r  = sqrt(x*x + y*y + z*z);
	if(fabs(z) > r) r = fabs(z);	// Correct if (z/r) is greater than 1.0 by a few bits
	double th = acos(z / r);
	double ph = atan2(y, x);
	if(ph < 0.0)
		ph += M_2PI;
	return GetSphr(r, th, ph);
}

std::complex<double> JacobianSphereGrid::GetSphr( double rr, double th, double ph ) const {

	std::complex<double> res, plane1, plane2, plane3, plane4;
	double frac;

	// Origin
	if(rr == 0.0) {
		return std::complex<double>(data[0], data[1]);
	}

	double frc = (rr / stepSize);
	// Make sure there aren't any floating point rounding issues
	//  (e.g. int(0.99999999999999989) --> 0 instead of 1)
	double *fptr = &frc;
	(*((long long *)fptr)) += 8;

	int fInd = int(*fptr);
	frac = frc - double(fInd);

	plane2 = (fInd > 0) ? InterpolateThetaPhiPlane(fInd, th, ph) : std::complex<double>(data[0], data[1]);

 	if(frac < 1.e-9)
  		return plane2;
	plane3 = InterpolateThetaPhiPlane(fInd + 1, th, ph);

	plane1 = (fInd > 1) ? InterpolateThetaPhiPlane(fInd - 1, th, ph) : std::conj(plane3);
	// For the last plane, assume it continues straight (this can be corrected later in life)
	plane4 = (fInd + 2 < actualGridSize) ? InterpolateThetaPhiPlane(fInd + 2, th, ph) : plane3;

	std::complex<double> d1,d2;
	EvenlySpacedFourPointSpline(plane1, plane2, plane3, plane4, &d1, &d2);
	if (fInd == 0)
		d1 = 0.;

	return plane2 + d1 * frac + 
		(3.0 * (plane3 - plane2) - 2.0 * d1 - d2) * (frac*frac) + 
		(2.0 * (plane2 - plane3) + d1 + d2) * (frac*frac*frac);
}


int JacobianSphereGrid::GetSplineBetweenPlanes(FACC q, FACC th, FACC ph, OUT std::complex<double>& pl1, OUT std::complex<double>& pl2, OUT std::complex<double>& d1, OUT std::complex<double>& d2)
{
	std::complex<double> res, plane1, plane2, plane3, plane4;
	double frac;

	double frc = (q / stepSize);
	// Make sure there aren't any floating point rounding issues
	//  (e.g. int(0.99999999999999989) --> 0 instead of 1)
	double *fptr = &frc;
	(*((long long *)fptr)) += 8;

	int fInd = int(*fptr);
	frac = frc - double(fInd);

	plane2 = (fInd > 0) ? InterpolateThetaPhiPlane(fInd, th, ph) : std::complex<double>(data[0], data[1]);

	// 	if(frac < 1.e-9)
	//  		return plane2;
	plane3 = InterpolateThetaPhiPlane(fInd + 1, th, ph);

	plane1 = (fInd > 1) ? InterpolateThetaPhiPlane(fInd - 1, th, ph) :
		std::complex<double>(data[0], data[1]);
		//std::conj(plane3);
	// For the last plane, assume it continues straight (this can be corrected later in life)
	plane4 = (fInd + 2 < actualGridSize) ? InterpolateThetaPhiPlane(fInd + 2, th, ph) : plane3;

	EvenlySpacedFourPointSpline(plane1, plane2, plane3, plane4, &d1, &d2);
 	if (fInd == 0)
 		d1 = 0.;

	pl1 = plane2;
	pl2 = plane3;

	return std::max(fInd, 0);
}

ArrayXcX JacobianSphereGrid::getAmplitudesAtPoints(const std::vector<FACC> & relevantQs, FACC theta, FACC phi)
{
	ArrayXcX reses(relevantQs.size());
	size_t i = 0;

	std::complex<double> pl1, pl2;
	std::complex<double> d1, d2;
	
	double frac;

	auto  qForSpline = relevantQs.begin();
	while (qForSpline != relevantQs.end())
	{
		int planeIndex = GetSplineBetweenPlanes(*qForSpline, theta, phi, pl1, pl2, d1, d2);
		auto curQ = qForSpline;
		while (curQ != relevantQs.end() && *curQ < *qForSpline + stepSize)
		{
			double frc = (*curQ / stepSize);
			// Make sure there aren't any floating point rounding issues
			//  (e.g. int(0.99999999999999989) --> 0 instead of 1)
			double *fptr = &frc;
			(*((long long *)fptr)) += 8;

			frac = frc - double(planeIndex);

			reses(i) = pl1 + d1 * frac +
				(3.0 * (pl2 - pl1) - 2.0 * d1 - d2) * (frac*frac) +
				(2.0 * (pl1 - pl2) + d1 + d2) * (frac*frac*frac);

			i++;
			curQ++;
		} // while *curQ
		
		qForSpline = curQ;

	} // while qForSpline
	
	return reses;
} //getAmplitudesAtPoints

double* JacobianSphereGrid::GetDataPointer() {
	return data;

}

void JacobianSphereGrid::Fill( Amplitude *amp, void *prog, void *progArgs, double progMin, double progMax, int *pStop ) {
	progressFunc progFunc = (progressFunc)prog;

	const long long dims = totalsz / 2;

	int iprog = 0;

	// This is an inefficient form for the CPU. If using nested loops, sin/cos(theta) gets calculated a lot less.
	// It's implemented this way (for now) as a template for the GPU
#pragma omp parallel for schedule(guided, dims / (omp_get_num_procs() * 16))
	for(long long ind = 0; ind < dims; ind++) {
		std::complex<double> rs(0.0,0.0);
		int qi;
		long long thi, phi;
		
		// Determine q_vec as a function of the single index
		IndicesFromIndex(ind, qi, thi, phi);

		//const int dimy = int(thetaDivisions) * qi + 1;	// (4*i+1)
		const int dimz = int(phiDivisions) * qi;

		// Stop check		
		if(pStop && *pStop)
			continue;

		// Progress report
		if(progFunc) {
#pragma omp atomic
			++iprog;
			if((100 * iprog) % dims < 100) {
#pragma omp critical
				{			
					progFunc(progArgs, (progMax - progMin) * (double(iprog) / double(dims)) + progMin);
				}
			}
		}

		double qI = double(qi) * stepSize;
		double tI = M_PI  * double(thi) / double(int(thetaDivisions) * qi);	// dimy - 1
		double pI = M_2PI * double(phi) / double(dimz);

		double cst = cos(tI),
			   snt = sin(tI);

		double csp = cos(pI),
			   snp = sin(pI);

		if(qi == 0) {
			cst = snt = csp = snp = 0.0;
		}
#ifdef _DEBUG5
		if(ind < 1024) {
			std::cout << ind << ":\t(" << qi << ",\t" << thi << ",\t" << phi << ")";
			std::cout << "\t(" << qI << ",\t" << tI << ",\t" << pI << ")" <<std::endl;
		}
#endif
		rs = amp->calcAmplitude(qI * snt * csp, qI * snt * snp, qI * cst);

		data[2*ind]   = rs.real();
 		data[2*ind+1] = rs.imag();
	}

	CalculateSplines();
/*
	ExportThetaPhiPlane("S:\\Basic Dimer\\Grid Tests\\debug\\Plane 1.dat" , 1);
	ExportThetaPhiPlane("S:\\Basic Dimer\\Grid Tests\\debug\\Plane 5.dat" , 5);
	ExportThetaPhiPlane("S:\\Basic Dimer\\Grid Tests\\debug\\Plane 10.dat" , 10);
	ExportThetaPhiPlane("S:\\Basic Dimer\\Grid Tests\\debug\\Plane 50.dat" , 50);
*/
	/************************************************************************/
	/* Debug section                                                        */
	/* Check the validity of the IndexFromIndices and IndicesFromIndex      */
	/************************************************************************/
	if(false) {
		int qi;
		long long ti, pi, ret;
		for(long long i = 0; i < dims; i++) {
			IndicesFromIndex(i, qi, ti, pi);
			ret = IndexFromIndices(qi, ti, pi);
			if(ret != i)
				std::cout << "Bad!\t" << i << "\t" << ret << std::endl;

		}
	}
}

bool JacobianSphereGrid::ExportBinaryData( std::ostream& stream, const std::string& header ) const {
	if(!stream)
		return false;

	// Versions 0 and 1 are text
	// Version 1 has "\n" in the data region
	// Versions >= 10 contain the data as binary
	// Version 4 is reserved for files converted from formated to binary
	// Version 2 is reserved for files converted from binary to formatted
	// Version 11 uses the new form of grid and removed other crap
	// Version 12 uses the new form of spherical grid and removed other crap
	// Version 13 uses Uri's quasi-spherical grid

	unsigned int versionNum = 13;

	char DESCRIPTOR[3] = "#@";
	unsigned int headlen = (unsigned int)(2 * sizeof(char) + sizeof(unsigned int) + sizeof(char) + 
		                   header.length() * sizeof(char) + sizeof(char)); // descriptor + \n + header length + \n

	stream.write(DESCRIPTOR, 2 * sizeof(char));
	stream.write((const char *)&headlen, sizeof(unsigned int)); // 32-bit, always
	
	stream << "\n" << header << "\n";

	// File version number
	stream << versionNum << "\n";
	// Bytes per element
	stream << sizeof(std::complex<double>) << "\n";
	// Size of trnsfm --> txSize
	unsigned short dimx = GetDimX();
	stream << dimx << "\n";	
	stream << Extras << "\n";
	stream.write((const char *)&(stepSize), sizeof(double));
//	stream.write((const char *)&(existingRotation), sizeof(double)*9);

	// Later, for file-based grids
	/*
	headlen = (unsigned int)stream.tellp(); // Say we're 32-bit
	stream.seekp(2, ios::beg);
	stream.write((const char *)&headlen, sizeof(unsigned int)); // 32-bit, always
	stream.seekp(headlen, ios::beg);
	*/

	stream.write((const char *)(data), sizeof(double) * totalsz);

	return stream.good();

}

bool JacobianSphereGrid::ImportBinaryData( std::istream& stream, std::string &header ) {
	unsigned int versionNum = 0;
	unsigned int numBytes = 0;

	// Disabled, unnecessary
	//stream.seekg(0, ios::beg);

	char desc[2] = {0};
	unsigned int offset = 0;

	if(stream.peek() == '#') {
		stream.read(desc, 2);
		if(desc[1] == '@') { // We have an offset
			stream.read((char *)&offset, sizeof(unsigned int));
			std::string del;
			getline(stream, del);
		} else if(desc[1] != '\n') {
			std::string tmphead;
			getline(stream, tmphead);
			header.append(tmphead + "\n");
		}
	}

	// Read the header
	std::string head;
	while(stream.peek() == '#') {
		getline(stream, head);
		header.append(head + "\n");
	}

	if(offset > 0) {
		// Skip the header
		stream.seekg(offset, ios::beg);
	} else {
	}

	// Version
	stream >> versionNum;

	// Check if a binary file
	if(/*versionNum != 4 ||*/ versionNum < 10)
		return false;

	if(versionNum < 13)	// Decide what to do later
		return false;

	stream >> numBytes;
	if(numBytes != sizeof(std::complex<double>))
		return false;

	// Delta Q
	unsigned int tmpGridSize, tmpExtras;
	stream >> tmpGridSize;
	if(versionNum > 10)
		stream >> tmpExtras;

	if(actualGridSize != tmpGridSize)
		return false;
	if(versionNum > 10 && Extras != tmpExtras)
		return false;

	gridSize = (actualGridSize - Extras) * 2;

	// Ignore the newline
	stream.ignore(100, '\n');
	stream.read((char *)&(stepSize), sizeof(double));
//	stream.read((char *)&(existingRotation), sizeof(double)*9);
	return InnerImportBinaryData(stream);

}

void JacobianSphereGrid::Add( Grid* other, double scale ) {
	JacobianSphereGrid *rhs = dynamic_cast<JacobianSphereGrid*>(other);
	if(!rhs)
		return;

	if(GetSize() != rhs->GetSize())
		return;

#pragma omp parallel for
	for(long long pos = 0; pos < totalsz; pos++) {
		data[pos] += scale * rhs->data[pos];
	}

}

void JacobianSphereGrid::Scale( double scale ) {
#pragma omp parallel for schedule(dynamic, totalsz / (omp_get_num_procs() * 8))
	for(long long pos = 0; pos < totalsz; pos++)
		data[pos] *= scale;
}

bool JacobianSphereGrid::Validate() const {
	volatile bool res = true;

#pragma omp parallel for schedule(dynamic, totalsz / (omp_get_num_procs() * 8))
	for(long long pos = 0; pos < totalsz; pos++) {
		if(!res)
			continue;
		if(data[pos] != data[pos] || 
			data[pos] == std::numeric_limits<double>::infinity() ||
			data[pos] == -std::numeric_limits<double>::infinity() )
		{
			res = false;
			std::cout << "BAD! data[" << pos << "] = " << data[pos] << std::endl;
		}
	}
	if(!res) {
		std::cout << "Grid contains invalid numbers!" << std::endl;
	}
	return res;
}

bool JacobianSphereGrid::IndicesToVectors( unsigned short xi, unsigned short yi, unsigned short zi, OUT double &r, OUT double &thet, OUT double &phi ) {
	r = double(xi) * stepSize;
	thet = M_PI  * double(yi) / double(thetaDivisions * xi);	// +1 -1
	phi  = M_2PI * double(zi) / double(phiDivisions * xi);
	return true;

}

double * JacobianSphereGrid::GetPointerWithIndices() {
	// Input (in hex form)	// Numbers are indices and max indices
	// real part = [ THTH DYDY PHPH DZDZ ]
	// imag part = [ 0000 0000 0000 RRRR ]

	u64 *dataAsULL = (u64 *)data;

	size_t indexCtr = 0;

	const unsigned short dimx = GetDimX();
	for(unsigned short ri = 0; ri < dimx; ri++) {
		const unsigned short dimy = GetDimY(ri);

		for(unsigned short ti = 0; ti < dimy; ti++) {
			const unsigned short dimz = GetDimZ(ri, ti);

			for(unsigned short pi = 0; pi < dimz; pi++) {
				dataAsULL[indexCtr++] = (((u64)ti << 48) | ((u64)dimy << 32) | ((u64)pi << 16) | (u64)dimz);
				dataAsULL[indexCtr++] = (u64)ri;
			}
		}
	}

	return data;
}

bool JacobianSphereGrid::RunAfterCache() {
	if(data) {
		delete data;
		data = NULL;
	}
	if(interpolantCoeffs) {
		delete interpolantCoeffs;
		interpolantCoeffs = NULL;
	}
	return true;
}

bool JacobianSphereGrid::RunAfterReadingCache() {
	CalculateSplines();
	return true;
}

bool JacobianSphereGrid::RunBeforeReadingCache() {
	try {
		data = new double[totalsz];		
	} catch(...) {
		data = NULL;
	}

	if(!data) { // TODO: Report to user
		printf("ERROR: Unable to allocate enough space for data\n");
		gridSize = 0;
		actualGridSize = 0;
		return false;
	}

	return true;
}

bool JacobianSphereGrid::RotateGrid( double thet, double ph, double psi ) {
	// TODO::JacobianSphereGrid
	return false;
}

long long JacobianSphereGrid::IndexFromIndices( int qi, long long ti, long long pi ) const {
	if(qi == 0)
		return 0;
	qi--;
	long long base = ((qi+1)*(phiDivisions * qi)*(2 * thetaDivisions * qi + thetaDivisions + 3)) / 6;
	return (base + ti * phiDivisions * (qi+1) + pi + 1);	// The +1 is for the origin
}

void JacobianSphereGrid::IndicesFromIndex( long long index, int &qi, long long &ti, long long &pi ) {
	// Check the origin
	if(index == 0) {
		qi = 0;
		ti = 0;
		pi = 0;
		return;
	}

	long long bot, rem;
	// Find the q-radius
	// The numbers here assume thetaDivisions == 4 and phiDivisions == 8	--> no longer
	long long lqi = icbrt64((3*index)/(thetaDivisions*phiDivisions));
	bot = (lqi * phiDivisions * (lqi + 1) * (3 + thetaDivisions + 2 * thetaDivisions * lqi)) / 6;	//(lqi*(28 + lqi*(60 + 32*lqi))) / 3;
	if(index > bot )
		lqi++;
	lqi--;
	bot =(lqi * phiDivisions * (lqi + 1) * (3 + thetaDivisions + 2 * thetaDivisions * lqi)) / 6;	//(lqi*(28 + lqi*(60 + 32*lqi))) / 3;
	lqi++;
	qi = (int)lqi;
	rem = index - bot - 1;
	// Find the theta and phi radii
	ti = rem / (phiDivisions*qi);
	pi = rem % (phiDivisions*qi);

}

void JacobianSphereGrid::CalculateSplines() {
	if(interpolantCoeffs)
		delete interpolantCoeffs;
	try {
		interpolantCoeffs = new float[totalsz];
	} catch (...) {
		interpolantCoeffs = NULL;
	}
	if(!interpolantCoeffs) { // TODO: Report to user
		printf("ERROR: Unable to allocate enough space for interpolantCoeffs\n");
		return;
	}
	if(false) {
		memset(interpolantCoeffs, 0, totalsz * sizeof(float));
		printf("interpolantCoeffs set to zero\n");
		return;
	}
	std::cout << "Starting splines...";

    auto t1 = std::chrono::high_resolution_clock::now();

#define ORIGINAL_METHOD
#ifdef ORIGINAL_METHOD
	//cpu.start();
	//start_time.wall = cpu.elapsed().wall;
	interpolantCoeffs[0] = interpolantCoeffs[1] = 0.0f;
	// The [0] is the origin and doesn't need interpolation
#pragma omp parallel for schedule(dynamic, actualGridSize / (omp_get_num_procs() * 4))
	for(int i = 1; i <= actualGridSize; i++) {
		long long thetaRange = thetaDivisions * i + 1;
		for(long long j = 0; j < thetaRange; j++) {
			long long pos = IndexFromIndices(i, j, 0);
#ifdef _DEBUG
			//std::cout << pos << ":\t(" << i << ",\t" << j << ")\t" << i * phiDivisions <<std::endl;
#endif

			pos *= 2;
			EvenySpacedPeriodicSplineSolver((std::complex<double>*)(&data[pos]), (std::complex<float>*)(&interpolantCoeffs[pos]), i * phiDivisions);
		}
	}
    auto t2 = std::chrono::high_resolution_clock::now();
	std::cout << " Done! Took "
              << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << "us" << std::endl;
	if(false) {
		float *altInterpolantCoeffs = new float[totalsz];
		// The [0] is the origin and doesn't need interpolation
		altInterpolantCoeffs[0] = altInterpolantCoeffs[1] = 0.0f;
	#endif	// ORIGINAL_METHOD

		std::cout << "Alternative method:";
		// Alternative method
        auto t3 = std::chrono::high_resolution_clock::now();

		// Calculate the c' values
		int largestMatrixDim = thetaDivisions * actualGridSize + 1;

		// Call Tridiagonal matrix algorithm using Tempertons Algorithm 4
	#pragma omp parallel for schedule(dynamic, actualGridSize / (omp_get_num_procs() * 4))
		for(int i = 1; i <= actualGridSize; i++) {
			long long thetaRange = thetaDivisions * i + 1;
			for(long long j = 0; j < thetaRange; j++) {
				long long pos = IndexFromIndices(i, j, 0);

				pos *= 2;
				TempertonEvenySpacedPeriodicSplineSolver((std::complex<double>*)(&data[pos]),
	#ifndef ORIGINAL_METHOD
					(std::complex<float>*)(&interpolantCoeffs[pos]),
	#else
					(std::complex<float>*)(&altInterpolantCoeffs[pos]),
	#endif
					i * phiDivisions);
			}
		}
        auto t4 = std::chrono::high_resolution_clock::now();
		std::cout << " Done! Took "
                  << std::chrono::duration_cast<std::chrono::microseconds>(t4 - t3).count() << "us for Algorithm 4" << std::endl;
	#ifdef ORIGINAL_METHOD
		u64 worstInd = 0;
		double maxDiff = 0.0, maxLinDiff = 0.0, meanDiff = 0.0;
		for(long long i = 0; i < totalsz; i++) {
			meanDiff += fabs(altInterpolantCoeffs[i] - interpolantCoeffs[i]);
			double v = (closeToZero(altInterpolantCoeffs[i]) ? 0.0 : fabs(1.0 - interpolantCoeffs[i] / altInterpolantCoeffs[i]));
			if(v > maxDiff && !closeToZero(altInterpolantCoeffs[i])
				&& fabs(interpolantCoeffs[i] - altInterpolantCoeffs[i]) > 2.0e-5
				) {
					maxDiff = v;
					worstInd = i;
			}
			if(fabs(interpolantCoeffs[i] - altInterpolantCoeffs[i]) > maxLinDiff) {
				maxLinDiff = fabs(altInterpolantCoeffs[i] - interpolantCoeffs[i]);
			}
		}

		std::cout << "Mean error: " << meanDiff / double(totalsz) << "\n";
		std::cout << "Max (1-a/b) error: " << maxDiff  << "\t-log: "<< -log10(maxDiff) << "\n";
		std::cout << "\t worst values: " << interpolantCoeffs[worstInd] << " " <<
						altInterpolantCoeffs[worstInd] << "\t" << -log10(fabs(altInterpolantCoeffs[worstInd])) << std::endl;
		std::cout << "Max linear (a-b) error: " << maxLinDiff << "\n";


		delete altInterpolantCoeffs;
	#endif // ORIGINAL_METHOD
	}
}

bool JacobianSphereGrid::ExportThetaPhiPlane( std::string outFileName, long long index ) {
	fs::path pathName(outFileName);
	pathName = fs::system_complete(outFileName);
	std::wstring a1, a2, a3;
	a1 = pathName.leaf().wstring();
	a2 = pathName.parent_path().wstring();

	if(!fs::exists(pathName.parent_path())) {
		string strD = pathName.parent_path().string();
		boost::system::error_code er;
		if(!fs::create_directories(pathName.parent_path(), er) ) {
			std::cout << "\nError code: " << er << "\n";
			while(!fs::exists(pathName.parent_path())) {
				pathName = pathName.parent_path();
			}
			pathName = fs::wpath(pathName.string() + "ERROR_CREATING_DIR");
			{fs::ofstream f(pathName);}
			return false;
		}
	}

	long long base;
	long long lqi = index-1;
	base = 1 + (lqi * phiDivisions * (lqi + 1) * (3 + thetaDivisions + 2 * thetaDivisions * lqi)) / 6;	//(lqi*(28 + lqi*(60 + 32*lqi))) / 3;
// 	if( index > base )
// 		lqi++;
// 	lqi--;
// 	base = 1 + (lqi*(28 + lqi*(60 + 32*lqi))) / 3;
	lqi++;
	long long len;
	if(index == 0) {
		base = 0;
		len = 2 * 1;
	} else {
		len = 2 * ((thetaDivisions * lqi + 1) * phiDivisions * lqi);
	}
	fs::fstream writeFile;
	writeFile.open(pathName, /*ios::binary |*/ ios::out);
	if(writeFile.is_open()) {
		if(data) {
			//writeFile.write((const char *)(data + 2*base), sizeof(double) * len);
			for(int i = 0; i < thetaDivisions * lqi + 1; i++) {
				for(int j = 0; j < phiDivisions * lqi; j++) {	// Just the real part for now
					writeFile << *(data + 2*(base + i * phiDivisions * lqi + j)) << " ";
				}
				writeFile << std::endl;
			}
		}
		writeFile.close();
	}

	return true;
}

float *JacobianSphereGrid::GetInterpolantPointer() {
	return interpolantCoeffs;
}

void JacobianSphereGrid::DebugMethod() {
#ifdef _DEBUG__875
	long long pp = 1350;
	std::cout << "data["<< pp << "] = " << data[pp] << " data["<< pp+1 << "] = " << data[pp+1] << std::endl;
	std::cout << "interpolantCoeffs["<< pp << "] = " << interpolantCoeffs[pp] << 
		" interpolantCoeffs["<< pp+1 << "] = " << interpolantCoeffs[pp+1] << std::endl;
#endif
}

void JacobianSphereGrid::IndicesFromRadians( const u16 ri, const double theta, const double phi,
											long long &tI, long long &pI, long long &base, double &tTh, double &tPh ) const {
	// Determine the first cell using ri
	int qi = ri - 1;
	base = 1 + ((qi+1)*(phiDivisions * qi)*(2 * thetaDivisions * qi + thetaDivisions + 3)) / 6;

	// Determine the lowest neighbors coordinates within the plane
	int phiPoints = phiDivisions * ri;
	int thePoints = thetaDivisions * ri;
	double edge = M_2PI / double(phiPoints);

	tI = (theta / M_PI) * double(thePoints);
	pI = (phi  / M_2PI) * double(phiPoints);

	// The value [0, 1] representing the location ratio between the two points
	tTh = (theta / edge) - tI; //fmod(theta, edge) / edge;
	tPh = (phi   / edge) - pI; //fmod(phi, edge) / edge;

	if(fabs(tTh) < 1.0e-10)
		tTh = 0.0;
	if(fabs(tPh) < 1.0e-10)
		tPh = 0.0;
	assert(tTh >= 0.0);
	assert(tPh >= 0.0);
	assert(tTh <= 1.0000000001);
	assert(tPh <= 1.0000000001);

	//pI = (pI == phiPoints) ? 0 : pI;
	if(pI == phiPoints) {
		assert(tPh <= 0.000001);
		pI = 0;
	}
}

#endif
