#ifndef __GRID_H
#define __GRID_H

#pragma once
#include "Common.h"
#include <complex>
#include <vector>
#include "Eigen/Core"
#include <omp.h>

// Forward declaration
class Amplitude;

typedef Eigen::Array<std::complex<FACC>, Eigen::Dynamic, 1> ArrayXcX;

// Grid interface
class EXPORTED_BE Grid {
protected:
	double qmax;	/**< The effective maximum q-value the grid can be used for. Units of \f$nm^{-1}\f$. */
	double stepSize;	/**< The size of a step between two adjacent q-values. Units of \f$nm^{-1}\f$. */
	unsigned short gridSize, actualGridSize;
	unsigned short Extras;
	unsigned int versionNum, bytesPerElement;

	//Eigen::Matrix3d existingRotation;

	virtual void SetCart(double x, double y, double z, std::complex<double> val) = 0;
	virtual void SetSphr(double x, double y, double z, std::complex<double> val) = 0;

public:
	Grid() : qmax(0.0), gridSize(0), actualGridSize(0), stepSize(0) {
//		existingRotation.setIdentity(3,3);
	}

	Grid(unsigned short gridsz, double qMax) : qmax(qMax), gridSize(gridsz) {
		Extras = 11;	// Up until file version 10 (incl) this was 7
		actualGridSize = gridsz + Extras;

		// Since grid is in the range [-qmax, qmax], stepsize = 2qmax / gridsize
		stepSize = 2.0 * qMax / double(gridSize); 
//		existingRotation.setIdentity(3,3);
	}

	virtual ~Grid() {}

	// Not today...
	// Only returns required size if data == NULL
	/*virtual unsigned long long CopyTo(double *data) = 0;
	virtual bool CopyFrom(const Grid *other) = 0;*/
	
	virtual unsigned short GetDimX() const = 0;
	virtual unsigned short GetDimY(unsigned short x) const = 0;
	virtual unsigned short GetDimZ(unsigned short x, unsigned short y) const = 0;

	virtual double* GetDataPointer() = 0;

	virtual std::complex<double> GetCart(double x, double y, double z) const = 0;	
	virtual std::complex<double> GetSphr(double x, double y, double z) const = 0;	
	
	virtual void Fill(Amplitude *amp, void *progFunc, void *progArgs, double progMin, double progMax, int *pStop) = 0;

	// TODO: We can refactor import and export binary data, given the "InnerImport"/"InnerExport"
	//       methods implemented in every class, in conjunction with bytesPerElement and versionNum

	virtual bool ExportBinaryData(std::ostream& stream, const std::string& header) const = 0;

	// Fails if file is not of the same gridsize or stepsize
	virtual bool ImportBinaryData(std::istream& stream, std::string &header) = 0;

	virtual unsigned short GetSize() const { return gridSize; }
	virtual u64 GetRealSize() const = 0;
	virtual double GetQMax() const { return qmax; }
	virtual double GetStepSize() const { return stepSize; }

	virtual void Add(Grid* other, double scale) = 0;
	virtual void Scale(double scale) = 0;
	virtual bool Validate() const = 0;

	virtual bool IndicesToVectors(unsigned short xi, unsigned short yi, unsigned short zi, OUT double &x, OUT double &y, OUT double &z) = 0;

	// Returns a pointer to the data, filled with the x,y,z (dimy, dimz) indices
	virtual double *GetPointerWithIndices() = 0;

	virtual bool RunAfterCache() = 0;
	virtual bool RunAfterReadingCache() = 0;
	virtual bool RunBeforeReadingCache() = 0;
};

class GridOld : public Grid {
protected:
	std::vector<std::vector<std::vector<std::complex<double> > > > trnsfm;	
	u64 totalsz;

	virtual bool InnerImportBinaryData(std::istream& file);

	virtual void InitializeGrid();


	virtual void Set(double x, double y, double z, std::complex<double> val);

public:
	GridOld(unsigned short gridSize, double qMax);

	GridOld(std::istream& stream, std::string &header);

	GridOld() {}

	~GridOld();	

	virtual unsigned short GetDimX() const;	
	virtual unsigned short GetDimY(unsigned short x) const;
	virtual unsigned short GetDimZ(unsigned short x, unsigned short y) const;

	virtual u64 GetRealSize() const { return totalsz * sizeof(double); }

	virtual std::complex<double> GetCart(double x, double y, double z) const;	
	virtual std::complex<double> GetSphr(double r, double th, double ph) const;	

	virtual void Fill(Amplitude *amp, void *progFunc, void *progArgs, double progMin, double progMax, int *pStop);

	virtual void Add(Grid* other, double scale);
	virtual void Scale(double scale);

	virtual bool Validate() const;

	virtual bool ExportBinaryData(std::ostream& stream, const std::string& header) const;
	virtual bool ImportBinaryData(std::istream& stream, std::string &header);	

	virtual bool IndicesToVectors(unsigned short xi, unsigned short yi, unsigned short zi, OUT double &x, OUT double &y, OUT double &z);

	virtual double *GetPointerWithIndices() { return NULL; } // TODO
	virtual double *GetDataPointer() { return NULL; } // TODO
};

class GridV10Old : public GridOld {
public:
	GridV10Old(unsigned short gridSize, double qMax);

	GridV10Old(std::istream& stream, std::string &header);

	~GridV10Old();

	virtual void InitializeGrid();

	virtual bool ExportBinaryData(std::ostream& stream, const std::string& header) const;
	virtual bool InnerImportBinaryData(std::istream& file);

	virtual std::complex<double> GetCart(double x, double y, double z) const;	
	virtual std::complex<double> GetSphr(double r, double th, double ph) const;	

	virtual bool IndicesToVectors(unsigned short xi, unsigned short yi, unsigned short zi, OUT double &x, OUT double &y, OUT double &z) { return false; }


};

class EXPORTED_BE GridB : public Grid {
protected:
	double *data;
	// DO NOT MODIFY TO u64, OPENMP (v2.0) DOES NOT SUPPORT UNSIGNED TYPES
	long long totalsz; // 64-bit value (number of double elements, 2 times number of complex elements)

	std::vector< std::vector<unsigned short> > sphereEnds;
	std::vector< std::vector<u64> > coordMap;

	unsigned short center;
	int xxmax;

	// Call with 0 for real value, call with 1 for imaginary
	double Evaluate(unsigned short x, unsigned short y, unsigned short z, int im) const;

	virtual bool InnerImportBinaryData(std::istream& file);

	inline long long XYtoIndex(unsigned short x, unsigned short y) const;

	virtual void SetCart(double x, double y, double z, std::complex<double> val);
	virtual void SetSphr(double r, double th, double ph, std::complex<double> val);
public:
	GridB(unsigned short gridSize, double qMax);
	GridB(std::istream& stream, std::string &header);

	~GridB();

	virtual unsigned short GetDimX() const;
	virtual unsigned short GetDimY(unsigned short x) const;
	virtual unsigned short GetDimZ(unsigned short x, unsigned short y) const;

	virtual double* GetDataPointer() { return data; }

	virtual u64 GetRealSize() const { return totalsz * sizeof(double); }

	virtual void InitializeGrid();	

	virtual std::complex<double> GetCart(double x, double y, double z) const;	
	virtual std::complex<double> GetSphr(double r, double th, double ph) const;	

	virtual void Add(Grid* other, double scale);virtual void Scale(double scale);


	virtual void Fill(Amplitude *amp, void *progFunc, void *progArgs, double progMin, double progMax, int *pStop);

	virtual bool Validate() const;


	virtual bool ExportBinaryData(std::ostream& stream, const std::string& header) const;
	virtual bool ImportBinaryData(std::istream& stream, std::string &header);

	virtual bool IndicesToVectors(unsigned short xi, unsigned short yi, unsigned short zi, OUT double &x, OUT double &y, OUT double &z);
	virtual double *GetPointerWithIndices();

	virtual bool RunAfterCache();
	virtual bool RunAfterReadingCache();
	virtual bool RunBeforeReadingCache();

	virtual bool RotateGrid( double thet, double ph, double psi );

};

class GridSphere : public Grid {
protected:
	double *data;
	// DO NOT MODIFY TO u64, OPENMP DOES NOT SUPPORT UNSIGNED TYPES
	long long totalsz; // 64-bit value (number of double elements, 2 times number of complex elements)

	std::vector< std::vector<unsigned short> > sphereEnds;
	std::vector< std::vector<u64> > coordMap;

	unsigned short center;
	int xxmax;

	// Call with 0 for real value, call with 1 for imaginary
	double Evaluate(unsigned short x, unsigned short y, unsigned short z, int im) const;

	virtual bool InnerImportBinaryData(std::istream& file);

	inline long long XYtoIndex(unsigned short x, unsigned short y) const;

	virtual void Set(double x, double y, double z, std::complex<double> val);
public:
	GridSphere(unsigned short gridSize, double qMax);
	GridSphere(std::istream& stream, std::string &header);

	~GridSphere();

	virtual unsigned short GetDimX() const;                    // DimR
	virtual unsigned short GetDimY(unsigned short x) const;    // DimPhi
	virtual unsigned short GetDimZ(unsigned short x, unsigned short y) const; // DimTheta

	virtual u64 GetRealSize() const { return totalsz * sizeof(double); }

	virtual void InitializeGrid();	

	virtual std::complex<double> GetCart(double x, double y, double z) const;	
	virtual std::complex<double> GetSphr(double r, double th, double ph) const;	

	virtual void Add(Grid* other, double scale);
	virtual void Scale(double scale);


	virtual void Fill(Amplitude *amp, void *progFunc, void *progArgs, double progMin, double progMax, int *pStop);

	virtual bool Validate() const;


	virtual bool ExportBinaryData(std::ostream& stream, const std::string& header) const;
	virtual bool ImportBinaryData(std::istream& stream, std::string &header);

	virtual bool IndicesToVectors(unsigned short xi, unsigned short yi, unsigned short zi, OUT double &x, OUT double &y, OUT double &z);
	virtual double *GetPointerWithIndices();
};

// GridSuperblock: A grid structure of 2x2x2 superblocks
class GridSuperblock : public Grid {
protected:
	union Superblock {
		// Note the order of the data in the structure (it is the order in which they are read)
		struct interp {
			double r000, r100, i000, i100, r010, r110, i010, i110, 
				   r001, r101, i001, i101, r011, r111, i011, i111;
		} interpolant;

		double vals[16];
	};

	// TODO: Maybe std::vector based?
	Superblock *data;
	// DO NOT MODIFY TO u64, OPENMP DOES NOT SUPPORT UNSIGNED TYPES
	long long numElems; // 64-bit value (number of superblocks = number of complex elements / 8)

	std::vector<std::vector<unsigned short> > sphereEnds;
	std::vector<std::vector<u64> > coordMap;

	unsigned short center;
	int xxmax;

	virtual bool InnerImportBinaryData(std::istream& file);

	inline long long XYtoIndex(unsigned short x, unsigned short y) const;

	virtual void Set(double x, double y, double z, std::complex<double> val);
public:
	GridSuperblock(unsigned short gridSize, double qMax);
	GridSuperblock(std::istream& stream, std::string &header);

	~GridSuperblock();

	virtual unsigned short GetDimX() const;
	virtual unsigned short GetDimY(unsigned short x) const;
	virtual unsigned short GetDimZ(unsigned short x, unsigned short y) const;

	virtual u64 GetRealSize() const { return numElems * sizeof(Superblock); }

	virtual void InitializeGrid();	

	virtual std::complex<double> GetCart(double x, double y, double z) const;	
	//virtual std::complex<double> GetSphr(double r, double th, double ph) const;	

	virtual void Add(Grid* other, double scale);
	virtual void Scale(double scale);


	virtual void Fill(Amplitude *amp, void *progFunc, void *progArgs, double progMin, double progMax, int *pStop);

	virtual bool Validate() const;


	virtual bool ExportBinaryData(std::ostream& stream, const std::string& header) const;
	virtual bool ImportBinaryData(std::istream& stream, std::string &header);

	virtual double *GetPointerWithIndices() { return NULL; } // TODO

};

class EXPORTED_BE SphereGrid : public Grid {
protected:
	double *data;
	// DO NOT MODIFY TO u64, OPENMP (v2.0) DOES NOT SUPPORT UNSIGNED TYPES (and MS doesn't seem to want to upgrade)
	long long totalsz; // 64-bit value (number of double elements, 2 times number of complex elements)

	std::vector<std::vector<unsigned short> > radii;
	std::vector<std::vector<u64> > coordMap;
	std::vector<std::vector<u16> > sizeMap;
	// The step sizes are not in radians, but in nm^{-1}. To get theta, take \pi * \frac{index}{phiStepSizes[i].size() - 1} 
	std::vector<double> thetaStepSizes;
	std::vector<std::vector<double> > phiStepSizes;

	inline std::complex<double> InterpolateThetaPhiPlane(const u16 ri, const double rri, const u16 tI, const double theta, const u16 pI, const double phi) const;
// 	virtual inline std::complex<double> CircularSpline(std::vector<std::complex<double>>::iterator &firstPoint,
// 				std::vector<std::complex<double>>::iterator &lastPoint, double stepSz);
// 	virtual inline std::complex<double> BilinearInterpolate(); // ?

	virtual void SetCart(double x, double y, double z, std::complex<double> val);
	virtual void SetSphr(double x, double y, double z, std::complex<double> val);

	virtual void InitializeGrid();

	virtual bool InnerImportBinaryData(std::istream& file);

public:
	SphereGrid(unsigned short gridSize, double qMax);
	SphereGrid(std::istream& stream, std::string &header);

	~SphereGrid();

	virtual unsigned short GetDimX() const;                    // DimR
	virtual unsigned short GetDimY(unsigned short x) const;    // DimPhi
	virtual unsigned short GetDimZ(unsigned short x, unsigned short y) const; // DimTheta

	virtual std::complex<double> GetCart(double x, double y, double z) const;	
	virtual std::complex<double> GetSphr(double r, double th, double ph) const;	

	virtual double* GetDataPointer();

	virtual void Fill(Amplitude *amp, void *progFunc, void *progArgs, double progMin, double progMax, int *pStop);

	// TODO: We can refactor import and export binary data, given the "InnerImport"/"InnerExport"
	//       methods implemented in every class, in conjunction with bytesPerElement and versionNum

	virtual bool ExportBinaryData(std::ostream& stream, const std::string& header) const;

	// Fails if file is not of the same gridsize or stepsize
	virtual bool ImportBinaryData(std::istream& stream, std::string &header);

	virtual u64 GetRealSize() const { return totalsz * sizeof(double); }

	virtual void Add(Grid* other, double scale);
	virtual void Scale(double scale);
	virtual bool Validate() const;

	virtual bool IndicesToVectors(unsigned short xi, unsigned short yi, unsigned short zi, OUT double &x, OUT double &y, OUT double &z);

	// Returns a pointer to the data, filled with the x,y,z (dimy, dimz) indices
	virtual double *GetPointerWithIndices();

	virtual bool RunAfterCache();
	virtual bool RunAfterReadingCache();
	virtual bool RunBeforeReadingCache();

	virtual bool RotateGrid( double thet, double ph, double psi );

};

#define URIS_GRID
#ifdef URIS_GRID
/************************************************************************/
/* JacobianSphereGrid: Each shell is a square on the theta-phi plane    */
/************************************************************************/
class EXPORTED_BE JacobianSphereGrid : public Grid {
protected:
	double *data;
	// DO NOT MODIFY TO u64, OPENMP (v2.0) DOES NOT SUPPORT UNSIGNED TYPES (and MS doesn't seem to want to upgrade)
	long long totalsz; // 64-bit value (number of double elements, 2 times number of complex elements)

	float *interpolantCoeffs;

	char thetaDivisions, phiDivisions;	// 4*i+1 --> 4 and 8*i --> 8
	//double rotatedTheta, rotatedPhi;

	inline long long IndexFromIndices(int qi, long long ti, long long pi) const;
	inline void IndicesFromIndex(long long index, int &qi, long long &ti, long long &pi);
	inline void IndicesFromRadians(const u16 ri, const double theta, const double phi,
		long long &ti, long long &pi, long long &base, double &tTh, double &tPh) const;

	virtual void SetCart(double x, double y, double z, std::complex<double> val);
	virtual void SetSphr(double q, double th, double ph, std::complex<double> val);

	virtual void InitializeGrid();

	virtual bool InnerImportBinaryData(std::istream& file);

	inline std::complex<double> InterpolateThetaPhiPlane(const u16 ri, const double theta, const double phi) const;

public:
	JacobianSphereGrid(unsigned short gridSize, double qMax);
	JacobianSphereGrid(std::istream& stream, std::string &header);

	~JacobianSphereGrid();

	virtual unsigned short GetDimX() const;                    // DimR
	virtual unsigned short GetDimY(unsigned short x) const;    // DimPhi
	virtual unsigned short GetDimZ(unsigned short x, unsigned short y) const; // DimTheta

	virtual std::complex<double> GetCart(double x, double y, double z) const;	
	virtual std::complex<double> GetSphr(double r, double th, double ph) const;	

	virtual int GetSplineBetweenPlanes(FACC q, FACC theta, FACC phi, OUT std::complex<double>& pl1, OUT std::complex<double>& pl2, OUT std::complex<double>& d1, OUT std::complex<double>& d2);
	virtual ArrayXcX getAmplitudesAtPoints(const std::vector<FACC> & relevantQs, FACC theta, FACC phi);

	virtual double* GetDataPointer();

	virtual void Fill(Amplitude *amp, void *progFunc, void *progArgs, double progMin, double progMax, int *pStop);

	void CalculateSplines();

	// TODO: We can refactor import and export binary data, given the "InnerImport"/"InnerExport"
	//       methods implemented in every class, in conjunction with bytesPerElement and versionNum

	virtual bool ExportBinaryData(std::ostream& stream, const std::string& header) const;

	// Fails if file is not of the same gridsize or stepsize
	virtual bool ImportBinaryData(std::istream& stream, std::string &header);

	virtual u64 GetRealSize() const { return totalsz * sizeof(double); }

	virtual void Add(Grid* other, double scale);
	virtual void Scale(double scale);
	virtual bool Validate() const;

	virtual bool IndicesToVectors(unsigned short xi, unsigned short yi, unsigned short zi, OUT double &x, OUT double &y, OUT double &z);

	// Returns a pointer to the data, filled with the x,y,z (dimy, dimz) indices
	virtual double *GetPointerWithIndices();

	virtual float *GetInterpolantPointer();

	virtual bool RunAfterCache();
	virtual bool RunAfterReadingCache();
	virtual bool RunBeforeReadingCache();

	virtual bool RotateGrid( double thet, double ph, double psi );

	virtual bool ExportThetaPhiPlane(std::string outFileName, long long index);

	virtual void DebugMethod();
	
};
#endif	//URIS_GRID

#define USE_JACOBIAN_SPHERE_GRID
#define USE_SPHERE_GRID
#ifdef USE_SPHERE_GRID
	#ifdef USE_JACOBIAN_SPHERE_GRID
		typedef JacobianSphereGrid CurGrid;
		#define CURGRIDNAME   "JacobianSphereGrid"
		#define WCURGRIDNAME L"JacobianSphereGrid"
	#else
		typedef SphereGrid CurGrid;
		#define CURGRIDNAME   "SphereGrid"
		#define WCURGRIDNAME L"SphereGrid"
	#endif
#else
	typedef GridB CurGrid;
	#define CURGRIDNAME   "GridB"
	#define WCURGRIDNAME L"GridB"
#endif	// USE_SPHERE_GRID
#endif	// __GRID_H
