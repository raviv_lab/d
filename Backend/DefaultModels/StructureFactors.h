#ifndef __STRUCTUREFACTORS_H
#define __STRUCTUREFACTORS_H

#include "Geometry.h"


	class PeakBasedSF : public SFModel {
	protected:
		VectorXd amp, width, xCenter;
	public:
		PeakBasedSF(std::string st = "Abstract Peak SF Model", int nlp = 3) : SFModel(st, 0, nlp, 0) {};

		// Virtual destructor
		virtual ~PeakBasedSF() {}

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

		static std::string GetLayerParamName(int index, const EDPFunction *edpfunc);

		static ExtraParam GetExtraParameter(int index);

		static double GetDefaultParamValue(int paramIndex, int layer, const EDPFunction *edpfunc);

		static std::string GetLayerName(int layer);

		static bool IsParamApplicable(int layer, int lpindex);

		virtual void PreCalculate(VectorXd& p, int nLayers);

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p) = 0;

	};

	class MixedPeakBasedSF : public SFModel {
	protected:
		VectorXd amp, width, xCenter;
		VectorXi funcType;
	public:
		MixedPeakBasedSF(std::string st = "Mixed Peak SF Model", int nlp = 4) : SFModel(st, 0, nlp, 0) {};

		// Virtual destructor
		virtual ~MixedPeakBasedSF() {}

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

		static ExtraParam GetExtraParameter(int index);

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);

	};

	class CailleSF : public SFModel {
	protected:
		double D, amp, eta, N0, sig, NDiff, Q;
		VectorXd resF, qGlob;
		Eigen::MatrixXd ex1;
		double qmax;

	public:
		CailleSF(std::string st = "Caille SF Model", int nlp = 6) : SFModel(st, 0, nlp, 1, 1) {};

		// Virtual destructor
		virtual ~CailleSF() {}

		virtual void OrganizeParameters(const VectorXd& p, int nLayers);

		virtual void PreCalculate(VectorXd& p, int nLayers);

		virtual VectorXd CalculateVector(const std::vector<double>& q, int nLayers, VectorXd& p);

		static ExtraParam GetExtraParameter(int index);

		static std::string GetLayerParamName(int index);

		static double GetDefaultParamValue(int paramIndex, int layer);

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);
	};


	class GaussianSigmaPeakSFModel : public PeakBasedSF {
	protected:

	public:
		GaussianSigmaPeakSFModel() : PeakBasedSF("Gaussian Sigma Peak") {};

		virtual ~GaussianSigmaPeakSFModel() {}

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);
	};

	class LorentzianPeakSFModel : public PeakBasedSF {
	protected:
	public:
		LorentzianPeakSFModel() : PeakBasedSF("Lorentzian Peak") {};

		virtual ~LorentzianPeakSFModel() {}

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);
	};

	class LorentzianSquaredPeakSFModel : public PeakBasedSF {
	protected:
	public:
		LorentzianSquaredPeakSFModel() : PeakBasedSF("Lorentzian Squared Peak") {};

		virtual ~LorentzianSquaredPeakSFModel() {}

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);
	};

#endif
