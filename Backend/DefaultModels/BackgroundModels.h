#ifndef __BGMODELS_H
#define __BGMODELS_H

#include "Geometry.h"

	class AdditiveBG : public BGModel {
	protected:
		Eigen::VectorXd amp, decay, xCenter;
		Eigen::VectorXi funcType;
		// TODO::BG
	public:
		AdditiveBG();

		// Virtual destructor
		virtual ~AdditiveBG() {}

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

		static ExtraParam GetExtraParameter(int index);

		static std::string GetLayerParamName(int index);

		static std::string GetLayerName(int layer);

		static bool IsParamApplicable(int layer, int lpindex);

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);

	};

#endif
