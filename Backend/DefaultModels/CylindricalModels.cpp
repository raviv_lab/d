//#define NOMINMAX
//#include "Windows.h"	//For min/max and messagebox debugging


#include "CylindricalModels.h"
#include "Quadrature.h" // For SetupIntegral
#include "kronrod.hpp"
#include "mathfuncs.h" // For bessel functions and square

using Eigen::VectorXf;

#pragma region Abstract Cylindrical Geometry

CylindricalModel::CylindricalModel(int integralSteps, std::string str, ProfileShape eds, int nlp, int minlayers, int maxlayers, int extras)
							: FFModel(str, extras, nlp, minlayers, maxlayers, EDProfile(SYMMETRIC, eds)) {
	steps = integralSteps;
	SetupIntegral(xx, ww, 0.0f, 1.0f, steps);
}

ExtraParam CylindricalModel::GetExtraParameter(int index) {
	if(index == 2)
		return ExtraParam("Height", 10, 
						  true, true);
	
	// Default extra parameters
	return Geometry::GetExtraParameter(index);	
}

void CylindricalModel::OrganizeParameters(const Eigen::VectorXd &p, int nLayers) {
	Geometry::OrganizeParameters(p, nLayers);
	t	= (*parameters).col(0);
	ed	= (*parameters).col(1);
	H = (*extraParams)[2] / 2.0;
	edSolvent = ed[0]; 
}

#pragma endregion

#pragma region Uniform Cylindrical Model

UniformHCModel::UniformHCModel(int integralSteps, std::string str, ProfileShape eds, int nlp, int minlayers, int maxlayers, int extras) :
						CylindricalModel(integralSteps, str, eds, nlp, minlayers, maxlayers, extras) {
}

void UniformHCModel::PreCalculate(VectorXd& p, int nLayers){
	OrganizeParameters(p, nLayers);
}

void UniformHCModel::OrganizeParameters(const VectorXd& p, int nLayers) {
	CylindricalModel::OrganizeParameters(p, nLayers);

	for(int i = 1; i < t.size(); i++)
		t[i] = t[i - 1] + t[i];
}

double UniformHCModel::Calculate(double q, int nLayers, VectorXd& a) {
	double H = -1.0; 
    double scale, background;
    double intensity = 0.0;

	if(a.size() > 0)
		OrganizeParameters(a, nLayers);
	
	scale = (*extraParams)[0];
    background = (*extraParams)[1];
	H = std::isinf((*extraParams)[2]) ? -1.0 :
							(*extraParams)[2]/2.0;
    
	/*Pablo & Avi's model -----  */
	int notZero = 0;
	for(notZero = 0; (notZero < nLayers) && (t[notZero] <= 0.0); notZero++);
	if(notZero == nLayers)
		return 0.0;	//No layer thickness

	for(int i = nLayers - 1; i >= 0; i--)
		ed[i] -= ed[0];
	/*Finite*/
	if(H >= 0.0) {
		
#pragma omp parallel for reduction(+ : intensity)
		for(int i = 0; i < steps; i++) {
			if (xx[i] > 0.0 && xx[i] < 1.0) {			
				double temp = 0.0;

				if(pStop && *pStop)		// Place these line strategically in
					continue;			// slow models.

				for(int j = notZero; j < nLayers - 1; j++) {
					temp += (ed[j] - ed[j + 1])* t[j] * bessel_j1(q * sqrt(1.0 - xx[i] * xx[i]) * t[j]); 								
				}
				temp += ed[nLayers - 1] * t[nLayers- 1] * bessel_j1(q * sqrt(1.0 - xx[i] * xx[i]) * t[nLayers - 1]);
				temp *= 4.0 * PI * sin(q * xx[i] * H) / (q * q * xx[i] * sqrt(1.0 - xx[i] * xx[i]));
				intensity += temp * temp * ww[i];
			}
		}
	}
	
	/*Pablo & Avi's model -----  Infinite*/
	else {
		// "pre"-calculate the bessel coefficients [R * J1(qR)]
		VectorXd besselCoeff = VectorXd::Zero(nLayers);
		for(int i = 0; i < nLayers; i++)
			besselCoeff[i] = t[i] * bessel_j1(q * t[i]);

		double temp = 0.0;
		for(int j = notZero; j < nLayers - 1; j++) {
			temp += (ed[j] - ed[j + 1]) * besselCoeff[j]; 								
		}
		temp *= 2.0 * ed[nLayers - 1] * besselCoeff[nLayers - 1];

#pragma omp parallel for reduction(+ : temp)
		for(int i = 0; i < nLayers - 1; i++) {
			for(int j = 0; j < nLayers - 1; j++)
				temp += (ed[i] - ed[i + 1]) * besselCoeff[i] * (ed[j] - ed[j + 1]) * besselCoeff[j];
		}
		intensity = temp + sq(ed[nLayers - 1] * besselCoeff[nLayers - 1]);
		intensity *= 16.0 * sq(sq(PI)) / (q * q * q);
	}
	   
    intensity *= scale;// * 1.0e-9; // the scale is way too low to use
    intensity += background;
 
	return intensity;   
}

std::complex<double> UniformHCModel::CalculateFF(Vector3d qvec, 
										   int nLayers, double w, double precision, VectorXd* p) {

    // TODO::ComplexModels Implement - Make sure the coefficients are correct!!
	std::complex<double> res(0.0, 0.0);
	double qx = qvec(0), qy = qvec(1), qz = qvec(2), qperp = sqrt(qx*qx + qy*qy);
	double q = sqrt(qx*qx + qy*qy + qz*qz);

	if(false) {
		if(q < 1.0e-7)
			return std::complex<double>(0., 0.);
		if(fabs(qz) > q) q = fabs(qz);	// Correct if (z/r) is greater than 1.0 by a few bits
		double th = acos(qz / q);
		double ph = atan2(qy, qx);
		if(ph < 0.0)
			ph += 6.28318530717958647692528676656;

		return std::complex<double>(th, ph);
	}

	if(closeToZero(qperp)) {
		res = (ed[nLayers - 1] - edSolvent) * t[nLayers - 1] * t[nLayers - 1];
		for(int i = 0; i < nLayers - 1; i++) {
			res += (ed[i] - ed[i + 1]) * t[i] * t[i];
		}
	} else {
		res = (ed[nLayers - 1] - edSolvent) * t[nLayers - 1] * bessel_j1(t[nLayers - 1] * qperp);
		for(int i = 0; i < nLayers - 1; i++) {
			res += (ed[i] - ed[i + 1]) * t[i] * bessel_j1(t[i] * qperp);
		}
	}

	if(closeToZero(qperp)) {
		res *= 4.0 * PI * sinc(qz * H) * H / 2.0;
	} else {
		res *= 4.0 * PI * sinc(qz * H) * H / (qperp);
	}

	return res;
}

void UniformHCModel::PreCalculateFF(VectorXd& p, int nLayers) {
	OrganizeParameters(p, nLayers);
}

#pragma endregion

#pragma region Gaussian Cylindrical Model

GaussianHCModel::GaussianHCModel(int heightSteps, int radiiSteps, std::string name, int extras, int nlp, int minlayers, int maxlayers, EDProfile edp)
										: CylindricalModel(heightSteps, name, edp.shape, nlp, minlayers, maxlayers, extras), 
																	steps1(radiiSteps) {
}

bool GaussianHCModel::IsSlow() {
	return (!std::isinf(H) == 0);	// Doesn't work. The ! should not be there, but when it's not, it shows the infinite as 
}


void GaussianHCModel::OrganizeParameters(const Eigen::VectorXd &p, int nLayers) {
	CylindricalModel::OrganizeParameters(p, nLayers);
	r = (*parameters).col(2);
}

void GaussianHCModel::PreCalculate(VectorXd& p, int nLayers) {
	OrganizeParameters(p, nLayers);
	
	for(nonzero = 0; (nonzero < nLayers) && (t[nonzero] <= 0.0); nonzero++);

	if(nonzero >= nLayers)
		return;

	xxR = MatrixXd::Zero(nLayers - nonzero, steps1);
	wwR = MatrixXd::Zero(nLayers - nonzero, steps1);

	#pragma omp parallel for
	for(int i = nonzero; i < nLayers; i++) {
		VectorXd x, w;
		double s = std::min(0.0, r[i] - 3.0 * t[i]);
		SetupIntegral(x, w, s, r[i] + 3.0 * t[i], steps1);
		xxR.row(i - nonzero) = x;
		wwR.row(i - nonzero) = w;
	}
}

double GaussianHCModel::Calculate(double q, int nLayers, VectorXd& p) {
	double intensity = 0.0;
	
	if(!std::isinf(H)) {	// Finite Model
		double resouter = 0.0;
#pragma omp parallel for reduction(+ : resouter)
		for (int outer = 0; outer < steps; outer++) {
			double ressum = 0.0;
			for (int sum = nonzero; sum < nLayers; sum++) {
				if(pStop && *pStop)		// Place these line strategically in
					continue;			// slow models.

				int es = sum - nonzero;
				double resinner = 0.0;
				for (int inner = 0; inner < steps1; inner++) {
					resinner += exp(-4.0 * ln2 * sq(xxR(es,inner)-r[sum])/sq(t[sum])) *
						xxR(es,inner)* bessel_j0(q * sqrt(1-sq(xx[outer]))* xxR(es,inner)) * wwR(es,inner); 
				}
				ressum += resinner * (ed[sum] - edSolvent);
			}
			resouter += sq(ressum * sin(q * xx[outer] * H)/ xx[outer] ) * ww[outer];
		}
		intensity = 2.5 * resouter * 32.0 * sq(PI) * PI / sq(q) ; 
		//the 2.5 is a factor to normalize the ed area between this and the discrete model.
	} else {		// Infinite Model
		double ressum = 0.0;
#pragma omp parallel for reduction(+ : ressum)
		for (int sum = nonzero; sum < nLayers; sum++) {
			int es = sum - nonzero;
			double resinner = 0.0;

			if(pStop && *pStop)		// Place these line strategically in
				continue;			// slow models.

#pragma omp parallel for if(nLayers - nonzero < 2) reduction(+ : resinner)
			for (int inner = 0; inner < steps1; inner++ ) {
				resinner += exp(-4.0 * ln2 * sq(xxR(es,inner)-r[sum])/sq(t[sum])) *
					xxR(es,inner) * bessel_j0(q * xxR(es,inner)) * wwR(es,inner); 
			}
			ressum += resinner * (ed[sum] - edSolvent);
		}

		intensity = 2.0 * sq(ressum * sq(PI)) * 64.0 / q; // single integral
	//the 2.0 is a factor to normalize the ed area between this and the discrete model.
	}

	intensity *= (*extraParams)(0);	// Multiply by scale
	intensity += (*extraParams)(1);	// Add background

	return intensity;
}

std::string GaussianHCModel::GetLayerParamName(int index, EDPFunction *edpfunc) {
	switch(index) {
		case 0:
			return "Thickness";
		case 2:
			return "Z_0";
		default:
			return CylindricalModel::GetLayerParamName(index, edpfunc);
	}
}

bool GaussianHCModel::IsParamApplicable(int layer, int lpindex) {
	if(layer == 0 && lpindex != 1)
		return false;
	return true;
}

struct GaussHCFunc
{
	GaussHCFunc(double _Qperp, double _R_i, double _Tau_i) :Qperp(_Qperp), R_i(_R_i), Tau_i(_Tau_i){}

	double operator()(double Rperp)
	{
		if (closeToZero(Qperp))
		{
			return exp(-4.0 * ln2 *sq(Rperp - R_i) / sq(Tau_i))
				* Rperp; //bessel j0(0) = 1

		}
		return exp(-4.0 * ln2 *sq(Rperp - R_i) / sq(Tau_i)) 
					* Rperp 
					*  bessel_j0(Qperp * Rperp);
	}

private:
	double Qperp, R_i, Tau_i;
};



std::complex<double> GaussianHCModel::CalculateFF(Vector3d qvec, int nLayers, double w, double precision, VectorXd* p) 
{
	double qx = qvec(0), qy = qvec(1), qz = qvec(2), qperp = sqrt(qx*qx + qy*qy);

	std::complex<double> res(0.0, 1.0);

	double mult = 4.0 * PI * sinc(qz * H) * H;
	
	double sum = 0;

	for (int i = 1; i < nLayers; i++)
		{
			double delPiG = ed[i] - ed[0]; 
			GaussHCFunc g(qperp, r[i], t[i]);
			res += delPiG * GaussKronrod15(g, 0., 5.2, 1e-6, 12, 3);
		}

	res *= mult;
	return res;


}

#pragma endregion

#pragma region Squished Tubes

Cylindroid::Cylindroid(int integralSteps, std::string str, ProfileShape eds, int ) : FFModel(str, 4, 2, 2, -1, EDProfile(SYMMETRIC, eds)),
		steps1(integralSteps) {}

ExtraParam Cylindroid::GetExtraParameter(int index) {
	switch(index) {
		default:
			return Geometry::GetExtraParameter(index);
		case 2:
			return ExtraParam("Height", std::numeric_limits<double>::infinity(), true, true);
		case 3:
			return ExtraParam("Short inner radius", 1.0, false, true);
	}
}

void Cylindroid::OrganizeParameters(const Eigen::VectorXd &p, int nLayers) {
	Geometry::OrganizeParameters(p, nLayers);
	r	= (*parameters).col(0);
	ed	= (*parameters).col(1);
	edSolvent = ed[0];
	b1 = (*extraParams)(3);
	h = (*extraParams)(2);

	for(int i = 1; i < r.size(); i++)
		r[i] += r[i - 1];
}

void Cylindroid::PreCalculate(Eigen::VectorXd &p, int nLayers) {
	OrganizeParameters(p, nLayers);

	b = VectorXd::Zero(nLayers);
	eps = VectorXd::Zero(nLayers);
	
	for(nonzero = 0; (nonzero < nLayers) && (r[nonzero] <= 0.0); nonzero++);

	if(nonzero >= nLayers)
		return;

	bSwitch = r[nonzero] < (*extraParams)(3);
	

	for(int i = nLayers - 1; i >= 0; i--) {
		ed[i] -= edSolvent;
		if (!bSwitch)
			eps[i] = sqrt(1.0 - (b1 * b1) / (r[nonzero] * r[nonzero]));
		else
			eps[i] = sqrt(1.0 - (r[nonzero] * r[nonzero]) / (b1 * b1));
	}

	for(int i = 0; i < nLayers; i++) {
		if(!bSwitch)
			b[i] = r[i] * sqrt(1.0 - eps[i] * eps[i]);
		else {
			b[i] = r[i] / sqrt(1.0 - eps[i] * eps[i]);
			std::swap(b[i], r[i]);
		}
	}
	
	steps = !std::isinf(h) ? 200 : 100;

	steps1  = 1 + int(sqrt(1.0 - sq(b1 / r[nonzero])) * 1000.0);		
#pragma omp parallel sections 
		{
#pragma omp section
			{
				SetupIntegral(xIn, wIn, std::numeric_limits<double>::epsilon(), 2.0 * PI + std::numeric_limits<double>::epsilon(), steps);	// x = theta_r

				cosInner = VectorXd::Zero(steps);
				for(int p = 0; p < steps; p++)
					cosInner[p] = cos(xIn[p]);
			}
#pragma omp section
			{
				if(!std::isinf(h)) {
					thetaSteps = 10 * int(h);
					SetupIntegral(xOut, wOut, 0.0, 2.0 * PI, steps); // Orientational Average: phiQ
					SetupIntegral(xThetaQ, wThetaQ, std::numeric_limits<double>::epsilon(), 1.0 - std::numeric_limits<double>::epsilon(), thetaSteps); // Orientational Average: x = cos(theta_q)
				}
				else
					SetupIntegral(xOut, wOut, 0.0, 2.0 * PI, steps1); // test
			}
		}

}

double Cylindroid::Calculate(double q, int nLayers, Eigen::VectorXd &p) {
	double intensity = 0.0;

	if(!std::isinf(h)) {	// Finite model
		int subSteps = int(2.0 * sqrt((double)steps));
		double root, rootX, sinValue, qc, res = 0.0;
		std::complex<double> im(0.0, 1.0);
		
#pragma omp parallel for reduction(+ : intensity)
		for(int dTheta = 0; dTheta < thetaSteps; dTheta++) {
			std::complex<double> innerRes(0.0, 0.0);
			rootX = sqrt(1.0 - sq(xThetaQ[dTheta]));	// Can move to PreCalculate
			sinValue = sq(sin(q * xThetaQ[dTheta] * h) / xThetaQ[dTheta]) / rootX;
			for(int phiQc = 0; phiQc < steps; phiQc++) {
				std::complex<double> tmp(0.0, 0.0);

				if(pStop && *pStop)		// Place these line strategically in
					continue;			// slow models.

				for(int inner = 0; inner < steps; inner++) {
					qc = q * cos(xIn[inner] - xOut[phiQc]);
					for(int i = 1; i < nLayers - 1; i++) {
						root = sqrt(1.0 - sq(eps[i] * cosInner[inner]));	// Can move to PreCalculate
						tmp += (ed[i] - ed[i + 1]) * ( (b[i] / ( -im * qc * rootX * root)) + (1.0 / sq(qc * rootX)) )
									* exp(-im * b[i] * qc * rootX / root);
					}
					tmp += ed[nLayers - 1] * ( (b[nLayers - 1] / (-im * qc * rootX * root) ) + (1.0 / sq(qc * rootX)) )
								* exp(-im * b[nLayers - 1] * qc * rootX / root);
					tmp -= ed[nonzero] / sq(qc * rootX);
					innerRes += tmp * wIn[inner];
				}	//inner
				res += norm(innerRes * wOut[phiQc]);
			}	//phiQc
			intensity += res * sinValue * wThetaQ[dTheta];
		}	//dTheta

	} else {	// Infinite model
		std::complex<double> temp (0.0, 0.0), im (0.0, 1.0);
			double  perp = q; // q
			
			int subSteps = int(2.0 * sqrt((double)steps));

#pragma omp parallel for reduction(+ : intensity)
			for(int outest = 0; outest <= steps / subSteps; outest++) {
				double subIntensity = 0.0;
				for(int tester = outest * subSteps; tester < subSteps * (outest + 1); tester++) { // test
					double tempIm = 0.0, tempRe = 0.0;
					if(!(tester < steps1))
						continue;

					if(pStop && *pStop)		// Place these line strategically in
						continue;			// slow models.

					for(int inner = 0; inner < steps; inner++) {
						std::complex <double> res(0.0,0.0);
						double root = 0.0;
						double qc = perp * cos(xIn[inner] - xOut[tester]);	// q*cos(phi_r)
						for(int i = nonzero; i < nLayers - 1; i++) {
							root = sqrt(1.0 - sq(eps[i] * cosInner[inner]/*cos(xIn[inner])*/));	// Can move to PreCalculate
							res += (ed[i] - ed[i + 1]) * ( (b[i] / ( -im * qc * root)) + (1.0 / sq(qc)) )
									* exp(-im * b[i] * qc / root);
						}

						root = sqrt(1.0 - sq(eps[nLayers - 1] * cosInner[inner]/*cos(xIn[inner])*/));	// Can move to PreCalculate
						res += ed[nLayers - 1] * ( (b[nLayers - 1] / (-im * qc * root) ) + (1.0 / sq(qc)) )
								* exp(-im * b[nLayers - 1] * qc / root);
						res -= ed[nonzero] / sq(qc);
						tempRe += res.real() * wIn[inner];
						tempIm += res.imag() * wIn[inner];

					}
					subIntensity += wOut[tester] * (sq(4.0 * PI) / q * (sq(tempRe)+ sq(tempIm)));
				} // test
				intensity += subIntensity;
			} // outest
	}

	intensity *= (*extraParams)(0);   // Multiply by scale
	intensity += (*extraParams)(1); // Add background

	return intensity;

}

bool Cylindroid::IsSlow() {
	return true;
}

std::complex<double> Cylindroid::CalculateFF(Eigen::Vector3d qvec, int nLayers, double w, double precision, Eigen::VectorXd *p) {
	return std::complex<double>(0.0, 1.0);
}

#pragma endregion

#pragma region Non-conserved Eccentricity Cylindroid

CylindroidVaryingEcc::CylindroidVaryingEcc(int integralSteps, std::string str, ProfileShape eds, int nlp) : Cylindroid(integralSteps, str, eds, nlp) {
}

void CylindroidVaryingEcc::PreCalculate(Eigen::VectorXd &p, int nLayers) {
	Cylindroid::PreCalculate(p, nLayers);

	for(int i = nLayers - 1; i >= 0; i--) {
		if (!bSwitch)
			eps[i] = sqrt(1.0 - sq((r[i] + b1 - r[nonzero]) / r[i]));
		else
			eps[i] = sqrt(1.0 - sq(r[i] / (r[i] + b1 - r[nonzero])));
	}


}

//double CylindroidVaryingEcc::Calculate(double q, int nLayers, Eigen::VectorXd &p) {
//}

#pragma endregion

#pragma region Gaussian Hollow Cylinder with a Hexagonal Structure Factor

// GHCwSFModel functions
GHCwSFModel::GHCwSFModel(std::string st, ProfileType edp, int factorials) : GaussianHCModel(1000, 500, st, 8, 3, 2, -1, EDProfile(NONE)) {
	fact.resize(factorials);
	lfact.resize(factorials);
	fact[0] = 1.0;
	int steps = 150;

#pragma omp parallel sections 
	{
#pragma omp section
		{	// For firstAttempt
		for(int i = 1; i < fact.size(); i++)
			fact[i] = fact[i - 1] * i;

		for(int i = 0; i < lfact.size(); i++)
			lfact[i] = log(fact[i]);
		}
#pragma omp section
		{	// For hexSF2D
			SetupIntegral(xx, ww, 0.0, 2.0 * PI, steps);
			cs.resize(steps);
			sn.resize(steps);
			sn = cs = xx.array();
			cs = cs.cos();
			sn = sn.sin();
		}
	}
}

void GHCwSFModel::PreCalculate(VectorXd& p, int nLayers){
	GaussianHCModel::PreCalculate(p, nLayers);

	hkMax = int(qmax * a / (2.0 * PI) + 0.5);

	convergeN = 0;
}

void GHCwSFModel::OrganizeParameters(const Eigen::VectorXd &p, int nLayers) {
	GaussianHCModel::OrganizeParameters(p, nLayers);

	hkMax = 8;

	lf = (*extraParams)(5);
	a  = (*extraParams)(6);
	debyeWaller = (*extraParams)(7);
}

VectorXd GHCwSFModel::CalculateVector(const std::vector<double>& q, int nLayers, VectorXd& p) {
	qmax = q[q.size() - 1];
	return GaussianHCModel::CalculateVector(q, nLayers, p);
}

double GHCwSFModel::Calculate(double q, int nLayers, VectorXd& p) {
	double ff = GaussianHCModel::Calculate(q, nLayers, p);
	double sf = hexSF2D(q, nLayers, p);

	return ff * sf;
}

double GHCwSFModel::hexSF2D(double q, int nLayers, VectorXd& p) {
	if(p.size() > 0)
		OrganizeParameters(p, nLayers);

	double intensity = 0.0;
	int end = (int)fact.size();
	bool conv = false;

#pragma omp parallel for reduction(+ : intensity)
	for(int h = 0; h <= hkMax; h++) {
		for(int k = 0 + int(h == 0); k <= hkMax; k++) {
			double g, gx , gy, sum = 0.0;
			g = gHex(h, k);
			if(g > qmax + 0.25)
				continue;
			gx = 2.0 * PI * double(h) / a;
			gy = 2.0 * PI * ((double(k) + 0.5 * double(h)) / a) / (0.8660254038/*sin(2pi/3)*/);

			for(int i = 0; i < xx.size(); i++) {
				sum += exp(-sq(lf) * (sq(q) - 2.0 * q * (gx * cs[i] + gy * sn[i]) + sq(g) ) / (4.0 * PI) ) * ww[i];
			} // for i
			intensity += sum;
		} // for k
	} // for h
	intensity *= sq(sq(lf / a));	// N^4
	intensity *= exp(-sq(q * debyeWaller) / 2.0);   // Debye Waller factor
	intensity *= (*extraParams)(3);	// SF Scale
	intensity += (*extraParams)(4);	// SF Background

	return intensity;
}

std::complex<double> GHCwSFModel::CalculateFF(Vector3d qvec, int nLayers, double w, 
											  double precision, VectorXd& p) {
	return std::complex<double> (0.0, 1.0);
}

ExtraParam GHCwSFModel::GetExtraParameter(int index) {
	switch(index) {
	default:
	case 0:
	case 1:
	case 2:
		return GaussianHCModel::GetExtraParameter(index);
	case 3:
		return ExtraParam("SF Scale", 1.0, false, true, false, 0.0, 0.0, false, 12);
	case 4:
		return ExtraParam("SF Background", 10.0e-2, false, false);
	case 5:
		return ExtraParam("Lf", 200.0, false, true);
	case 6:
		return ExtraParam("Lattice Spacing", 25.0, false, true);
	case 7:
		return ExtraParam("Debye-Waller", 0.0, false, true);
	}
}

bool GHCwSFModel::IsLayerBased() {
	return false;
}

double GHCwSFModel::gHex(int h, int k) {
	return 2.0 * PI * sqrt( sq(double(h)/a) + (1/sq(0.8660254038/*sin(2pi/3)*/)) * sq( (double(k) + 0.5 * double(h)) / a));
}

#pragma endregion

#pragma region Uniform Hollow Cylinder with a Hexagonal Structure Factor

// GHCwSFModel functions
HCwSFModel::HCwSFModel(std::string st, ProfileType edp, int factorials) : UniformHCModel(1000, st, DISCRETE, 2, 2, -1, 8) {
	fact.resize(factorials);
	lfact.resize(factorials);
	fact[0] = 1.0;
	int steps = 150;

	SetupIntegral(xx, ww, 0.0, 2.0 * PI, steps);
	cs.resize(steps);
	sn.resize(steps);
	sn = cs = xx.array();
	cs = cs.cos();
	sn = sn.sin();
}


void HCwSFModel::PreCalculate(VectorXd& p, int nLayers){
	UniformHCModel::PreCalculate(p, nLayers);

	hkMax = int(qmax * a / (2.0 * PI) + 0.5);

	convergeN = 0;
}

void HCwSFModel::OrganizeParameters(const Eigen::VectorXd &p, int nLayers) {
	UniformHCModel::OrganizeParameters(p, nLayers);

	hkMax = 8;

	lf = (*extraParams)(5);
	a  = (*extraParams)(6);
	debyeWaller = (*extraParams)(7);
}

VectorXd HCwSFModel::CalculateVector(const std::vector<double>& q, int nLayers, VectorXd& p) {
	qmax = q[q.size() - 1];
	return UniformHCModel::CalculateVector(q, nLayers, p);
}

double HCwSFModel::Calculate(double q, int nLayers, VectorXd& p) {
	double ff = UniformHCModel::Calculate(q, nLayers, p);
	double sf = hexSF2D(q, nLayers, p);

	return ff * sf;
}

double HCwSFModel::hexSF2D(double q, int nLayers, VectorXd& p) {
	if(p.size() > 0)
		OrganizeParameters(p, nLayers);

	double intensity = 0.0;
	int end = (int)fact.size();
	bool conv = false;

#pragma omp parallel for reduction(+ : intensity)
	for(int h = 0; h <= hkMax; h++) {
		for(int k = 0 + int(h == 0); k <= hkMax; k++) {
			double g, gx , gy, sum = 0.0;
			g = gHex(h, k);
			if(g > qmax + 0.25)
				continue;
			gx = 2.0 * PI * double(h) / a;
			gy = 2.0 * PI * ((double(k) + 0.5 * double(h)) / a) / (0.8660254038/*sin(2pi/3)*/);

			for(int i = 0; i < xx.size(); i++) {
				sum += exp(-sq(lf) * (sq(q) - 2.0 * q * (gx * cs[i] + gy * sn[i]) + sq(g) ) / (4.0 * PI) ) * ww[i];
			} // for i
			intensity += sum;
		} // for k
	} // for h
	intensity *= sq(sq(lf / a));	// N^4
	intensity *= exp(-sq(q * debyeWaller) / 2.0);   // Debye Waller factor
	intensity *= (*extraParams)(3);	// SF Scale
	intensity += (*extraParams)(4);	// SF Background

	return intensity;
}

std::complex<double> HCwSFModel::CalculateFF(Vector3d qvec, int nLayers, double w, 
											  double precision, VectorXd& p) {
	return std::complex<double> (0.0, 1.0);
}

ExtraParam HCwSFModel::GetExtraParameter(int index) {
	switch(index) {
	default:
		return ExtraParam("N/A");
	case 0:
	case 1:
	case 2:
		return UniformHCModel::GetExtraParameter(index);
	case 3:
		return ExtraParam("SF Scale", 1.0, false, true, false, 0.0, 0.0, false, 12);
	case 4:
		return ExtraParam("SF Background", 10.0e-2, false, false);
	case 5:
		return ExtraParam("Lf", 200.0, false, true);
	case 6:
		return ExtraParam("Lattice Spacing", 25.0, false, true);
	case 7:
		return ExtraParam("Debye-Waller", 0.0, false, true);
	}
}

bool HCwSFModel::IsLayerBased() {
	return false;
}

double HCwSFModel::gHex(int h, int k) {
	return 2.0 * PI * sqrt( sq(double(h)/a) + (1/sq(0.8660254038/*sin(2pi/3)*/)) * sq( (double(k) + 0.5 * double(h)) / a));
}

#pragma endregion
