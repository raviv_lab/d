
#include "StructureFactors.h"
#include "mathfuncs.h" // For PI, sq


void PeakBasedSF::OrganizeParameters(const Eigen::VectorXd &p, int nLayers) {
	Geometry::OrganizeParameters(p, nLayers);
	amp		= (*parameters).col(0);
	width	= (*parameters).col(1);
	xCenter	= (*parameters).col(2);
}

ExtraParam PeakBasedSF::GetExtraParameter(int index) {
	return ExtraParam("N/A");
}

std::string PeakBasedSF::GetLayerParamName(int index, const EDPFunction *edpfunc) {
	switch(index) {
		default:
			return "N/A";
		case 0:
			return "Amplitude";
		case 1:
			return "Width";
		case 2:
			return "Center";
	}

}


double PeakBasedSF::GetDefaultParamValue(int paramIndex, int layer, const EDPFunction *edpfunc) {
	switch(paramIndex) {
		case 0:
			return 1.0;
		case 1:
			return 0.05;
		case 2:
			return 1.0;
		default:
			return 0.0;
	}
}

std::string PeakBasedSF::GetLayerName(int layer) {
	return "Peak %d";  // This was originally layer+1 - do we have a problem here?
}

bool PeakBasedSF::IsParamApplicable(int layer, int lpindex) {
	return true;
}

void PeakBasedSF::PreCalculate(VectorXd& p, int nLayers) {
	OrganizeParameters(p, nLayers);
}

//////////////////////////////////////////////////////////////////////////
// Caille model

void CailleSF::OrganizeParameters(const VectorXd& p, int nLayers) {
	SFModel::OrganizeParameters(p, nLayers);
		/* Parameters:	D		- d spacing
						amp		- amplitude
						q		- take a guess
						eta		- take another guess
						N0		- N0 from Zhang
						h		- I have no idea
						sigma	- the measure of polydispersity of N0
						N_diff	- Pabst's N diffused
		*/

	D		= (*parameters)(0);
	amp		= (*parameters)(1);
	eta		= (*parameters)(2);
	N0		= (*parameters)(3);
	sig		= (*parameters)(4);
	NDiff	= (*parameters)(5);

}

std::string CailleSF::GetLayerParamName(int index) {
	switch(index) {
		default:
			return "N/A";
		case 0:
			return "D";
		case 1:
			return "Amplitude";
		case 2:
			return "Eta";
		case 3:
			return "N0";
		case 4:
			return "Sigma";
		case 5:
			return "NDiff";
	}

}


double CailleSF::GetDefaultParamValue(int paramIndex, int layer) {
	switch(paramIndex) {
		case 0:
			return 1.0;
		case 1:
			return 1.0;
		case 2:
			return 0.05;
		case 3:
			return 20.0;
		case 4:
			return 2.0;
		case 5:
			return 0.0;
		default:
			return 0.0;
	}
}
ExtraParam CailleSF::GetExtraParameter(int index) {
	return ExtraParam("N/A");
}

double CailleSF::Calculate(double q, int nLayers, VectorXd& p) {
	double tot = 0.0, wtTot = 0.0;
	double h = q / Q;

#pragma omp parallel for reduction( + : tot, wtTot)
	for(int N = int(std::max(0.0, N0 - 3.0 * sig)); N <= int(N0 + 3.0 * sig); N++) {
		double tmp = 0.0;

		if(pStop && *pStop)
			continue;

		for(int n1 = 1; n1 < N; n1++) {
			double ex = 0.0;
			ex = exp(-eta * double(h) * double(h) * ex1(N,n1));
			tmp += (1.0 - double(n1)/double(N)) * cos(q * double(n1) * D) * ex;
		}
		double wt = (sig > 0.0) ? gaussianSig(sig, N0, 1.0, 0.0, N) : 1.0;
		wtTot += wt;

		tot += amp * ((double(N) * wt * (2.0 * tmp + 1.0) / (q * q)) - NDiff);
	}

	return (tot / wtTot);
}

void CailleSF::PreCalculate(VectorXd& p, int nLayers) {
	OrganizeParameters(p, nLayers);
	//double mini = std::numeric_limits<double>::infinity();
	Q = 2.0 * PI / D;
	int size =  int(N0 + 0.5 + 3.0 * sig) + 1;

	if(ex1.cols() != ex1.rows() || ex1.cols() < size) {
		ex1.resize(size, size);
#pragma omp parallel for
		for(int N = 1; N < size; N++) {
			for(int i = 0; i < size; i++) {
				double tmp = 0.0;
				for(int j = 1; j <= N; j++)
					tmp += (1.0 - cos(double(j) * PI * double(i) / double(N))) / double(j);
				ex1(N,i) = tmp;
			}
		}
	}
}

VectorXd CailleSF::CalculateVector(const std::vector<double>& q, int nLayers, VectorXd& p) {
	qmax = q[q.size() - 1];
	return SFModel::CalculateVector(q, nLayers, p);
}

//////////////////////////////////////////////////////////////////////////

double GaussianSigmaPeakSFModel::Calculate(double q, int nLayers, Eigen::VectorXd &p) {
	if(p.size() > 0)	// For PD model
		OrganizeParameters(p, nLayers);
	
	double intensity = 1.0;
#pragma omp parallel for reduction(+ : intensity)
	for(int i = 0; i < nLayers; i++)
		intensity += gaussianSig(width[i], xCenter[i], amp[i], 0.0, q);

	return intensity;
}

//////////////////////////////////////////////////////////////////////////

double LorentzianPeakSFModel::Calculate(double q, int nLayers, Eigen::VectorXd &p) {
	if(p.size() > 0)	// For PD model
		OrganizeParameters(p, nLayers);

	double intensity = 1.0;
#pragma omp parallel for reduction(+ : intensity)
	for(int i = 0; i < nLayers; i++)
		intensity += lorentzian(width[i], xCenter[i], amp[i], 0.0, q);
	
	return intensity;
}

//////////////////////////////////////////////////////////////////////////

double LorentzianSquaredPeakSFModel::Calculate(double q, int nLayers, Eigen::VectorXd &p) {
	if(p.size() > 0)	// For PD model
		OrganizeParameters(p, nLayers);

	double intensity = 1.0;
#pragma omp parallel for reduction(+ : intensity)
	for(int i = 0; i < nLayers; i++)
		intensity += lorentzian_squared(width[i], xCenter[i], amp[i], 0.0, q);
	
	return intensity;
}

//////////////////////////////////////////////////////////////////////////

void MixedPeakBasedSF::OrganizeParameters(const Eigen::VectorXd &p, int nLayers) {
	Geometry::OrganizeParameters(p, nLayers);
	funcType	= (*parameters).col(0).cast<int>();
	amp			= (*parameters).col(1);
	width		= (*parameters).col(2);
	xCenter		= (*parameters).col(3);
}

ExtraParam MixedPeakBasedSF::GetExtraParameter(int index) {
	return ExtraParam("N/A");
}

double MixedPeakBasedSF::Calculate(double q, int nLayers, VectorXd& p) {
	if(p.size() > 0)	// For PD model
		OrganizeParameters(p, nLayers);

	double intensity = 1.0;
#pragma omp parallel for reduction(+ : intensity)
	for(int i = 0; i < nLayers; i++) {
		switch(funcType(i)) {
			default:
				break;
			case SHAPE_GAUSSIAN:	// Gaussian sigma
				intensity += gaussianSig(width[i], xCenter[i], amp[i], 0.0, q);
				break;
			case SHAPE_GAUSSIAN_FWHM: // Gaussian FWHM
				intensity += gaussianFW(width[i], xCenter[i], amp[i], 0.0, q);
				break;
			case SHAPE_LORENTZIAN: // Lorentzian
				intensity += lorentzian(width[i], xCenter[i], amp[i], 0.0, q);
				break;
			case SHAPE_LORENTZIAN_SQUARED: // Lorentzian squared
				intensity += lorentzian_squared(width[i], xCenter[i], amp[i], 0.0, q);
				break;
		}
	}
	return intensity;
}
