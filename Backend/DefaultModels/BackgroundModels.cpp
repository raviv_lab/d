
#include "BackgroundModels.h"

AdditiveBG::AdditiveBG() : BGModel("Additive Background Model") {
}

ExtraParam AdditiveBG::GetExtraParameter(int index) {
	// There are no extra parameters
	return ExtraParam("N/A");
}

void AdditiveBG::OrganizeParameters(const Eigen::VectorXd &p, int nLayers) {
	Geometry::OrganizeParameters(p, nLayers);
	funcType	= (*parameters).col(0).cast<int>();
	amp			= (*parameters).col(1);
	decay		= (*parameters).col(2);
	xCenter		= (*parameters).col(3);
}

double AdditiveBG::Calculate(double q, int nLayers, VectorXd& p) {
	if(p.size() > 0)	// For PD model
		OrganizeParameters(p, nLayers);
	
	double intensity = 0.0;
	for(int i = 0; i < nLayers; i++) {
		switch(funcType(i)) {
			default:
				break;
			case BG_EXPONENT:	// exponent
				intensity += amp[i] * exp(-(q - xCenter[i]) / decay[i]);
				break;
			case BG_LINEAR: // linear
				intensity += -amp[i] * q + decay[i];
				break;
			case BG_POWER: // power
				if(q - xCenter[i] > 1.0e-9)
					intensity += amp[i] * pow((q - xCenter[i]), (-decay[i]));
				break;
		}
	}
	return intensity;
}

std::string AdditiveBG::GetLayerParamName(int index) {
	switch(index) {
		case 0:
			return "Amplitude";
		case 1:
			return "Decay";
		case 2:
			return "X Center";
		default:
			return "N/A";
	}

}

std::string AdditiveBG::GetLayerName(int layer) {
	return "";	// TODO::BG This is a problem, as it depends on the function type...
}

bool AdditiveBG::IsParamApplicable(int layer, int lpindex) {
	return true;	// TODO::BG This is a problem, as it depends on the function type...

}
