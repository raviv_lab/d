#ifndef __HELICAL_MODELS_H
#define __HELICAL_MODELS_H

#include "Geometry.h"

	class HelicalModel : public FFModel {
	protected:
		double rHelix;
		double P, edSolvent;
		VectorXd delta;
		VectorXd deltaED;
		VectorXd rCs;

	public:
		HelicalModel(std::string st = "Helical model - do not use", int extraParams = 5);

		static ExtraParam GetExtraParameter(int index);

		static bool IsParamApplicable(int layer, int lpindex);

		virtual bool IsLayerBased();

		static std::string GetLayerParamName(int index, EDPFunction *edpfunc);

		static std::string GetLayerName(int layer);

		static double GetDefaultParamValue(int paramIndex, int layer, EDPFunction *edpfunc);

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

		virtual std::complex<double> CalculateFF(Vector3d qvec,
			int nLayers, double w, double precision, VectorXd* p) = 0;

	};

	class HelixModel : public HelicalModel {
	protected:
		int steps, steps1, subSteps;

		double height;

		VectorXd xIn, wIn, xOut, wOut;
	public:

		HelixModel(std::string st = "Helix", int integralStepsIn = 1000, int integralStepsOut = 1000, int extras = 5);

		virtual std::complex<double> CalculateFF(Vector3d qvec,
			int nLayers, double w, double precision, VectorXd* p);

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

		virtual bool IsSlow();

		virtual void PreCalculate(VectorXd& p, int nLayers);

		virtual void PreCalculateFF(VectorXd& p, int nLayers);

		virtual VectorXd Derivative(const std::vector<double>& x, VectorXd param,
			int nLayers, int ai);

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);

	};

	class HelixModelwHSF : public HelixModel {
	protected:
		VectorXd xx, ww;	// For numerical integral
		ArrayXd cs, sn;	// For numerical integral
		double a, lf, debyeWaller;
		int convergeN;
		int hkMax;

		virtual double Calculate(double q, int nLayers, VectorXd& p);

		double gHex(int h, int k);

		double hexSF2D(double q, int nLayers, VectorXd& p);

	public:
		HelixModelwHSF(std::string st = "Helix with Hexagonal Structure Factor", int extras = 10);

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

		virtual void PreCalculate(VectorXd& p, int nLayers);

		static ExtraParam GetExtraParameter(int index);

		virtual VectorXd CalculateVector(const std::vector<double>& q, int nLayers, VectorXd& p);

	};

	class DelixModel : public HelicalModel {
	protected:
		VectorXd x, w, root, deltaz;
		int steps;
		double rHelix, P, edSolvent, deltaw, debyeWaller;
		int Nb;

	public:
		DelixModel(std::string st = "Discrete Helix", int step = 400);

		virtual std::complex<double> CalculateFF(Vector3d qvec,
			int nLayers, double w, double precision, VectorXd* p);

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

		static ExtraParam GetExtraParameter(int index);

		virtual bool IsSlow();

		virtual void PreCalculate(VectorXd& p, int nLayers);

		virtual void PreCalculateFF(VectorXd& p, int nLayers);

		virtual VectorXd Derivative(const std::vector<double>& x, VectorXd param,
			int nLayers, int ai);

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);

	};

	class GaussianDelixModel : public DelixModel {
	public:
		GaussianDelixModel(std::string st = "Gaussian Discrete Helix", int step = 400);

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);
	};

#endif
