#ifndef __CYLINDRICAL_MODELS_H
#define __CYLINDRICAL_MODELS_H

#include "Geometry.h"

	class CylindricalModel : public FFModel {
	protected:
		int steps;

		double edSolvent;
		VectorXd ed;	// The electron density
		VectorXd t;		// The width of the layers
		double H;		// Half the height

		VectorXd xx, ww;
	public:
		CylindricalModel(int integralSteps = 1000, std::string str = "Abstract Cylindrical Model", ProfileShape eds = DISCRETE, int nlp = 2, int minlayers = 2, int maxlayers = -1, int extras = 2);

		static ExtraParam GetExtraParameter(int index);

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p) = 0;

	};


	class UniformHCModel : public CylindricalModel {
	public:
		UniformHCModel(int integralSteps = 1000, std::string str = "Uniform Hollow Cylinder", ProfileShape eds = DISCRETE, int nlp = 2, int minlayers = 2, int maxlayers = -1, int extras = 3);

		virtual void PreCalculate(VectorXd& p, int nLayers);

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

		virtual void PreCalculateFF(VectorXd& p, int nLayers);

		virtual std::complex<double> CalculateFF(Vector3d qvec,
			int nLayers, double w, double precision, VectorXd* p);
	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);

	};

	class GaussianHCModel : public CylindricalModel {
	protected:
		VectorXd r;	// The center of the Gaussian relative to the center of the cylinder
		int steps1, nonzero;
		MatrixXd xxR, wwR;

	public:
		GaussianHCModel(int heightSteps = 1000, int radiiSteps = 500, std::string name = "Gaussian Hollow Cylinder",
			int extras = 3, int nlp = 3, int minlayers = 2, int maxlayers = -1, EDProfile edp = EDProfile());

		virtual bool IsSlow();

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

		virtual void PreCalculate(VectorXd& p, int nLayers);

		static std::string GetLayerParamName(int index, EDPFunction *edpfunc);

		static bool IsParamApplicable(int layer, int lpindex);

		virtual std::complex<double> CalculateFF(Vector3d qvec,
			int nLayers, double w, double precision, VectorXd* p);

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);
	};

	/////////////////////////
	// Cylindroid Model(s) //
	/////////////////////////
	class CylindroidVaryingEcc;
	class Cylindroid : public FFModel {
	protected:
		int steps, steps1, thetaSteps, nonzero;

		bool bSwitch;
		double edSolvent, b1, h;
		VectorXd ed;	// The electron density
		VectorXd r;		// The radii of the layers
		VectorXd b;		// The short radii of the layers
		VectorXd eps;	// The eccentricities of the layers

		VectorXd xIn, xOut, wIn, wOut, xThetaQ, wThetaQ, cosInner;
	public:
		Cylindroid(int integralSteps = 1000, std::string str = "Conservered Eccentricity Cylindroid", ProfileShape eds = DISCRETE, int nlp = 2);

		static ExtraParam GetExtraParameter(int index);

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

		virtual void PreCalculate(VectorXd& p, int nLayers);

		virtual bool IsSlow();

		virtual std::complex<double> CalculateFF(Vector3d qvec,
			int nLayers, double w, double precision, VectorXd* p);

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);
	};

	class CylindroidVaryingEcc : public Cylindroid {
	public:
		CylindroidVaryingEcc(int integralSteps = 1000, std::string str = "Non-conservered Eccentricity Cylindroid", ProfileShape eds = DISCRETE, int nlp = 2);

		virtual void PreCalculate(VectorXd& p, int nLayers);
	};

	class GHCwSFModel : public GaussianHCModel {
	protected:
		VectorXd fact, lfact;		// Factorials
		VectorXd xx, ww;	// For numerical integral
		ArrayXd cs, sn;	// For numerical integral
		double a, lf, debyeWaller;
		int convergeN;
		int hkMax;

	public:
		GHCwSFModel(std::string st = "Gaussian Hollow Cylinder with Hexagonal Structure Factor", ProfileType edp = NONE, int factorials = 171);

		void PreCalculate(VectorXd& p, int nLayers);
		void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);
		virtual double Calculate(double q, int nLayers, VectorXd& p);
		virtual VectorXd CalculateVector(const std::vector<double>& q, int nLayers, VectorXd& p);

		virtual std::complex<double> CalculateFF(Vector3d qvec,
			int nLayers, double w, double precision, VectorXd& p);

		static ExtraParam GetExtraParameter(int index);

		virtual bool IsLayerBased();

	protected:
		double gHex(int h, int k);

		double hexSF2D(double q, int nLayers, VectorXd& p);
	};

	class HCwSFModel : public UniformHCModel {
	protected:
		VectorXd fact, lfact;		// Factorials
		VectorXd xx, ww;	// For numerical integral
		ArrayXd cs, sn;	// For numerical integral
		double a, lf, debyeWaller;
		int convergeN;
		int hkMax;

	public:
		HCwSFModel(std::string st = "Uniform Hollow Cylinder with Hexagonal Structure Factor", ProfileType edp = NONE, int factorials = 171);

		void PreCalculate(VectorXd& p, int nLayers);
		void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);
		virtual double Calculate(double q, int nLayers, VectorXd& p);
		virtual VectorXd CalculateVector(const std::vector<double>& q, int nLayers, VectorXd& p);

		virtual std::complex<double> CalculateFF(Vector3d qvec,
			int nLayers, double w, double precision, VectorXd& p);

		static ExtraParam GetExtraParameter(int index);

		virtual bool IsLayerBased();

	protected:
		double gHex(int h, int k);

		double hexSF2D(double q, int nLayers, VectorXd& p);
	};

#endif
