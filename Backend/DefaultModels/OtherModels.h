#ifndef __OTHER_MODELS_H
#define __OTHER_MODELS_H

#include "Geometry.h"


	class MicroemulsionModel : public FFModel {
	protected:
		double Izero, Imax, qmax;

	public:
		MicroemulsionModel(std::string st = "Micromulsion");

		static std::string GetDisplayParamName(int index);

		static double GetDisplayParamValue(int index, const paramStruct *p);

		static ExtraParam GetExtraParameter(int index);

		virtual bool IsLayerBased();

		virtual void OrganizeParameters(const Eigen::VectorXd &p, int nLayers);

		virtual void PreCalculate(VectorXd& p, int nLayers);

		virtual std::complex<double> CalculateFF(Vector3d qvec,
			int nLayers, double w = 1.0, double precision = 1E-5, VectorXd *p = NULL) {
			return std::complex<double>(0.0, 1.0);
		}

	protected:
		virtual double Calculate(double q, int nLayers, VectorXd& p);


	};

#endif
