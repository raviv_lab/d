#define EXPORTER

// Specific model headers
#include "SphericalModels.h"
#include "CylindricalModels.h"
#include "HelicalModels.h"
#include "SlabModels.h"
#include "OtherModels.h"
#include "StructureFactors.h"
#include "BackgroundModels.h"
#include "Symmetries.h"

#include "ModelContainer.h" // After SphericalModels.h to minimize redefinition warnings

// The model container (contained within the backend DLL) that
// contains the default models included with the program

enum ModelIndices {UNIFORM_HOLLOW_CYLINDER, CYLINDROID, SPHERE, CUBOID, SYMMETRIC_LAYERED_SLABS,
				  ASYMMETRIC_LAYERED_SLABS, HELIX, DELIX, GAUSSIAN_SLABS, MEMBRANE, GAUSSIAN_SPHERE,
				  GAUSSIAN_HOLLOW_CYLINDER, SMOOTH_SPHERE, MICROEMULSION, GAUSSIAN_HOLLOW_CYLINDER_W_HEX_SF,
				  UNIFORM_HOLLOW_CYLINDER_W_HEX_SF, HELIX_W_HEX_SF, GAUSSIAN_DELIX, CYLINDROID_W_VAR_ECC,
				  GAUSSIAN_SIGMA_PEAKS, LORENTZIAN_PEAKS, LORENTZIAN_SQUARED_PEAKS,
				  CAILLE_SF, ADDITIVE_BG, MIXED_PEAK_SF, GRID_SYMMETRY, MANUAL_SYMMETRY,
				  MIND_SIZE};

int GetNumCategories() {
	return 10;
}

ModelCategory GetCategoryInformation(int catInd) {
	switch(catInd) {
		default:	
		{
			ModelCategory mc = { "N/A", MT_FORMFACTOR, {-1} };
			return mc;
		}

		case 0:	
		{
			ModelCategory mc = { "Cylindrical Models", MT_FORMFACTOR,
								{UNIFORM_HOLLOW_CYLINDER, GAUSSIAN_HOLLOW_CYLINDER, 
								GAUSSIAN_HOLLOW_CYLINDER_W_HEX_SF, UNIFORM_HOLLOW_CYLINDER_W_HEX_SF, -1} };
			return mc;
		}

		case 1:	
		{
			ModelCategory mc = { "Spherical Models", MT_FORMFACTOR,
								{SPHERE, GAUSSIAN_SPHERE, SMOOTH_SPHERE, -1} };
			return mc;
		}

		case 2:	
		{
			ModelCategory mc = { "Slab Models", MT_FORMFACTOR,
								{SYMMETRIC_LAYERED_SLABS, ASYMMETRIC_LAYERED_SLABS, GAUSSIAN_SLABS, MEMBRANE, -1} };
			return mc;
		}

		case 3:	
		{
			ModelCategory mc = { "Helical Models", MT_FORMFACTOR, 
								{HELIX, DELIX, HELIX_W_HEX_SF, GAUSSIAN_DELIX, -1} };
			return mc;
		}

		case 4:	
		{
			ModelCategory mc = { "Cylindroids", MT_FORMFACTOR,
								{CYLINDROID, CYLINDROID_W_VAR_ECC, -1} };
			return mc;
		}

		case 5:	
		{
			ModelCategory mc = { "Microemulsions", MT_FORMFACTOR,
								{MICROEMULSION, -1} };
			return mc;
		}

		case 6:	
		{
			ModelCategory mc = { "Cuboids", MT_FORMFACTOR,
								{CUBOID, -1} };
			return mc;
		}

		case 7:
		{
			ModelCategory mc = { "Structure Factors", MT_STRUCTUREFACTOR,
								{GAUSSIAN_SIGMA_PEAKS, LORENTZIAN_PEAKS,
								LORENTZIAN_SQUARED_PEAKS, CAILLE_SF, MIXED_PEAK_SF, -1} };
			return mc;
		}

		case 8:
		{
			ModelCategory mc = { "Backgrounds", MT_BACKGROUND,
								{ADDITIVE_BG, -1} };
			return mc;
		}
		case 9:
			{
				ModelCategory mc = { "Symmetries", MT_SYMMETRY,
								    {GRID_SYMMETRY, MANUAL_SYMMETRY, -1} };
				return mc;

			}
	}
}

int GetNumModels() {
	return MIND_SIZE;
}

ModelInformation GetModelInformation(int index) {
	bool bGPU = false;

	switch(index) {
		default:		
			return ModelInformation("N/A");

		case UNIFORM_HOLLOW_CYLINDER:
			return ModelInformation("Uniform Hollow Cylinder", 0, index, true, 2, 2, -1, 3, 0, false, false, true);

		case GAUSSIAN_HOLLOW_CYLINDER: //being added
			return ModelInformation("Gaussian Hollow Cylinder", 0, index, false, 3, 2, -1, 3, 0, bGPU, false, true);

		case CYLINDROID:
			return ModelInformation("Cylindroid with Constant Eccentricity", 4, index, true, 2, 2, -1, 4, 0, bGPU, true);

		case SPHERE:
			return ModelInformation("Sphere", 1, index, true, 2, 2, -1, 2, 0, bGPU, false, true);

		case CUBOID:
			return ModelInformation("Cuboid", 6, index, false, 2, 4, 4, 2, 0, bGPU, true);

		case SYMMETRIC_LAYERED_SLABS:
			return ModelInformation("Symmetric Layered Slabs", 2, index, true, 2, 2, -1, 4, 0, bGPU, false, true);

		case ASYMMETRIC_LAYERED_SLABS:
			return ModelInformation("Asymmetric Layered Slabs", 2, index, true, 2, 2, -1, 4, 0, bGPU, false, true);

		case HELIX:
			return ModelInformation("Helix", 3, index, false, 3, 2, -1, 5, 0, bGPU, false, true);

		case DELIX:
			return ModelInformation("Discrete Helix", 3, index, false, 3, 2, -1, 7, 0, bGPU, true, true);

		// From here on, the models are not reachable from within the OpeningWindow
		case GAUSSIAN_SLABS:
			return ModelInformation("Gaussian Slabs", 2, index, false, 3, 2, -1, 2, 0, bGPU, false);

		case MEMBRANE:
			return ModelInformation("Membrane", 2, index, true, 3, 3, 3, 2, 4, bGPU, false);

		case GAUSSIAN_SPHERE:
			return ModelInformation("Gaussian Sphere", 1, index, false, 2, 2, -1, 2, 0, bGPU, false);



		case SMOOTH_SPHERE:
			return ModelInformation("Smooth Sphere", 1, index, false, 3, 2, -1, 2, 0, bGPU, false);

		case MICROEMULSION:
			return ModelInformation("Microemulsion", 5, index, false, 0, 0, 0, 5, 3, bGPU, false);

		case GAUSSIAN_HOLLOW_CYLINDER_W_HEX_SF:
			return ModelInformation("Gaussian Hollow Cylinder with Hexagonal Structure Factor", 0, index, false, 3, 2, -1, 8, 0, bGPU, true);

		case UNIFORM_HOLLOW_CYLINDER_W_HEX_SF:
			return ModelInformation("Uniform Hollow Cylinder with Hexagonal Structure Factor", 0, index, false, 2, 2, -1, 8, 0, bGPU, true);

		case HELIX_W_HEX_SF:
			return ModelInformation("Helix with Hexagonal Structure Factor", 3, index, false, 3, 2, -1, 10, 0, bGPU, true);

		case GAUSSIAN_DELIX:
			return ModelInformation("Gaussian Discrete Helix", 3, index, false, 3, 2, -1, 7, 0, bGPU, true);

		case CYLINDROID_W_VAR_ECC:
			return ModelInformation("Cylindroid with Variable Eccentricity", 4, index, false, 2, 2, -1, 4, 0, bGPU, true);

		case GAUSSIAN_SIGMA_PEAKS:
			return ModelInformation("Gaussian Sigma Peak Model", 7, index, true, 3, -1, -1, 0);

		case LORENTZIAN_PEAKS:
			return ModelInformation("Lorentzian Peak Model", 7, index, true, 3, -1, -1, 0);

		case LORENTZIAN_SQUARED_PEAKS:
			return ModelInformation("Lorentzian Squared Peak Model", 7, index, true, 3, -1, -1, 0);

		case CAILLE_SF:
			return ModelInformation("Caille Structure Factor", 7, index, false, 6, 1, 1, 0);

		case ADDITIVE_BG:
			return ModelInformation("Additive Background Functions", 8, index, true, 4, -1, -1, 0);
	
		case MIXED_PEAK_SF:
			return ModelInformation("Mixed Peak SF Model", 7, index, true, 4, -1, -1, 0);
			
		case GRID_SYMMETRY:
			return ModelInformation("Space-filling Symmetry", 9, index, true, 3, 3, 3, 1);

		case MANUAL_SYMMETRY:
			return ModelInformation("Manual Symmetry", 9, index, true, 6, 0, -1, 1);
	}
}

IModel *GetModel(int index) {
	// This serves as an abstract factory of sorts
	switch(index) {
		default:
			return NULL;
		case UNIFORM_HOLLOW_CYLINDER:
			return new UniformHCModel();
		case CYLINDROID:
			return new Cylindroid();
		case SPHERE:
			return new UniformSphereModel();
		case CUBOID:
			return new CuboidModel();
		case SYMMETRIC_LAYERED_SLABS:
			return new UniformSlabModel();
		case ASYMMETRIC_LAYERED_SLABS:
			return new UniformSlabModel("Asymmetric Uniform Slabs", ASYMMETRIC);
		case HELIX:
			return new HelixModel();
		case DELIX:
			return new DelixModel();
		case GAUSSIAN_SLABS:
			return new GaussianSlabModel();
		case MEMBRANE:
			return new MembraneModel();
		case GAUSSIAN_SPHERE:
			return new GaussianSphereModel();
		case GAUSSIAN_HOLLOW_CYLINDER:
			return new GaussianHCModel();
		case SMOOTH_SPHERE:
			return new SmoothSphereModel();
		case MICROEMULSION:
			return new MicroemulsionModel();
		case GAUSSIAN_HOLLOW_CYLINDER_W_HEX_SF:
			return new GHCwSFModel();
		case UNIFORM_HOLLOW_CYLINDER_W_HEX_SF:
			return new HCwSFModel();
		case HELIX_W_HEX_SF:
			return new HelixModelwHSF();
		case GAUSSIAN_DELIX:
			return new GaussianDelixModel();
		case CYLINDROID_W_VAR_ECC:
			return new CylindroidVaryingEcc();
		case GAUSSIAN_SIGMA_PEAKS:
			return new GaussianSigmaPeakSFModel();
		case LORENTZIAN_PEAKS:
			return new LorentzianPeakSFModel();
		case LORENTZIAN_SQUARED_PEAKS:
			return new LorentzianSquaredPeakSFModel();
		case CAILLE_SF:
			return new CailleSF(/*"Caille Structure Factor", 7, index, true, 1, -1, -1, 5*/);
		case ADDITIVE_BG:
			return new AdditiveBG(/*"Additive Background Functions", 8, index, true, 4, -1, -1, 0*/);
		case MIXED_PEAK_SF:
			return new MixedPeakBasedSF();

		// This is dirty but had to be done (these are actually ISymmetry*s)
		case GRID_SYMMETRY:
			return (IModel *)new GridSymmetry();
		case MANUAL_SYMMETRY:
			return (IModel *)new ManualSymmetry();
	}

	return NULL;
}

template<typename T>
static void *GetMIProc(InformationProcedure type) {
        if(type == IP_LAYERPARAMNAME)         return (void *)&T::GetLayerParamName;
	else if(type == IP_LAYERNAME)         return (void *)&T::GetLayerName;
	else if(type == IP_EXTRAPARAMETER)    return (void *)&T::GetExtraParameter;
	else if(type == IP_DEFAULTPARAMVALUE) return (void *)&T::GetDefaultParamValue;
	else if(type == IP_ISPARAMAPPLICABLE) return (void *)&T::IsParamApplicable;
	else if(type == IP_DISPLAYPARAMNAME)  return (void *)&T::GetDisplayParamName;
	else if(type == IP_DISPLAYPARAMVALUE) return (void *)&T::GetDisplayParamValue;
	
	return NULL;
}

EXPORTED void *GetModelInformationProcedure(int index, InformationProcedure type) {
	if(type <= IP_UNKNOWN || type >= IP_UNKNOWN2)
		return NULL;

	// This serves as an abstract factory of sorts
	switch(index) {
		default:
			return NULL;
		case UNIFORM_HOLLOW_CYLINDER:
			return GetMIProc<UniformHCModel>(type);
		case CYLINDROID:
			return GetMIProc<Cylindroid>(type);
		case SPHERE:
			return GetMIProc<UniformSphereModel>(type);
		case CUBOID:
			return GetMIProc<CuboidModel>(type);
		case SYMMETRIC_LAYERED_SLABS:
		case ASYMMETRIC_LAYERED_SLABS:
			return GetMIProc<UniformSlabModel>(type);
		case HELIX:
			return GetMIProc<HelixModel>(type);
		case DELIX:
			return GetMIProc<DelixModel>(type);
		case GAUSSIAN_SLABS:
			return GetMIProc<GaussianSlabModel>(type);
		case MEMBRANE:
			return GetMIProc<MembraneModel>(type);
		case GAUSSIAN_SPHERE:
			return GetMIProc<GaussianSphereModel>(type);
		case GAUSSIAN_HOLLOW_CYLINDER:
			return GetMIProc<GaussianHCModel>(type);
		case SMOOTH_SPHERE:
			return GetMIProc<SmoothSphereModel>(type);
		case MICROEMULSION:
			return GetMIProc<MicroemulsionModel>(type);
		case GAUSSIAN_HOLLOW_CYLINDER_W_HEX_SF:
			return GetMIProc<GHCwSFModel>(type);
		case UNIFORM_HOLLOW_CYLINDER_W_HEX_SF:
			return GetMIProc<HCwSFModel>(type);
		case HELIX_W_HEX_SF:
			return GetMIProc<HelixModelwHSF>(type);
		case GAUSSIAN_DELIX:
			return GetMIProc<GaussianDelixModel>(type);
		case CYLINDROID_W_VAR_ECC:
			return GetMIProc<CylindroidVaryingEcc>(type);


		case GAUSSIAN_SIGMA_PEAKS:
			return GetMIProc<GaussianSigmaPeakSFModel>(type);
		case LORENTZIAN_PEAKS:
			return GetMIProc<LorentzianPeakSFModel>(type);
		case LORENTZIAN_SQUARED_PEAKS:
			return GetMIProc<LorentzianSquaredPeakSFModel>(type);
		case CAILLE_SF:
			return GetMIProc<CailleSF>(type);
		case ADDITIVE_BG:
			return GetMIProc<AdditiveBG>(type);
		case MIXED_PEAK_SF:
			return GetMIProc<MixedPeakBasedSF>(type);

		case GRID_SYMMETRY:
			return GetMIProc<GridSymmetry>(type);
		case MANUAL_SYMMETRY:
			return GetMIProc<ManualSymmetry>(type);
	}
}
