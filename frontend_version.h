//
// This files contains the version of the frontend (DLLs and D+).
//
// It will be updated manually and replaces the old gitrev.h.
//

#ifndef FRONTEND_VERSION_H
#define FRONTEND_VERSION_H

// The version is used in RC files for resources, so it must be broken into elements
#define FRONTEND_VERSION_MAJOR 3
#define FRONTEND_VERSION_MINOR 2
#define FRONTEND_VERSION_REVISION 1
#define FRONTEND_VERSION_BUILD 15


// Double macro expansion taken from here: http://stackoverflow.com/a/5459929/871910
// Lengthy macro names are here to prevent pollution the global namespace with these macro names
#define FRONTEND_VERSION_STR_HELPER(x) #x
#define FRONTEND_VERSION_STR(x) FRONTEND_VERSION_STR_HELPER(x)

#define FRONTEND_VERSION FRONTEND_VERSION_STR(FRONTEND_VERSION_MAJOR) ", " FRONTEND_VERSION_STR(FRONTEND_VERSION_MINOR) ", " FRONTEND_VERSION_STR(FRONTEND_VERSION_REVISION) ", " FRONTEND_VERSION_STR(FRONTEND_VERSION_BUILD)


#endif
